\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{21}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivos}{22}{section.1.1}
\contentsline {section}{\numberline {1.2}Metodologia}{22}{section.1.2}
\contentsline {section}{\numberline {1.3}Organiza\IeC {\c c}\IeC {\~a}o do Documento}{23}{section.1.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica e Tecnol\IeC {\'o}gica}}{25}{chapter.2}
\contentsline {section}{\numberline {2.1}Engenharia de Requisitos}{25}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Processo de Engenharia de Requisitos}{26}{subsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.1.1}Investiga\IeC {\c c}\IeC {\~a}o}{27}{subsubsection.2.1.1.1}
\contentsline {subsubsection}{\numberline {2.1.1.2}An\IeC {\'a}lise}{30}{subsubsection.2.1.1.2}
\contentsline {subsubsection}{\numberline {2.1.1.3}Especifica\IeC {\c c}\IeC {\~a}o}{31}{subsubsection.2.1.1.3}
\contentsline {subsubsection}{\numberline {2.1.1.4}Valida\IeC {\c c}\IeC {\~a}o}{32}{subsubsection.2.1.1.4}
\contentsline {section}{\numberline {2.2}Sistemas Autoadaptativos}{34}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Propriedades de Autoadapta\IeC {\c c}\IeC {\~a}o}{34}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Abordagens de Sistemas Autoadaptativos}{37}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Abordagens de apoio ao Desenvolvimento de Sistemas Autoadaptativos}{38}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Plataforma Eclipse para o Desenvolvimento de Plugins}{39}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Arquitetura da Plataforma Eclipse}{39}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Arquitetura de um Plugin Eclipse}{40}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}\textit {Plugin Development Environment}}{42}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Conclus\IeC {\~a}o do Cap\IeC {\'\i }tulo}{42}{section.2.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Trabalhos Relacionados}}{43}{chapter.3}
\contentsline {section}{\numberline {3.1}Metodologia}{43}{section.3.1}
\contentsline {section}{\numberline {3.2}Trabalhos}{43}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Ferramentas de Apoio ao Processo de Engenharia de Requisitos}{44}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Ferramentas de Apoio ao Processo de Engenharia de Requisitos para Sistemas Autoadaptativos}{45}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}An\IeC {\'a}lise}{45}{section.3.3}
\contentsline {section}{\numberline {3.4}Conclus\IeC {\~a}o do Cap\IeC {\'\i }tulo}{46}{section.3.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Fase Preliminar de Desenvolvimento}}{47}{chapter.4}
\contentsline {section}{\numberline {4.1}Fase de Concep\IeC {\c c}\IeC {\~a}o}{47}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Declara\IeC {\c c}\IeC {\~a}o do Problema}{47}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Declara\IeC {\c c}\IeC {\~a}o do Produto}{48}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Ambiente do Usu\IeC {\'a}rio}{48}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Vis\IeC {\~a}o Geral do Produto}{49}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2}Fase de Elabora\IeC {\c c}\IeC {\~a}o- Intera\IeC {\c c}\IeC {\~a}o 1}{50}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Est\IeC {\'o}rias}{52}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Modelo de Caso de Uso}{52}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Modelo de Dom\IeC {\'\i }nio}{52}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Arquitetura Utilizada}{53}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}\textit {StoryBoard} da Ferramenta}{53}{subsection.4.2.5}
\contentsline {section}{\numberline {4.3}Conclus\IeC {\~a}o do Cap\IeC {\'\i }tulo}{54}{section.4.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Conclus\IeC {\~o}es Preliminares}}{55}{chapter.5}
\contentsline {section}{\numberline {5.1}Cronograma}{55}{section.5.1}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Refer\^encias}{57}{section*.10}
\cftinsert {AAA}
\renewcommand *{\cftappendixname }{ANEXO \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Anexos}}{61}{section*.11}
