\chapter{Desenvolvimento da Ferramenta}
%texto introdutório, propósito e organização
Esse capítulo apresenta os artefatos obtidos nas atividades de desenvolvimento da ferramenta, a~\autoref{sectionAnalise} trata sobre a análise realizada, nessa seção é apresentado o problema a ser resolvido, técnicas para o entendimento desse problema e os respectivos artefatos gerados. Após isso, a~\autoref{sectionProjeto} apresenta o projeto da ferramenta descrevendo a solução proposta, os artefatos de projeto e a arquitetura do software. Já a~\autoref{sectionImplementacao} apresenta a etapa de implementação, nela será apresentado aspectos de codificação. A etapa de testes é descrita na~\autoref{sectionTestes}, nela será abordado os testes realizados na ferramenta para comprovar a sua eficiência a nível de componente unitário até a integração desses componentes.

\section{Análise}\label{sectionAnalise}

Conforme apresentado por~\citeonline{whittle2010relax} existe uma carência de ferramentas de suporte a criação e especificação de requisitos para \ac{sa}, muitas vezes a preocupação está direcionada somente a arquitetura e projeto desses sistemas. O objetivo principal do nosso estudo é desenvolver um \textit{plugin} para a \ac{ide} Eclipse que possibilite a especificação de requisitos em \ac{sa} utilizando a linguagem de requisitos proposta por~\citeonline{whittle2010relax}.

As atividades de análise são destinadas para o conhecimento do problema do cliente, nessa fase foram gerados artefatos como: \textit{storyboard}, histórias de usuário, documento de casos de uso e diagrama de caso de uso.

\subsection{Histórias de Usuário}

Através do conhecimento da problemática abordada por~\citeonline{whittle2010relax} foi construído histórias de usuário para compreender o que o usuário (desenvolvedor) pode realizar (ações) na ferramenta, a partir dessa técnica também é possível visualizar o motivo dessas ações.

As histórias são utilizadas para descrever requisitos em processos de desenvolvimento ágil como por exemplo: \ac{xp}, \textit{Scrum}, entre outros. Nessa fase foi utilizado essa prática para simplificar o modelo de caso de uso (~\autoref{docCasoDeUso:anexo}) e demonstrar os objetivos e motivos de cada funcionalidade. Na \autoref{tab:estorias} é possível visualizar as histórias construídas.

\begin{table}[!htb]
\footnotesize
\caption[Narrativas de requisitos]{Narrativas de requisitos.}
\begin{center}
\label{tab:estorias}
\begin{tabular}{m{1.0cm}m{12.5cm}}
  \toprule
  \textbf{ID} & \textbf{Descrição}\\
  \hline
  1 & Como desenvolvedor, quero escrever requisitos utilizando a linguagem Relax,
para que eu possa expressar os aspectos ambientais e de incerteza nos
requisitos de Sistemas Autoadaptativos.\\
  2 & Como desenvolvedor, quero salvar minha especificação de requisitos, para
que eu possa editá-la em outro momento.\\
  3 & Como desenvolvedor, quero converter minha especificação Relax em um modelo
semântico, para que nas fases posteriores do projeto seja possível a
compreensão dos requisitos por diferentes papéis da equipe de
desenvolvimento como analistas, projetistas, codificadores, testadores e assim
por diante.\\
  4 & Como desenvolvedor, quero verificar os erros sintáticos da minha especificação,
para que eu possa construí-la e manté-la com mais qualidade.\\
 \bottomrule
\end{tabular}
\end{center}
\end{table}

\subsection{\textit{StoryBoard}}

A técnica \textit{StoryBoard}\footnote{\textit{StoryBoard} é uma técnica que utiliza a narração de cenários organizados em roteiro para demonstrar os objetivos (funcionalidades) principais do software a ser desenvolvido.} foi utilizada para apresentar de maneira geral as funcionalidades da ferramenta, as narrativas do personagem descreverão os principais objetivos da ferramenta e suas possibilidades.

Na \autoref{fig_storyBoard1},~\autoref{fig_storyBoard2},~\autoref{fig_storyBoard3} e~\autoref{fig_storyBoard4} é possível visualizar o contexto em que a ferramenta pode ser utilizada e suas principais funcionalidades como a escrita de requisitos e a coversão para modelo semântico.

\begin{figure}[!htb]
\centering
\caption{\textit{Storyboard} - parte I}
\includegraphics[width=16cm, height=6cm]{img/StoryBoard1.png}
\label{fig_storyBoard1}
\end{figure}

\begin{figure}[!htb]
\centering
\caption{\textit{Storyboard} - parte II}
\includegraphics[width=14cm, height=6cm]{img/StoryBoard1-1.png}
\label{fig_storyBoard2}
\end{figure}

\begin{figure}[!htb]
\centering
\caption{\textit{Storyboard} - parte III}
\includegraphics[width=14cm, height=6cm]{img/StoryBoard1-2.png}
\label{fig_storyBoard3}
\end{figure}

\begin{figure}[!htb]
\centering
\caption{\textit{Storyboard} - parte IV}
\includegraphics[width=14cm, height=6cm]{img/StoryBoard1-3.png}
\label{fig_storyBoard4}
\end{figure}

O~\textit{StoryBoard} inicia com a chegada de uma cliente solicitando um sistema autoadaptativo para sua problemática, após isso o atendente encaminha o pedido para o setor de analistas, o analista utiliza o RelaxEditor para especificar os requisitos em linguagem Relax.

\vspace{12cm}

\subsection{Modelo de Caso de Uso}

A histórias de usuário foram fundamentais para a construção dos casos de uso do sistema (disponível no~\autoref{docCasoDeUso:anexo}), os quais estão representados em forma de diagrama na~\autoref{fig_casoDeUso}. A partir desse é possível visualizar as funcionalidades que a ferramenta disponibiliza para a resolução da problemática abordada na etapa de análise.

\begin{figure}[!htb]
\centering
\caption{Diagrama de caso de uso.}
\includegraphics[width=14cm, height=9cm]{img/useCaseDiagram.png}
\label{fig_casoDeUso}
\end{figure}

Dentre os casos de uso apresentados na~\autoref{fig_casoDeUso}, o caso de uso de maior prioridade é o escrever requisitos em linguagem Relax, essa funcionalidade do sistema foi a primeira a ser implementada, a partir dela o desenvolvedor pode escrever os requisitos em um editor, no qual os operadores reservados da linguagem são reconhecidos e destacados. Outra funcionalidade de prioridade alta foi a conversão do requisito especificado para um modelo semântico.

Além dessas funcionalidades, as operações básicas de usuário~\ac{crud} são fundamentais para a ferramenta, pois a mesma persiste dados de especificação de requisitos.


\section{Projeto}\label{sectionProjeto}

A atividade de projeto tem por objetivo propor a solução para o problema investigado na etapa de análise. Dentre os artefatos construídos, os principais são os diagramas de arquitetura, classes e de sequência.

Os artefatos de projeto devem respeitar as restrições impostas pela plataforma Eclipse, como também deve utilizar os recursos disponibilizados pela mesma. 

\subsection{Diagrama de Arquitetura}

A arquitetura proposta utiliza duas camadas de suporte, a \textit{Service} e a \ac{dao} para complementar o modelo tradicional~\ac{mvc}. A arquitetura é representada pela~\autoref{fig_arquitetura}.

\begin{figure}[!htb]
\centering
\caption{Arquitetura utilizada.}
\includegraphics[width=16cm,height=9cm]{img/arquiteturaMVC.png}
\label{fig_arquitetura}
\end{figure}

O módulo arquitetural \textit{View} contém os componentes gráficos utilizados pela aplicação. Na plataforma Eclipse a interface gráfica utiliza em seus \textit{plugins} as bibliotecas \textit{JFace} e \textit{SWT}. Soma-se a isso o módulo \textit{Controller} que centraliza as estratégias da aplicação, esse módulo é responsável pelo gerenciamento das classes de negócio (\textit{Model}), os serviços disponibilizados pela camada \textit{Service} e os \textit{listeners} disparados pela \textit{View}. Quanto a camada~\ac{dao}, ela é responsável pela infraestrutura de persistência da aplicação.

Além disso, o módulo~\textit{Model} é responsável por tratar as classes de domínio da aplicação. Esse módulo possui uma camada arquitetural interna chamada~\textit{Repository}, na qual são gerenciadas as coleções de objetos.

\subsection{Diagrama de Classes}

O diagrama de classes reflete a estrutura estática do software descrevendo as classes (atributos e métodos) e relacionamentos. O diagrama abordado apresenta a estrutura do módulo arquitetural \textit{Model}. A \autoref{fig_diagramaDeClasse1} e \autoref{fig_diagramaDeClasse2} apresentam o diagrama de classes do software.


\begin{figure}[!htb]
\centering
\caption{Modelo de classes - parte 1.}
\includegraphics[width=16cm, height=10cm]{img/primeiraParteDiagrama.png}
\label{fig_diagramaDeClasse1}
\end{figure}

\begin{figure}[!htb]
\centering
\caption{Modelo de classes - parte 2.}
\includegraphics[width=16cm, height=11cm]{img/segundaParteDiagrama.png}
\label{fig_diagramaDeClasse2}
\end{figure}

A~\autoref{fig_diagramaDeClasse1} apresenta a classe especificação que possui uma lista de requisitos, cada requisito possui fatores de incerteza, os quais podem surgir do ambiente ou de fatores de monitoração. Também, um requisito possui relacionamentos entre esses fatores (ambiente e monitoração) e dependências.

A~\autoref{fig_diagramaDeClasse2} possui as classe interpretador, ela é reponsável pela maior parte das operações relacionadas a interpretação da descrição de um requisito, o "GeradorDeExpressao" utiliza todos os recursos do "Intepretador" para poder gerar uma expressão semântica adequada para a descrição textual de entrada. Em parceria com o 'AnalisadorSintaticoDeExpressao', "AnalisadorLexicoDeExpressao", o "Interpretador" realiza operações como verificações léxicas e sintáticas.


\subsection{Diagrama de Sequência}

Os diagramas de sequência são utilizados para representar a ordem temporal das trocas de mensagem entre os objetos, esse modelo reflete aspectos dinâmicos do sistema, diferente do diagrama de classes. A~\autoref{fig_diagramaDeSeq1} e ~\autoref{fig_diagramaDeSeq2} apresentam a sequência de mensagens que ocorrem para a conversão de uma descrição de requisito para um modelo semântico.

A~\autoref{fig_diagramaDeSeq1} apresenta o início da troca de mensagens, a primeira etapa é o evento disparado pelo usuário, o qual é capturado pela instância do "ControleRelaxParte", após isso o objeto controlador inicializa o "AnalisadorLexicoDeRequisito" para verificar a quantidade de erros contidos na expressão de requisito.

\begin{figure}[!htb]
\centering
\caption{Modelo de sequência - parte 1.}
\includegraphics[width=13cm, height=8cm]{img/DSparte1.png}
\label{fig_diagramaDeSeq1}
\end{figure}


%\begin{figure}[!htb]
%\centering
%\includegraphics[width=18cm, height=9cm]{img/DSparte2.png}
%\caption{Modelo de Sequência - Parte 2.}
%\label{fig_diagramaDeSeq2}
%\end{figure}

A~\autoref{fig_diagramaDeSeq2} apresenta a inicialização do "Interpretador", a partir dele é possível avaliar lexicamente a expressão, essa verificação é fundamental, caso o valor retornado seja verdadeiro, o "GeradorDeExpressao" é inicializado para gerar a expressão semântica do requisito.


\begin{figure}[!htb]
\centering
\caption{Modelo de sequência - parte 2.}
\includegraphics[width=16cm, height=9cm]{img/DSparte2.png}
\label{fig_diagramaDeSeq2}
\end{figure}


\section{Implementação}\label{sectionImplementacao}

A implementação foi realizada utilizando a linguagem de programação Java e os recursos disponibilizados pela plataforma Eclipse.


\subsection{Interface Gráfica}

A interface gráfica foi desenvolvida a partir dos componentes disponibilizados pela \textit{JFace} e os recursos~\textit{SWT}\footnote{\textit{SWT} é uma biblioteca que disponibiliza componentes gráficos de interface.}. A~\autoref{fig_implementacaoTelaInicial} apresenta a interface gráfica da tela principal.

\begin{figure}[!htb]
\centering
\caption{Interface gráfica - tela principal}
\includegraphics[width=16cm, height=9cm]{img/areaTotalDoPlugin.png}
\label{fig_implementacaoTelaInicial}
\end{figure}

A partir da~\autoref{fig_implementacaoTelaInicial} é possível visualizar o recurso de abas utilizado, o qual possibilita que o usuário minimize ou maximize sua área. Além disso, a interface gráfica possui na aba de projetos um menu árvore, nele é organizado os requisito como itens.

Na área de editor da~\autoref{fig_implementacaoTelaInicial} é possível visualizar a aba editor, ela possui um recurso \textit{highlighter text} para destacar as palavras reservadas da linguagem. O editor conta também com outros campos para que o usuário possa especificar os aspectos de ambiente, monitoração, relação e dependência.

%\begin{figure}[!htb]
%\centering
%\includegraphics[width=15cm, height=9cm]{img/telaComFocoEmEditor.png}
%\caption{Interface gráfica - aba \textit{editor}}
%\label{fig_implementacaoTelaGraficaEditor}
%\end{figure}

Além da aba \textit{Project} (apresentada na área de projeto) e \textit{Editor}, o \textit{plugin} conta com a aba \textit{Relax Expression} (apresentada na área de expressão) nela é apresentado o resultado do processo de geração da expressão semântica do requisito.

%\begin{figure}[!htb]
%\centering
%\includegraphics[width=13cm]{img/telaAbaRelaxExpression.png}
%\caption{Interface gráfica - aba \textit{Relax Expression}}
%\label{fig_implementacaoTelaGraficaRelaxExpression}
%\end{figure}

\subsection{Processamento de Expressões}

O processamento de expressão pode ser compreendido através do diagrama de atividades apresentado na~\autoref{fig_diagramaDeAtividadesProcessadorDeLinguagem}.

\begin{figure}[!htb]
\centering
\caption{Diagrama de atividades - Processador de Linguagem}
\includegraphics[width=14cm]{img/diagramaDeAtividades.png}
\label{fig_diagramaDeAtividadesProcessadorDeLinguagem}
\end{figure}

A primeira atividade é efetuar a análise sintática da expressão do requisito, caso a verificação resultar em verdadeiro a expressão Relax é gerada. A partir dessa expressão é avaliado sua léxica, posteriormente a isso sua sintaxe. A última atividade é recuperar os aspectos de expressão, esses aspectos representam tempo, frequência, quantidade ou eventos.

As verificações léxicas foram apoiadas pela utilização do gerador de analisadores léxicos JFlex, a especificação criada pode ser visualizada no~\autoref{especificacaoJFlex:apendice}. A partir dessa especificação o JFlex gera uma classe para auxiliar o processo de análise léxica de uma expressão, nessa especificação é descrito os \textit{tokens} a serem interpretados pela classe~\textit{Scanner} gerada.

Quanto as verificações de sintaxe, as mesmas foram realizadas por meio de classes analisadoras geradas pelo \textit{CUP}, o~\autoref{especificacaoCUP:apendice} apresenta a especificação CUP, nela deve ser descrito as regras de produção possibilitadas pela gramática da linguagem e deve ser definido quais são os símbolos terminais, não terminais e de precedência. Vale salientar que, a ferramenta não realiza a análise semântica das expressões geradas.


\subsection{Persistência}

A persistência da ferramenta foi implementada utilizando uma hierarquia de arquivos~\ac{xml}, esses arquivos são conectados por um arquivo principal, cada arquivo (exceto o principal) representa um requisito, todos os arquivos juntos representam uma especificação de requisitos. Na~\autoref{fig_persistenciaXML} é apresentado uma ilustração da hierarquia de persistência adotada.

\begin{figure}[!htb]
\centering
\caption{Persistência do \textit{plugin} - hierarquia de arquivos~\ac{xml}.}
\includegraphics[width=16cm, height=9cm]{img/persistenciaXML.png}
\label{fig_persistenciaXML}
\end{figure}

A persistência adotada facilita a leitura das informações, já que os nós são indexados e referenciados no arquivo principal. As \textit{tags} do arquivo requisito são id, ambiente, monitoração, descrição, relações, e dependência. As \textit{tags} ambiente e monitoração representam uma lista de elementos, já na \textit{tag} relações é representado uma estrutura de dados de tipo mapa, cada elemento contém uma chave e um valor, o valor representa dois itens, um derivado do ambiente e outro da monitoração. A dependência é também é uma lista de elementos, seu diferencial é que cada item dessa lista aponta para um arquivo requisito.

\section{Testes}\label{sectionTestes}

Na etapa de testes foram realizadas atividades de particionamento por equivalência, testes unitários e estruturais. O particionamento por equivalência divide em grupos os dados de entrada dos testes unitários de acordo com pontos de vista, a partir disso é possível visualizar o escopo de teste. Essa técnica auxilia a implementação dos testes unitários (caixa preta), os quais tem por objetivo analisar as saídas geradas, levando em consideração apenas as suas entradas.

Já a análise de cobertura é uma técnica de teste estrutural (caixa-branca) que tem por objetivo analisar a estrutura do código, através dela é possível visualizar quais estruturas ou comandos foram executados, a partir disso é possível determinar a porcentagem de eficiência dos testes unitários implementados.

\subsection{Particionamento por Equivalência}

A~\autoref{fig_particionamentoPorEquivalencia} apresenta as classes de equivalência e o número de casos de teste escritos para cada classe, ao total foram escritos 106 casos de teste, todos os testes executados foram aprovados, retornando o resultado esperado.

\begin{table}[!htb]
\footnotesize
\caption[Particionamento por equivalência.]{Particionamento por equivalência.}
\begin{center}
\label{fig_particionamentoPorEquivalencia}
\begin{tabular}{m{10.5cm}m{3.5cm}}
  \toprule
  \textbf{Classe de Equivalência} & \textbf{Testes Escritos} \\
  \midrule
  Operadores com frequência ou quantidade escrita numérica & 9 \\
  Operadores com frequência ou quantidade de escrita somente texto & 3 \\
  Operadores com tempo utilizando limitador ":" & 2 \\
  Operadores com tempo utilizando nenhum limitador & 4 
  \\
  Operadores com tempo utilizando o valor por extenso & 2 \\
  Operador \textit{BEFORE} com evento em verbo & 3 \\
  Operador \textit{AFTER} com evento em verbo & 3 \\
  Operador \textit{BEFORE} ou \textit{AFTER} com mais de um evento associado & 3 \\
  Preposições sem operadores associados & 9 \\
  Expressão com operadores em \textit{UpperCase} & 3 \\
  Expressão com operadores em \textit{MixedCase} & 3 \\
  Expressão com operadores em \textit{LowerCase} & 29 \\
  Expressão sem operadores & 5 \\
  Expressão com um ou mais operadores & 28 \\
  \bottomrule
\end{tabular}
\end{center}
\end{table}

%\begin{figure}[!htb]
%\centering
%\includegraphics[width=16cm, height=12cm]{img/particionamentoPorEquivalencia.png}
%\caption{Particionamento por equivalência.}
%\label{fig_particionamentoPorEquivalencia}
%\end{figure}

\subsection{Análise de Cobertura}

O \textit{plugin} EclEmma 2.3.2 (disponível para a plataforma Eclipse) foi utilizado para analisar a cobertura dos testes unitários construídos, esse \textit{plugin} é executado juntamente com os testes unitários e verifica qual a porcentagem de cobertura das classes analisadas, sua perspectiva apresenta as estruturas exercitadas, não exercitadas e exercitadas apenas uma única vez.

A cobertura dos testes pode ser visualizada na~\autoref{tab:coberturaDosTestes}, os testes realizados para a classe "AnalisadorLexicoDeRequisito" corresponderam em 90,2\% de cobertura, já para a classe "GeradorDeExpressao", os testes cobriram cerca de 94,2\% de código, quanto a classe "Interpretador" cobriu cerca de 88,7\% de cobertura.

\begin{table}[!htb]
\footnotesize
\caption[Cobertura dos Testes]{Cobertura dos testes.}
\begin{center}
\label{tab:coberturaDosTestes}
\begin{tabular}{m{8.5cm}m{2.5cm}m{2.5cm}}
  \toprule
  \textbf{Classe} & \textbf{Estruturas Cobertas} & \textbf{Estruturas Não Cobertas}\\
  \midrule
  AnalisadorLexicoDeRequisito & 90.2\% & 9,8\% \\
  GeradorDeExpressao & 94,2\% & 5,8\% \\
  Interpretador & 88,7\% & 11,3\% \\
  \bottomrule
\end{tabular}
\end{center}
\end{table}

\section{Considerações do Capítulo}

Esse capítulo apresentou os artefatos desenvolvidos nas atividades de análise, projeto, implementação e testes. Cada atividade realizada contribuiu diretamente com o produto final, as atividades de análise possibilitaram o conhecimento da linguagem Relax e dos principais problemas que ocorrem nas atividades de análise de~\acp{sa}. Já na fase de projeto foi construído modelos estáticos e dinâmicos para propor soluções para a problemática. 

A fase de implementação possibilitou a construção da ferramenta, através dela os artefatos de projeto foram refinados de acordo com novas necessidades de desenvolvimento ou restrições impostas pela plataforma Eclipse. Soma-se a isso as atividades de teste, a cada bateria de testes a implementação era revisitada com o objetivo de garantir a robustez da ferramenta, os casos de teste foram criados racionalmente utilizando a técnica de particionamento por equivalência, após a construção dos testes unitários foi possível analisar a cobertura dos mesmos, pois em algumas baterias não exercitavam regiões importantes do código.

%como o CAPÍTULO contribuiu para o trabalho
%Através desse capítulo foi possível construir os primeiros artefatos %de desenvolvimento da ferramenta Relax~\textit{Editor} como diagrama %de caso de uso e atividades, modelagem de domínio, documentação de %caso de uso e \textit{StoryBoard}. Esses artefatos oferecem os %principais insumos para a primeira interação da fase de construção da %ferramenta, pois, foi conhecido os problemas e as necessidades do %desenvolvimento da ferramenta e foi proposto uma possível solução. A %partir disso, nas próximas interações será possível refinar a solução %apresentada e construir o produto de acordo com ela.