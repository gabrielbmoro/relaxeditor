\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{21}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivos}{22}{section.1.1}
\contentsline {section}{\numberline {1.2}Metodologia}{22}{section.1.2}
\contentsline {section}{\numberline {1.3}Organiza\IeC {\c c}\IeC {\~a}o do Documento}{23}{section.1.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica}}{25}{chapter.2}
\contentsline {section}{\numberline {2.1}Engenharia de Requisitos}{25}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Processo de Engenharia de Requisitos}{25}{subsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.1.1}Investiga\IeC {\c c}\IeC {\~a}o}{27}{subsubsection.2.1.1.1}
\contentsline {subsubsection}{\numberline {2.1.1.2}An\IeC {\'a}lise}{30}{subsubsection.2.1.1.2}
\contentsline {subsubsection}{\numberline {2.1.1.3}Especifica\IeC {\c c}\IeC {\~a}o}{31}{subsubsection.2.1.1.3}
\contentsline {subsubsection}{\numberline {2.1.1.4}Valida\IeC {\c c}\IeC {\~a}o}{32}{subsubsection.2.1.1.4}
\contentsline {section}{\numberline {2.2}Sistemas Autoadaptativos}{34}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Propriedades de Autoadapta\IeC {\c c}\IeC {\~a}o}{34}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Abordagens de Sistemas Autoadaptativos}{36}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Abordagens de apoio ao Desenvolvimento de Sistemas Autoadaptativos}{37}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Linguagem Relax}{38}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Vocabul\IeC {\'a}rio da Linguagem}{39}{subsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.1.1}Sint\IeC {\'a}xe}{41}{subsubsection.2.3.1.1}
\contentsline {subsubsection}{\numberline {2.3.1.2}Sem\IeC {\^a}ntica}{42}{subsubsection.2.3.1.2}
\contentsline {section}{\numberline {2.4}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{43}{section.2.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Base Tecnol\IeC {\'o}gica}}{45}{chapter.3}
\contentsline {section}{\numberline {3.1}Processador de Linguagem}{45}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Geradores para Criar Analisadores L\IeC {\'e}xico e de Sint\IeC {\'a}xe}{46}{subsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.1}Estrutura de uma Especifica\IeC {\c c}\IeC {\~a}o L\IeC {\'e}xica JFlex}{47}{subsubsection.3.1.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.2}Estrutura de uma Especifica\IeC {\c c}\IeC {\~a}o Sint\IeC {\'a}tica CUP}{48}{subsubsection.3.1.1.2}
\contentsline {section}{\numberline {3.2}Plataforma Eclipse para o Desenvolvimento de \textit {Plugins}}{49}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Arquitetura da Plataforma Eclipse}{49}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Arquitetura de um \textit {Plugin} Eclipse}{51}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}\textit {Plugin Development Environment}}{52}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{52}{section.3.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Trabalhos Relacionados}}{55}{chapter.4}
\contentsline {section}{\numberline {4.1}Metodologia}{55}{section.4.1}
\contentsline {section}{\numberline {4.2}Trabalhos}{55}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Ferramentas de Apoio ao Processo de Engenharia de Requisitos}{56}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Ferramentas de Apoio ao Processo de Engenharia de Requisitos para Sistemas Autoadaptativos}{57}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}An\IeC {\'a}lise}{57}{section.4.3}
\contentsline {section}{\numberline {4.4}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{58}{section.4.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Desenvolvimento da Ferramenta}}{59}{chapter.5}
\contentsline {section}{\numberline {5.1}An\IeC {\'a}lise}{59}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Hist\IeC {\'o}rias de Usu\IeC {\'a}rio}{59}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}\textit {StoryBoard}}{60}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Modelo de Caso de Uso}{61}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Projeto}{62}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Diagrama de Arquitetura}{63}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Diagrama de Classes}{63}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Diagrama de Sequ\IeC {\^e}ncia}{65}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Implementa\IeC {\c c}\IeC {\~a}o}{66}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Interface Gr\IeC {\'a}fica}{66}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Processamento de Express\IeC {\~o}es}{67}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Persist\IeC {\^e}ncia}{68}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}Testes}{68}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Particionamento por Equival\IeC {\^e}ncia}{69}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}An\IeC {\'a}lise de Cobertura}{69}{subsection.5.4.2}
\contentsline {section}{\numberline {5.5}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{70}{section.5.5}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {Experimento em Ambiente Controlado}}{73}{chapter.6}
\contentsline {section}{\numberline {6.1}Protocolo}{73}{section.6.1}
\contentsline {section}{\numberline {6.2}Resultados Obtidos}{74}{section.6.2}
\contentsline {section}{\numberline {6.3}Considera\IeC {\c c}\IeC {\~o}es do Cap\IeC {\'\i }tulo}{75}{section.6.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {7}\MakeTextUppercase {Conclus\IeC {\~o}es}}{77}{chapter.7}
\contentsline {section}{\numberline {7.1}Trabalhos Futuros}{77}{section.7.1}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{79}{section*.9}
\renewcommand *{\cftappendixname }{AP\^ENDICE \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Ap\^endices}}{83}{section*.10}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {Especifica\IeC {\c c}\IeC {\~a}o JFlex}}{85}{appendix.A}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {B}\MakeTextUppercase {Especifica\IeC {\c c}\IeC {\~a}o CUP}}{87}{appendix.B}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {C}\MakeTextUppercase {Tabela de Requisitos Utilizada no Protocolo - Experimento da Ferramenta}}{89}{appendix.C}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {D}\MakeTextUppercase {Documenta\IeC {\c c}\IeC {\~a}o de Caso de Uso}}{91}{appendix.D}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {E}\MakeTextUppercase {Question\IeC {\'a}rio Utilizado no Experimento}}{95}{appendix.E}
