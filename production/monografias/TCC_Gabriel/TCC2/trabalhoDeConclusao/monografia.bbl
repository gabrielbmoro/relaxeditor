\providecommand{\abntreprintinfo}[1]{%
 \citeonline{#1}}
\setlength{\labelsep}{0pt}\begin{thebibliography}{}
\providecommand{\abntrefinfo}[3]{}
\providecommand{\abntbstabout}[1]{}
\abntbstabout{v-1.9.2 }

\bibitem[AHO, Sethi e Lam 2008]{aho2008compiladores}
\abntrefinfo{AHO, Sethi e Lam}{AHO; SETHI; LAM}{2008}
{AHO, A.; SETHI, R.; LAM, S. \emph{Compiladores: princípios, técnicas e
  ferramentas}. Pearson Addison Wesley, 2008.
ISBN 9788588639249. Dispon{\'\i}vel em:
  \url{https://books.google.com.br/books?id=hahXPgAACAAJ}.}

\bibitem[Ali, Dalpiaz e Giorgini 2010]{ali2010reasoning}
\abntrefinfo{Ali, Dalpiaz e Giorgini}{ALI; DALPIAZ; GIORGINI}{2010}
{ALI, R.; DALPIAZ, F.; GIORGINI, P. Reasoning about contextual requirements for
  mobile information systems: a goal-based approach.
University of Trento, 2010.}

\bibitem[Ali, Dalpiaz e Giorgini 2013]{ali2013reasoning}
\abntrefinfo{Ali, Dalpiaz e Giorgini}{ALI; DALPIAZ; GIORGINI}{2013}
{ALI, R.; DALPIAZ, F.; GIORGINI, P. Reasoning with contextual requirements:
  Detecting inconsistency and conflicts.
\emph{Information and Software Technology}, Elsevier, v.~55, n.~1, p. 35--57,
  2013.}

\bibitem[Ali, Dalpiaz e Giorgini 2014]{ali2014requirements}
\abntrefinfo{Ali, Dalpiaz e Giorgini}{ALI; DALPIAZ; GIORGINI}{2014}
{ALI, R.; DALPIAZ, F.; GIORGINI, P. Requirements-driven deployment.
\emph{Software \& Systems Modeling}, Springer, v.~13, n.~1, p. 433--456, 2014.}

\bibitem[Andr{\'e} et al. 2010]{andre2010architectures}
\abntrefinfo{Andr{\'e} et al.}{ANDR{\'E} et al.}{2010}
{ANDR{\'E}, F. et al. Architectures \& infrastructure. In:  \emph{Service
  research challenges and solutions for the future internet}. [S.l.]: Springer,
  2010. p. 85--116.}

\bibitem[Azevedo et al. 2011]{azevedointegrative}
\abntrefinfo{Azevedo et al.}{AZEVEDO et al.}{2011}
{AZEVEDO, D. et al. An integrative approach to diagram-based collaborative
  brainstorming: a case study.
2011.}

\bibitem[Balsiger 2010]{apostilaEclipseI}
\abntrefinfo{Balsiger}{BALSIGER}{2010}
{BALSIGER, M. A quick-start tutorial to eclipse plug-in development.
Universität Bern, 2010.}

\bibitem[Beckers et al. 2013]{beckers2013structured}
\abntrefinfo{Beckers et al.}{BECKERS et al.}{2013}
{BECKERS, K. et al. Structured pattern-based security requirements elicitation
  for clouds. In:  IEEE. \emph{Availability, Reliability and Security (ARES),
  2013 Eighth International Conference on}. [S.l.], 2013. p. 465--474.}

\bibitem[Bencomo et al. 2012]{bencomo2012self}
\abntrefinfo{Bencomo et al.}{BENCOMO et al.}{2012}
{BENCOMO, N. et al. Self-explanation in adaptive systems. In:  IEEE.
  \emph{Engineering of Complex Computer Systems (ICECCS), 2012 17th
  International Conference on}. [S.l.], 2012. p. 157--166.}

\bibitem[Bencomo et al. 2010]{bencomo2010requirements}
\abntrefinfo{Bencomo et al.}{BENCOMO et al.}{2010}
{BENCOMO, N. et al. Requirements reflection: requirements as runtime entities.
  In:  ACM. \emph{Proceedings of the 32nd ACM/IEEE International Conference on
  Software Engineering-Volume 2}. [S.l.], 2010. p. 199--202.}

\bibitem[Bilobrovec e Kovaleski 2004]{bilobrovec2004implementaccao}
\abntrefinfo{Bilobrovec e Kovaleski}{BILOBROVEC; KOVALESKI}{2004}
{BILOBROVEC, R. F. M.~M. M.; KOVALESKI, J.~L. Implementação de um sistema de
  controle inteligente utilizando a lógica fuzzy.
\emph{XI SIMPEP, Bauru/Brasil}, 2004.}

\bibitem[Brun et al. 2009]{brun2009engineering}
\abntrefinfo{Brun et al.}{BRUN et al.}{2009}
{BRUN, Y. et al. Engineering self-adaptive systems through feedback loops. In:
  \emph{Software engineering for self-adaptive systems}. [S.l.]: Springer,
  2009. p. 48--70.}

\bibitem[Butler e TSSG 2011]{butlersecure}
\abntrefinfo{Butler e TSSG}{BUTLER; TSSG}{2011}
{BUTLER, B.; TSSG, A.~B. Secure and trustworthy composite services.
2011.}

\bibitem[Cheng et al. 2009]{cheng2009software}
\abntrefinfo{Cheng et al.}{CHENG et al.}{2009}
{CHENG, B.~H. et al. Software engineering for self-adaptive systems: A research
  roadmap. In:  \emph{Software engineering for self-adaptive systems}. [S.l.]:
  Springer, 2009. p. 1--26.}

\bibitem[Daramola e Stalhane 2012]{daramolaSindreStalhane2012}
\abntrefinfo{Daramola e Stalhane}{DARAMOLA; STALHANE}{2012}
{DARAMOLA, G.~S. O.; STALHANE, T. Pattern-based security requirements
  specification using ontologies and boilerplates.
2012.}

\bibitem[Daramola Tor~Stalhane e Omoronyia
  2011]{daramolaSatalhaneSindreOmoronia2011}
\abntrefinfo{Daramola Tor~Stalhane e Omoronyia}{DARAMOLA TOR~STALHANE;
  OMORONYIA}{2011}
{DARAMOLA TOR~STALHANE, G.~S. O.; OMORONYIA, I. Enabling hazard identification
  from requirements and reuse-oriented hazop analysis.
2011.}

\bibitem[Deitel 2010]{deitel2010java}
\abntrefinfo{Deitel}{DEITEL}{2010}
{DEITEL, H. \emph{Java: Como programar}. PRENTICE HALL BRASIL, 2010.
ISBN 9788576050193. Dispon{\'\i}vel em:
  \url{https://books.google.com.br/books?id=U5AyAgAACAAJ}.}

\bibitem[desRivieres e Wiegand 2004]{desrivieres2004eclipse}
\abntrefinfo{desRivieres e Wiegand}{DESRIVIERES; WIEGAND}{2004}
{DESRIVIERES, J.; WIEGAND, J. Eclipse: A platform for integrating development
  tools.
\emph{IBM Systems Journal}, IBM, v.~43, n.~2, p. 371--383, 2004.}

\bibitem[Eclipse Foundation 2014]{ecliseDocumentationLuna}
\abntrefinfo{Eclipse Foundation}{ECLIPSE FOUNDATION}{2014}
{ECLIPSE FOUNDATION. Eclipse documentation - current release.
2014.}

\bibitem[Fredericks, DeVries e Cheng 2014]{fredericks2014autorelax}
\abntrefinfo{Fredericks, DeVries e Cheng}{FREDERICKS; DEVRIES; CHENG}{2014}
{FREDERICKS, E.~M.; DEVRIES, B.; CHENG, B.~H. Autorelax: automatically relaxing
  a goal model to address uncertainty.
\emph{Empirical Software Engineering}, Springer, p. 1--36, 2014.}

\bibitem[Herczeg 2010]{herczeg2010smart}
\abntrefinfo{Herczeg}{HERCZEG}{2010}
{HERCZEG, M. The smart, the intelligent and the wise: roles and values of
  interactive technologies. In:  ACM. \emph{Proceedings of the First
  International Conference on Intelligent Interactive Technologies and
  Multimedia}. [S.l.], 2010. p. 17--26.}

\bibitem[Hudson 1999]{manualcupparausuario}
\abntrefinfo{Hudson}{HUDSON}{1999}
{HUDSON, S.~E. \emph{CUP User's Manual}. [s.n.], 1999. Dispon{\'\i}vel em:
  \url{http://www.cs.princeton.edu/~appel/modern/java/CUP/manual.html}.}

\bibitem[Huebscher e McCann 2008]{huebscher2008survey}
\abntrefinfo{Huebscher e McCann}{HUEBSCHER; MCCANN}{2008}
{HUEBSCHER, M.~C.; MCCANN, J.~A. A survey of autonomic computing—degrees,
  models, and applications.
\emph{ACM Computing Surveys (CSUR)}, ACM, v.~40, n.~3, p.~7, 2008.}

\bibitem[Hussein et al. 2013]{hussein2013scenario}
\abntrefinfo{Hussein et al.}{HUSSEIN et al.}{2013}
{HUSSEIN, M. et al. Scenario-based validation of requirements for context-aware
  adaptive services. In:  IEEE. \emph{Web Services (ICWS), 2013 IEEE 20th
  International Conference on}. [S.l.], 2013. p. 348--355.}

\bibitem[Ito et al. 2011]{ito2011support}
\abntrefinfo{Ito et al.}{ITO et al.}{2011}
{ITO, M. et al. Support tool to the validation process of functional
  requirements.
\emph{Latin America Transactions, IEEE (Revista IEEE America Latina)}, IEEE,
  v.~9, n.~5, p. 889--894, 2011.}

\bibitem[Jerome e Kaashoek 2009]{saltzer2009principles}
\abntrefinfo{Jerome e Kaashoek}{JEROME; KAASHOEK}{2009}
{JEROME, H. S.; KAASHOEK, M. F. \emph{Principles of computer system design: an
  introduction}. [S.l.]: Morgan Kaufmann, 2009.}

\bibitem[Junior 2013]{daengenharia}
\abntrefinfo{Junior}{JUNIOR}{2013}
{JUNIOR, O.~A. de A. Engenharia de requisitos para sistemas auto-adaptativos.
2013.}

\bibitem[Klein e Décamps 2015]{manualjflexparausuario}
\abntrefinfo{Klein e Décamps}{KLEIN; DéCAMPS}{2015}
{KLEIN, S.~R. G.; DéCAMPS, R. \emph{JFlex User's Manual}. [s.n.], 2015.
  Dispon{\'\i}vel em: \url{http://jflex.de/}.}

\bibitem[MK e S 2009]{zarinah2009supporting}
\abntrefinfo{MK e S}{MK; S}{2009}
{MK, Z.~M. Z.; S, S. S.~S. S.~S. Supporting collaborative requirements
  elicitation using focus group discussion technique.
\emph{International Journal of Software Engineering and Its Applications},
  v.~3, n.~3, p. 59--70, 2009.}

\bibitem[Moraes 2009]{aanalise}
\abntrefinfo{Moraes}{MORAES}{2009}
{MORAES, J. B.~D. Análise de pontos de função.
\emph{Engenharia de Software Magazine}, p.~54, 2009.}

\bibitem[Pfleeger e Atlee 2004]{pfleeger2004software}
\abntrefinfo{Pfleeger e Atlee}{PFLEEGER; ATLEE}{2004}
{PFLEEGER, S.; ATLEE, J. \emph{Software Engineering: Theory and Practice}.
  [S.l.]: Prentice Hall, 2004.}

\bibitem[Pfleeger e Atlee 2010]{pfleeger2010software}
\abntrefinfo{Pfleeger e Atlee}{PFLEEGER; ATLEE}{2010}
{PFLEEGER, S.; ATLEE, J. \emph{Software Engineering: Theory and Practice}.
  Prentice Hall, 2010.
ISBN 9780136061694. Dispon{\'\i}vel em:
  \url{http://books.google.ca/books?id=7zbSZ54JG1wC}.}

\bibitem[Pimentel 2008]{abordagensDeEngDeReqComAuto}
\abntrefinfo{Pimentel}{PIMENTEL}{2008}
{PIMENTEL, J. H.~C. Abordagens de engenharia de requisitos para computação
  autonômica.
2008.}

\bibitem[Pressman 2011]{pressman2011engenharia}
\abntrefinfo{Pressman}{PRESSMAN}{2011}
{PRESSMAN, R. \emph{Engenharia de Software}. McGraw Hill Brasil, 2011.
ISBN 9788580550443. Dispon{\'\i}vel em:
  \url{http://books.google.com.br/books?id=y0rH9wuXe68C}.}

\bibitem[Qureshi e Perini 2009]{qureshi2009engineering}
\abntrefinfo{Qureshi e Perini}{QURESHI; PERINI}{2009}
{QURESHI, N.~A.; PERINI, A. Engineering adaptive requirements. In:  IEEE.
  \emph{Software Engineering for Adaptive and Self-Managing Systems, 2009.
  SEAMS'09. ICSE Workshop on}. [S.l.], 2009. p. 126--131.}

\bibitem[Qureshi e Perini 2010]{qureshi2010continuous}
\abntrefinfo{Qureshi e Perini}{QURESHI; PERINI}{2010a}
{QURESHI, N.~A.; PERINI, A. Continuous adaptive requirements engineering: An
  architecture for self-adaptive service-based applications. In:  IEEE.
  \emph{Requirements@ Run. Time (RE@ RunTime), 2010 First International
  Workshop on}. [S.l.], 2010. p. 17--24.}

\bibitem[Qureshi e Perini 2010]{qureshi2010requirements}
\abntrefinfo{Qureshi e Perini}{QURESHI; PERINI}{2010b}
{QURESHI, N.~A.; PERINI, A. Requirements engineering for adaptive service based
  applications. In:  IEEE. \emph{Requirements Engineering Conference (RE), 2010
  18th IEEE International}. [S.l.], 2010. p. 108--111.}

\bibitem[Sawyer et al. 2010]{sawyer2010requirements}
\abntrefinfo{Sawyer et al.}{SAWYER et al.}{2010}
{SAWYER, P. et al. Requirements-aware systems: A research agenda for re for
  self-adaptive systems. In:  IEEE. \emph{Requirements Engineering Conference
  (RE), 2010 18th IEEE International}. [S.l.], 2010. p. 95--103.}

\bibitem[Sommerville 2011]{sommervilleengenharia}
\abntrefinfo{Sommerville}{SOMMERVILLE}{2011}
{SOMMERVILLE, I. \emph{Engenharia de Software}. Pearson Brasil, 2011.
ISBN 9788579361081. Dispon{\'\i}vel em:
  \url{http://books.google.com.br/books?id=H4u5ygAACAAJ}.}

\bibitem[Suri e Cabri 2014]{adaptiveDynamicAndResilientSystem}
\abntrefinfo{Suri e Cabri}{SURI; CABRI}{2014}
{SURI, N.; CABRI, G. \emph{Adaptive, Dynamic and Resilient Systems}. [S.l.]:
  CRC Press, 2014.}

\bibitem[Umoh, Sampaio e Theodoulidis 2011]{umoh2011refinto}
\abntrefinfo{Umoh, Sampaio e Theodoulidis}{UMOH; SAMPAIO; THEODOULIDIS}{2011}
{UMOH, E.; SAMPAIO, P. R.~F.; THEODOULIDIS, B. Refinto: An ontology-based
  requirements engineering framework for business-it alignment in financial
  services organizations. In:  IEEE. \emph{Services Computing (SCC), 2011 IEEE
  International Conference on}. [S.l.], 2011. p. 600--607.}

\bibitem[Varela João~Araújo 2011]{varelaAraujoBritoMoreira2011}
\abntrefinfo{Varela João~Araújo}{VARELA JOãO~ARAúJO}{2011}
{VARELA JOãO~ARAúJO, I. B. e. A.~M. P. Aspect-oriented analysis for software
  product lines requirements engineering.
2011.}

\bibitem[Wazlawick 2004]{wazlawick2004analise}
\abntrefinfo{Wazlawick}{WAZLAWICK}{2004}
{WAZLAWICK, R.~S. \emph{An{\'a}lise e Projeto de Sistemas da
  Informa{\c{c}}{\~a}o}. [S.l.]: Elsevier Brasil, 2004.}

\bibitem[Welsh e Sawyer 2010]{welsh2010understanding}
\abntrefinfo{Welsh e Sawyer}{WELSH; SAWYER}{2010}
{WELSH, K.; SAWYER, P. Understanding the scope of uncertainty in dynamically
  adaptive systems. In:  \emph{Requirements Engineering: Foundation for
  Software Quality}. [S.l.]: Springer, 2010. p. 2--16.}

\bibitem[Whittle et al. 2010]{whittle2010relax}
\abntrefinfo{Whittle et al.}{WHITTLE et al.}{2010}
{WHITTLE, J. et al. Relax: a language to address uncertainty in self-adaptive
  systems requirement.
\emph{Requirements Engineering}, Springer, v.~15, n.~2, p. 177--196, 2010.}

\end{thebibliography}
