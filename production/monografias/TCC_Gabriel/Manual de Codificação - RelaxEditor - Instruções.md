## Manual de Codificação - RelaxEditor##

**RelaxEditor** é um plugin para a especificação de requisitos em linguagem Relax.


**Autores:** Gabriel Bronzatti Moro, Douglas Freitas Almeida.

**Orientador:** João Pablo Silva da Silva

### Tópicos a serem observados ###

**I. Arquitetura** 

A arquitetura foi definida utilizando os padrão MVC (*Model-View-Controller*). Deverá ser seguido um padrão de nomes para os módulos da arquitetura, para *Model* deverá ser nomeado para **br.unipampa.lesa.modelo**, já para *Controller* o nome **br.unipampa.lesa.controle** e para *View* a descrição **br.unipampa.lesa.apresentacao**.


Toda classe criada deverá possuir o seguinte formato de nome: 
<ol>
<li> iniciando por letras maiúsculas;</li> 
<li> o nome da classe é o mesmo nome do arquivo;</li> 
<li> não utilizar mais de uma classe por arquivo;</li>
<li> o caracter utilizado para descrever nomes compostos deverá ser em maiúsculo e não *underline*, por exemplo: "DocumentoRelax" e não "Documento_Relax".</li>
</ol>

**II. Métodos**

Devem ser escrios em letra minúscula, poderá ser utilizado letra maiúscula apenas para mais de uma palavra, por exemplo: "salvarArquivo", "abrirDiretorio" e assim por diante.

**III. Atributos**

Devem ser escritos em letras minúsculas, como os métodos podem ser utilizados o carácter maiúsculo para separar palavras compostas. Em situações de *enums* e utilização de constantes deverá ser utilizado letras maiúsculas para escrita, lembrando uma constante é estática e final.

**IV. Comentários**

Os comentários devem ser precedidos de duas barras "//". Deve ser utilizado o seguinte padrão: 
<ol>
<li>Erros com exeções: "//Exception: NullPointerException, observações";</li> <li>Erros sem exeções mas a causa é conhecida pelo programador: "//Erro: Motivo, observações";</li> 
<li>Erros sem exeções mas a causa é desconhecida pelo programador: "//Erro: observações";</li> 
<li>Lembretes para demais membros da equipe de desenvolvimento: "//Lembrete: observações";</li> 
<li>Código comentado deverá ser descrito da seguinte maneira: "//Código em manutenção, por nomeDoDesenvolvedor".</li> 
</ol> 

**V. Documentação de Código-Fonte**

Para documentação de **classes** deverá ser descrito o autor, data e versão, anterior a classe essas informações deverão aparecer da seguinte maneira:
 
	 /** NomeDaClasse.java
	<ul> 
		* <li><b>Propósito:</b></li> 
		* Apresentar o propósito da classe.
		* <li><b>Instruções de uso:</b></li>
		* Explicar como ela deverá ser utilizada. 
		* </ul>
		* @author Nome da Pessoa
		* @version 1.0
		* @since 19/11/2014 
		*/

Para documentar os métodos deverá apresentar o seu propósito e explicar sobre seus parametros *@param* e retornos *@return*, para métodos deprecados deverá ser sinalizado *@deprecated* e informar qual é o método mais indicado para sua utilização. Por exemplo: 

	/** nomeDoMetodo 
		<ul> 
		* <li><b>Propósito</b></li> 
		* Esse método é responsável por gravar uma descrição utilizada pela 
		* propriedade de {@link Monitoracao}. 
		</ul> 
		* @param caracteristica 
		* deve ser de tipo {@link String} e possuir uma característica 
		* válida de requisito. */
