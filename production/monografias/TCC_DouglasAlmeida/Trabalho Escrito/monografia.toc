\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{15}{chapter.1}
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o}{16}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivo}{17}{section.1.2}
\contentsline {section}{\numberline {1.3}Metodologia de Trabalho}{17}{section.1.3}
\contentsline {section}{\numberline {1.4}Organiza\IeC {\c c}\IeC {\~a}o do Documento}{18}{section.1.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica}}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}Engenharia de Requisitos}{19}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Processo de Engenharia de Requisitos}{19}{subsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.1.1}Investiga\IeC {\c c}\IeC {\~a}o}{22}{subsubsection.2.1.1.1}
\contentsline {subsubsection}{\numberline {2.1.1.2}An\IeC {\'a}lise}{22}{subsubsection.2.1.1.2}
\contentsline {subsubsection}{\numberline {2.1.1.3}Especifica\IeC {\c c}\IeC {\~a}o}{24}{subsubsection.2.1.1.3}
\contentsline {subsubsection}{\numberline {2.1.1.4}Valida\IeC {\c c}\IeC {\~a}o}{25}{subsubsection.2.1.1.4}
\contentsline {section}{\numberline {2.2}Sistemas Autoadaptativos}{26}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Propriedades dos Sistemas Autoadaptativos}{26}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Engenharia de Requisitos para Sistemas Autoadaptativos}{28}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Unified Modeling Language}{30}{section.2.3}
\contentsline {section}{\numberline {2.4}Conclus\IeC {\~a}o do Cap\IeC {\'\i }tulo}{31}{section.2.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {M\IeC {\'e}todos e Recursos}}{33}{chapter.3}
\contentsline {section}{\numberline {3.1}Pr\IeC {\'a}ticas da Engenharia de Software}{33}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}\textit {Rational Unified Process}}{33}{subsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.1}Fases do Processo Unificado}{34}{subsubsection.3.1.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.2}Disciplinas do Processo Unificado}{35}{subsubsection.3.1.1.2}
\contentsline {subsection}{\numberline {3.1.2}\textit {User Stories}}{35}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}\textit {StoryBoards}}{36}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Plataforma Eclipse}{37}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Arquitetura da Plataforma Eclipse}{37}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Arquitetura de Plugins da Plataforma Eclipse}{38}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}\textit {Plugin Development Environment}}{39}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}\textit {Eclipse Modeling Framework}}{40}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Arquitetura do EMF}{41}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Arquitetura B\IeC {\'a}sica de um Projeto EMF}{41}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Conclus\IeC {\~a}o do Cap\IeC {\'\i }tulo}{42}{section.3.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Trabalhos Relacionados}}{43}{chapter.4}
\contentsline {section}{\numberline {4.1}Metodologia}{43}{section.4.1}
\contentsline {section}{\numberline {4.2}Resultados}{44}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Quais s\IeC {\~a}o as solu\IeC {\c c}\IeC {\~o}es de suporte a modelagem de requisitos?}{44}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Quais s\IeC {\~a}o as solu\IeC {\c c}\IeC {\~o}es de suporte a classifica\IeC {\c c}\IeC {\~a}o de requisitos?}{45}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Quais s\IeC {\~a}o as solu\IeC {\c c}\IeC {\~o}es de suporte a negocia\IeC {\c c}\IeC {\~a}o de requisitos?}{45}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Conclus\IeC {\~a}o do Cap\IeC {\'\i }tulo}{46}{section.4.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Desenvolvimento Preliminar}}{47}{chapter.5}
\contentsline {section}{\numberline {5.1}Fase de Concep\IeC {\c c}\IeC {\~a}o}{47}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Descri\IeC {\c c}\IeC {\~a}o do Problema}{47}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Descri\IeC {\c c}\IeC {\~a}o da Solu\IeC {\c c}\IeC {\~a}o}{48}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Vis\IeC {\~a}o Geral}{48}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}\textit {User Stories}}{49}{subsection.5.1.4}
\contentsline {section}{\numberline {5.2}Fase de Elabora\IeC {\c c}\IeC {\~a}o}{49}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Arquitetura da Ferramenta}{49}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}\textit {StoryBoard} da Ferramenta}{50}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Conclus\IeC {\~a}o do Cap\IeC {\'\i }tulo}{51}{subsection.5.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {Conclus\IeC {\~o}es Preliminares}}{53}{chapter.6}
\contentsline {subsection}{\numberline {6.0.4}Cronograma de Atividades}{53}{subsection.6.0.4}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{55}{section*.7}
\renewcommand *{\cftappendixname }{AP\^ENDICE \space }
