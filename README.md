# **RELAX Project**

## **Student:** Gabriel Bronzatti Moro                              
**Advisor:** João Pablo Silva da Silva

- The repository is organized as follows:
        
1. support - folder with research materials
2. production - folder with materials produced
3. engineering - artifacts of software engineering
4. development - artifacts of source code

- To more informations, the labbook.org file shall be access