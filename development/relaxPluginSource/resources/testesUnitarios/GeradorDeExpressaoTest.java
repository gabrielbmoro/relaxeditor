package testesUnitarios;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import br.unipampa.lesa.modelo.GeradorDeExpressao;
import br.unipampa.lesa.modelo.Requisito;

public class GeradorDeExpressaoTest {

	private GeradorDeExpressao geradorDeExpressao;
	private Requisito requisito;
	
	@Before
	public void setUp() throws Exception {
		this.geradorDeExpressao = new GeradorDeExpressao();
		this.requisito = new Requisito();
	}

	@Test
	public void testGerarExpressaoRelax(){
		System.err.println("testGerarExpressaoRelax");
		String expressaoDeRequisito =  "The system SHALL perform all the calculations AS MANY AS POSSIBLE calls occur";
		String expressaoRelax = "SHALL (AS MANY AS POSSIBLE p)";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax1(){
		System.err.println("testGerarExpressaoRelax1");
		String expressaoDeRequisito =  "The system SHALL perform all the calculation";
		String expressaoRelax = "SHALL p";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax2(){
		System.err.println("testGerarExpressaoRelax2");
		String expressaoDeRequisito =  "The system perform all the calculation";
		String expressaoRelax = "";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax3(){
		System.err.println("testGerarExpressaoRelax3");
		String expressaoDeRequisito = "O sistema SHALL realizar inumeros calculos por segundo";
		String expressaoRelax = "SHALL p";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax4(){
		System.err.println("testGerarExpressaoRelax4");
		String expressaoDeRequisito = "O sistema shall realizar inumeros calculos por segundo";
		String expressaoRelax = "SHALL p";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax5(){
		System.err.println("testGerarExpressaoRelax5");
		String expressaoDeRequisito = "The system shall make is it convert for true";
		String expressaoRelax = "SHALL true";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax6(){
		System.err.println("testGerarExpressaoRelax6");
		String expressaoDeRequisito = "The system shall make is it convert for false";
		String expressaoRelax = "SHALL false";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax7(){
		System.err.println("testGerarExpressaoRelax7");
		String expressaoDeRequisito = "The system before make is it convert for as many as possible forever";
		String expressaoRelax = "BEFORE e (AS MANY AS POSSIBLE p)";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax8(){
		System.err.println("testGerarExpressaoRelax8");
		String expressaoDeRequisito = "The system before make is it convert for forever";
		String expressaoRelax = "BEFORE e p";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax9(){
		System.err.println("testGerarExpressaoRelax9");
		String expressaoDeRequisito = "The system before make is it convert for forever";
		String expressaoRelax = "BEFORE e p";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax10(){
		System.err.println("testGerarExpressaoRelax10");
		String expressaoDeRequisito = "The system after make is it convert for forever";
		String expressaoRelax = "AFTER e p";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax11(){
		System.err.println("testGerarExpressaoRelax11");
		String expressaoDeRequisito = "The system before make is it convert for forever true";
		String expressaoRelax = "BEFORE e true";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax12(){
		System.err.println("testGerarExpressaoRelax12");
		String expressaoDeRequisito = "The system before make is it convert for forever false";
		String expressaoRelax = "BEFORE e false";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax13(){
		System.err.println("testGerarExpressaoRelax13");
		String expressaoDeRequisito = "The system after make is it convert for forever true";
		String expressaoRelax = "AFTER e true";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax14(){
		System.err.println("testGerarExpressaoRelax14");
		String expressaoDeRequisito = "The system after make is it convert for forever false";
		String expressaoRelax = "AFTER e false";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax15(){
		System.err.println("testGerarExpressaoRelax15");
		String expressaoDeRequisito = "The system shall make convert after computing of data for forever false";
		String expressaoRelax = "SHALL (AFTER e false)";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax16(){
		System.err.println("testGerarExpressaoRelax16");
		String expressaoDeRequisito = "O sistema shall cadastrar o acervo after do amanhecer de acordo as many as possible das condi��es climaticas.";
		String expressaoRelax = "SHALL (AFTER e (AS MANY AS POSSIBLE p))";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax17(){
		System.err.println("testGerarExpressaoRelax17");
		String expressaoDeRequisito = "shall cadastrar o acervo after do amanhecer de acordo as many as possible das condi��es climaticas.";
		String expressaoRelax = "SHALL (AFTER e (AS MANY AS POSSIBLE p))";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax18(){
		System.err.println("testGerarExpressaoRelax18");
		String expressaoDeRequisito = "shall cadastrar o acervo do amanhecer de acordo until das condi��es climaticas.";
		String expressaoRelax = "SHALL (UNTIL p)";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax19(){
		System.err.println("testGerarExpressaoRelax19");
		String expressaoDeRequisito = "O sistema deve true o acervo until amanhecer de acordo das condi��es climaticas.";
		String expressaoRelax = "true UNTIL p";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax20(){
		System.err.println("testGerarExpressaoRelax20");
		String expressaoDeRequisito = "O sistema deve o acervo until amanhecer de acordo das condi��es climaticas.";
		String expressaoRelax = "p UNTIL p";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax21(){
		System.err.println("testGerarExpressaoRelax21");
		String expressaoDeRequisito = "O sistema deve cadastrar o false o acervo until amanhecer de acordo das condi��es climaticas.";
		String expressaoRelax = "false UNTIL p";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax22(){
		System.err.println("testGerarExpressaoRelax22");
		String expressaoDeRequisito = "O sistema deve cadastrar o false o acervo until amanhecer de acordo das false condi��es climaticas.";
		String expressaoRelax = "false UNTIL false";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	@Test
	public void testGerarExpressaoRelax23(){
		System.err.println("testGerarExpressaoRelax23");
		String expressaoDeRequisito = "O sistema de telhado SHALL analisar AS CLOSE AS POSSIBLE TO 30 IN 1 hora para identificar se existe umidade temporal correspondente 30 % que "
				+ "o telhado fechar� para que proteja o "
				+ "ch�o da �gua da chuva";
		String expressaoRelax = "SHALL (AS CLOSE AS POSSIBLE TO (f or q) (IN t p))";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	
	@Test
	public void testGerarExpressaoRelax24(){
		System.err.println("testGerarExpressaoRelax24");
		String expressaoDeRequisito = "O sistema de telhado SHALL analisar AS CLOSE AS POSSIBLE TO  30  IN 1 hora para identificar se existe "
				+ "umidade temporal correspondente 30  que o telhado fechara para que"
				+ "proteja o chao da agua da chuva.";
		String expressaoRelax = "SHALL (AS CLOSE AS POSSIBLE TO (f or q) (IN t p))";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	
	@Test
	public void testGerarExpressaoRelax25(){
		System.err.println("testGerarExpressaoRelax25");
		String expressaoDeRequisito = "O sistema de telhado SHALL analisar AS CLOSE AS POSSIBLE TO  30  IN 1 hora para identificar se existe in "
				+ "umidade temporal correspondente 30  que o telhado fechara para que"
				+ "proteja o chao da agua da chuva.";
		String expressaoRelax = "SHALL (AS CLOSE AS POSSIBLE TO (f or q) (IN t p))";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}

	@Test
	public void testGerarExpressaoRelax26(){
		System.err.println("testGerarExpressaoRelax26");
		String expressaoDeRequisito = "O sistema de telhado deve analisar AS CLOSE AS POSSIBLE TO  30  IN 1 hora para identificar se existe in "
				+ "umidade temporal correspondente 30  que o telhado fechara para que"
				+ "proteja o chao da agua da chuva.";
		String expressaoRelax = "AS CLOSE AS POSSIBLE TO (f or q) (IN t p)";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	
	@Test
	public void testGerarExpressaoRelax27(){
		System.err.println("testGerarExpressaoRelax27");
		String expressaoDeRequisito = "O sistema de telhado deve analisar CLOSE AS POSSIBLE TO 30 IN 1 hora para identificar se existe in "
				+ "umidade temporal correspondente 30 que o telhado fechara para que"
				+ "proteja o chao da agua da chuva.";
		String expressaoRelax = "IN t p";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}

	@Test
	public void testGerarExpressaoRelax28(){
		System.err.println("testGerarExpressaoRelax28");
		String expressaoDeRequisito = "O sistema de telhado deve analisar AS CLOSE AS POSSIBLE TO 30 in hora para identificar se existe in "
				+ "umidade temporal correspondente 30 que o telhado fechara para que"
				+ "proteja o chao da agua da chuva.";
		String expressaoRelax = "AS CLOSE AS POSSIBLE TO (f or q) (IN t p)";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	
	@Test
	public void testGerarExpressaoRelax29(){
		System.err.println("testGerarExpressaoRelax29");
		String expressaoDeRequisito = "O sistema de telhado deve analisar CLOSE AS POSSIBLE TO 30 in hora para identificar se existe in "
				+ "umidade temporal correspondente 30 que o telhado fechara para que"
				+ "proteja o chao da agua da chuva.";
		String expressaoRelax = "IN t p";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	
	@Test
	public void testGerarExpressaoRelax30(){
		System.err.println("testGerarExpressaoRelax30");
		String expressaoDeRequisito = "O sistema de telhado deve analisar CLOSE AS POSSIBLE TO 30 hora para identificar se existe "
				+ "umidade temporal correspondente 30 que o telhado fechara para que"
				+ "proteja o chao da agua da chuva, may algumas or nenhuma porcentagem";
		String expressaoRelax = "MAY p (OR p)";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	
	@Test
	public void testGerarExpressaoRelax31(){
		System.err.println("testGerarExpressaoRelax31");
		String expressaoDeRequisito = "O sistema de telhado deve analisar CLOSE AS POSSIBLE TO 30 hora para identificar se existe "
				+ "umidade temporal correspondente 30 que o telhado fechara para que"
				+ "proteja o chao da agua da chuva, may algumas or nenhuma porcentagem";
		String expressaoRelax = "MAY p (OR p)";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
	
	@Test
	public void testGerarExpressaoRelax32(){
		System.err.println("testGerarExpressaoRelax32");
		String expressaoDeRequisito = "O sistema de telhado deve analisar AS CLOSE AS POSSIBLE TO 30 hora para identificar se existe "
				+ "umidade temporal correspondente 30 que o telhado fechara para que"
				+ "proteja o chao da agua da chuva, may algumas or nenhuma porcentagem";
		String expressaoRelax = "AS CLOSE AS POSSIBLE TO (f or q) (MAY p (OR p))";
		requisito.setDescricao(expressaoDeRequisito);
		String expressaoGerada = geradorDeExpressao.gerarExpressaoRelax(requisito);
		assertEquals(expressaoRelax, expressaoGerada);
	}
}
