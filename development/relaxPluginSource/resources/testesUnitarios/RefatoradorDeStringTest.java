package testesUnitarios;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.unipampa.lesa.servico.RefatoradorDeString;

public class RefatoradorDeStringTest extends RefatoradorDeString {
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSubstituirCaracteresBrPorEng() {
		System.err.println("testSubstituirCaracteresBrPorEng");
		String expressaoDeRequisito =  "2 + 3 é 5";
		String resultadoEsperado =  "2 + 3 e 5";
		assertEquals(resultadoEsperado,
				RefatoradorDeString.substituirCaracteresBrPorEng(expressaoDeRequisito));
	}
	@Test
	public void testSubstituirCaracteresBrPorEng1() {
		System.err.println("testSubstituirCaracteresBrPorEng1");
		String expressaoDeRequisito =  "2 + 3 é 5";
		String resultadoEsperado =  "2 + 3 e 5";
		assertEquals(resultadoEsperado,
				RefatoradorDeString.substituirCaracteresBrPorEng(expressaoDeRequisito));
	}
	@Test
	public void testSubstituirCaracteresBrPorEng2() {
		System.err.println("testSubstituirCaracteresBrPorEng2");
		String expressaoDeRequisito =  "A atividade é caçar";
		String resultadoEsperado =  "A atividade e cacar";
		assertEquals(resultadoEsperado,
				RefatoradorDeString.substituirCaracteresBrPorEng(expressaoDeRequisito));
	}
	@Test
	public void testSubstituirCaracteresBrPorEng3() {
		System.err.println("testSubstituirCaracteresBrPorEng3");
		String expressaoDeRequisito =  "A atividade e `~";
		String resultadoEsperado =  "A atividade e ";
		assertEquals(resultadoEsperado,
				RefatoradorDeString.substituirCaracteresBrPorEng(expressaoDeRequisito));
	}
	@Test
	public void testSubstituirCaracteresBrPorEng4() {
		System.err.println("testSubstituirCaracteresBrPorEng4");
		String expressaoDeRequisito =  "A atividade e %$#@/";
		String resultadoEsperado =  "A atividade e /";
		assertEquals(resultadoEsperado,
				RefatoradorDeString.substituirCaracteresBrPorEng(expressaoDeRequisito));
	}
	@Test
	public void testSubstituirCaracteresBrPorEng5() {
		System.err.println("testSubstituirCaracteresBrPorEng5");
		String expressaoDeRequisito =  "A atividade e %$-ÍU#UF@/";
		String resultadoEsperado =  "A atividade e -IUUF/";
		assertEquals(resultadoEsperado,
				RefatoradorDeString.substituirCaracteresBrPorEng(expressaoDeRequisito));
	}
	
	@Test
	public void testPrepararExpressaoParaGeracao() {
		System.err.println("testPrepararExpressaoParaGeracao");
		String expressaoDeRequisito =  "Possuo   um  e s";
		String resultadoEsperado =  "Possuo um e s";
		assertEquals(resultadoEsperado,
				RefatoradorDeString.prepararExpressaoParaGeracao(expressaoDeRequisito));
	}
	
	@Test
	public void testPrepararExpressaoParaGeracao1() {
		System.err.println("testPrepararExpressaoParaGeracao1");
		String expressaoDeRequisito =  "Possuo   um      "
				+ ""
				+ " um  tenis tenis";
		String resultadoEsperado =  "Possuo um um tenis tenis";
		assertEquals(resultadoEsperado,
				RefatoradorDeString.prepararExpressaoParaGeracao(expressaoDeRequisito));
	}
	@Test
	public void testPreparaParaYacc() {
		System.err.println("testPreparaParaYacc");
		String expressaoDeRequisito =  "Possuo  um";
		String resultadoEsperado =  "Possuo um";
		assertEquals(resultadoEsperado,
				RefatoradorDeString.prepararExpressaoParaGeracao(expressaoDeRequisito));
	}
	@Test
	public void testFechaParenteses() {
		System.err.println("testFechaParenteses");
		String expressaoDeRequisito =  "Possuo (um";
		String resultadoEsperado =  "Possuo (um)";
		assertEquals(resultadoEsperado,
				RefatoradorDeString.fecharParenteses(expressaoDeRequisito,1));
	}

	@Test
	public void verificarExistenciaDeItemRequisito() {
		System.err.println("verificarExistenciaDeItemRequisito");
		String expressaoDeRequisito =  "1-Registrar Banca de TCC";
		boolean resultadoEsperado =  true;
		assertEquals(resultadoEsperado,
				RefatoradorDeString.verificarExistenciaDeItemRequisito(
						expressaoDeRequisito));
	}
	
	@Test
	public void prepararExpressaoParaYacc() {
		System.err.println("prepararExpressaoParaYacc");
		String expressaoDeRequisito =  " teste valor";
		String resultadoEsperado =  "testevalor";
		assertEquals(resultadoEsperado,
				RefatoradorDeString.prepararExpressaoParaYacc(
						expressaoDeRequisito));
	}
}
