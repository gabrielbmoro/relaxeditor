package testesUnitarios;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import br.unipampa.lesa.modelo.AnalisadorLexicoDeRequisito;

public class AnalizadorLexicoDeRequisitoTest {

	private AnalisadorLexicoDeRequisito analizadorLexicoDeRequisito;

	@Before
	public void setUp() throws Exception {
		this.analizadorLexicoDeRequisito = new AnalisadorLexicoDeRequisito();
	}

	@Test
	public void testVerificarValidadeDaExpressao() {
		System.err.println("testVerificarValidadeDaExpressao");
		String expressaoDeRequisito = "The system SHALL perform all the calculations AS MANY AS POSSIBLE calls occur";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	
	@Test
	public void testVerificarValidadeDaExpressao1() {
		System.err.println("testVerificarValidadeDaExpressao1");
		String expressaoDeRequisito = "The system perform all the calculations calls occur";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	
	@Test
	public void testVerificarValidadeDaExpressao2() {
		System.err.println("testVerificarValidadeDaExpressao2");
		String expressaoDeRequisito = "The system perform teste";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	
	@Test
	public void testVerificarValidadeDaExpressao3() {
		System.err.println("testVerificarValidadeDaExpressao3");
		String expressaoDeRequisito = "The system perform";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	
	@Test
	public void testVerificarValidadeDaExpressao4() {
		System.err.println("testVerificarValidadeDaExpressao4");
		String expressaoDeRequisito =  "The system shall perform all the calculations as many as possible calls occur";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao5() {
		System.err.println("testVerificarValidadeDaExpressao5");
		String expressaoDeRequisito =  "The system SHALL perform all the calculations AS calls occur";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao6() {
		System.err.println("testVerificarValidadeDaExpressao6");
		String expressaoDeRequisito =  "The system SHALL perform all the calculations AS MANY AS POSSIBLE calls occur";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao7() {
		System.err.println("testVerificarValidadeDaExpressao7");
		String expressaoDeRequisito =  "The system sHalL perform all the calculations AS MANY calls occur";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao8() {
		System.err.println("testVerificarValidadeDaExpressao8");
		String expressaoDeRequisito =  "the system must retrieve the information in 4 seconds";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao9() {
		System.err.println("testVerificarValidadeDaExpressao9");
		String expressaoDeRequisito =  "the system must retrieve the information in four seconds";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao10() {
		System.err.println("testVerificarValidadeDaExpressao10");
		String expressaoDeRequisito =  "the system must retrieve the information in 2:2 hours";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao11() {
		System.err.println("testVerificarValidadeDaExpressao11");
		String expressaoDeRequisito =  " in 2:2 hours";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao12() {
		System.err.println("testVerificarValidadeDaExpressao12");
		String expressaoDeRequisito =  " shall system ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao13() {
		System.err.println("testVerificarValidadeDaExpressao13");
		String expressaoDeRequisito =  " shall as many as possible system ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao14() {
		System.err.println("testVerificarValidadeDaExpressao14");
		String expressaoDeRequisito =  "shall eventually as many as possible ed";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao15() {
		System.err.println("testVerificarValidadeDaExpressao15");
		String expressaoDeRequisito =  "Shall e ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao16() {
		System.err.println("testVerificarValidadeDaExpressao16");
		String expressaoDeRequisito =  " as early as possible ";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao17() {
		System.err.println("testVerificarValidadeDaExpressao17");
		String expressaoDeRequisito =  " as as asudhasuh aishdiashd aisjdiasjdi asjdiasjd shall ";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao18() {
		System.err.println("testVerificarValidadeDaExpressao18");
		String expressaoDeRequisito =  " until ";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao19() {
		System.err.println("testVerificarValidadeDaExpressao19");
		String expressaoDeRequisito =  " until asidj asidj ";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao20() {
		System.err.println("testVerificarValidadeDaExpressao20");
		String expressaoDeRequisito =  "until asidj asidj ";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao21() {
		System.err.println("testVerificarValidadeDaExpressao21");
		String expressaoDeRequisito =  "O sistema shall garantir ao usuario a possibilidade de realizar as many as possible calculos in 4 segundos ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao22() {
		System.err.println("testVerificarValidadeDaExpressao22");
		String expressaoDeRequisito =  "O sistema shall garantir ao usuario a possibilidade de realizar as many as possible calculos in quatro segundos ";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao23() {
		System.err.println("testVerificarValidadeDaExpressao23");
		String expressaoDeRequisito =  "shall garantir ao usuario a possibilidade de realizar as many as possible calculos in quatro segundos ";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao24() {
		System.err.println("testVerificarValidadeDaExpressao23");
		String expressaoDeRequisito =  "shall garantir ao usuario a possibilidade de realizar as many as calculos in quatro segundos ";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao25() {
		System.err.println("testVerificarValidadeDaExpressao25");
		String expressaoDeRequisito =  "shall garantir ao usuario a possibilidade de realizar as many as calculos in 4. segundos ";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao26() {
		System.err.println("testVerificarValidadeDaExpressao26");
		String expressaoDeRequisito =  "shall garantir ao usuario a possibilidade de realizar as many as calculos in 4,2 segundos ";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao27() {
		System.err.println("testVerificarValidadeDaExpressao27");
		String expressaoDeRequisito =  "shall garantir ao usuario a possibilidade de realizar as many as calculos in 4:64 segundos ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao28() {
		System.err.println("testVerificarValidadeDaExpressao28");
		String expressaoDeRequisito =  "shall garantir ao usuario a possibilidade de realizar as many as calculos in ";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao29() {
		System.err.println("testVerificarValidadeDaExpressao29");
		String expressaoDeRequisito =  "shall garantir ao usuario a possibilidade de realizar as many as calculos as close as possible to 1 ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao30() {
		System.err.println("testVerificarValidadeDaExpressao30");
		String expressaoDeRequisito =  "shall garantir ao usuario a possibilidade de realizar as many as calculos as close as possible to 1,2 ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao31() {
		System.err.println("testVerificarValidadeDaExpressao31");
		String expressaoDeRequisito =  "shall garantir ao usuario a possibilidade de realizar as many as calculos as close as possible to 1.2 ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao32() {
		System.err.println("testVerificarValidadeDaExpressao32");
		String expressaoDeRequisito =  "shall garantir ao usuario a possibilidade de realizar as many as calculos in 2-2";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao33() {
		System.err.println("testVerificarValidadeDaExpressao20");
		String expressaoDeRequisito =  "asidj asidj until";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao34() {
		System.err.println("testVerificarValidadeDaExpressao20");
		String expressaoDeRequisito =  "asidj asidj until ";
		int resultadoEsperado = 1;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao35() {
		System.err.println("testVerificarValidadeDaExpressao35");
		String expressaoDeRequisito =  "until as many as possible ";
		int resultadoEsperado = 2;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao36() {
		System.err.println("testVerificarValidadeDaExpressao36");
		String expressaoDeRequisito =  " O sistema deve responder as early as possible aos eventos do usuario ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao37() {
		System.err.println("testVerificarValidadeDaExpressao37");
		String expressaoDeRequisito =  " O sistema deve responder as late as possible aos eventos do usuario ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao38() {
		System.err.println("testVerificarValidadeDaExpressao38");
		String expressaoDeRequisito =  " O sistema deve responder as few as possible aos eventos do usuario ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao39() {
		System.err.println("testVerificarValidadeDaExpressao39");
		String expressaoDeRequisito =  "as few as possible aos eventos do usuario ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao40() {
		System.err.println("testVerificarValidadeDaExpressao40");
		String expressaoDeRequisito =  " as few as possible aos eventos do usuario ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
	@Test
	public void testVerificarValidadeDaExpressao41() {
		System.err.println("testVerificarValidadeDaExpressao41");
		String expressaoDeRequisito =  " few as possible aos eventos do usuario ";
		int resultadoEsperado = 0;
		assertEquals(resultadoEsperado,
				analizadorLexicoDeRequisito.verificarValidadeDaExpressao(expressaoDeRequisito));
	}
}