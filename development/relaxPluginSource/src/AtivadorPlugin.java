import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.TipoDeLog;

public class AtivadorPlugin implements BundleActivator {

	@Override
	public void start(BundleContext arg0) throws Exception {
		GerenciadorDeLog.getMyInstance().registrarLog("Inicializando Plugin", TipoDeLog.INFO,
				getClass());
	}

	@Override
	public void stop(BundleContext arg0) throws Exception {
		GerenciadorDeLog.getMyInstance().registrarLog("Finalizando Plugin", TipoDeLog.INFO,
				getClass());
	}

}
