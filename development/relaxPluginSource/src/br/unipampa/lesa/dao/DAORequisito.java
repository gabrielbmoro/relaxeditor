package br.unipampa.lesa.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;
import br.unipampa.lesa.modelo.Requisito;
import br.unipampa.lesa.modelo.repositorio.GerenciadorDeEsquema;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.TipoDeLog;

/**
 * 
 * <b>Propósito:</b> <br>
 * Classe responsável por manipular ou gerenciar a infra-estrutura de
 * persistência de dados realizada pela ferramenta. <br>
 * <b>Instruções de uso:</b> <br>
 * Deve ser utilizado pelas classes que estão acima dela, pela camada
 * br.unipampa.lesa.modelo ou br.unipampa.lesa.modelo.repositorio
 * 
 * @author GabrielBMoro
 * @version 0.4
 * @since 26/04/2015
 */
public class DAORequisito {

	private Document documento;
	private File fileMain;
	private FileWriter fileWriter;
	private XMLOutputter xout;
	private SAXBuilder xinput;

	/**
	 * construtor
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * O método construtor deve inicializar o arquivo principal, pois ele indexa
	 * todos os outros (que são vistos pela aplicação como requisitos).
	 * </ul>
	 */
	public DAORequisito(File filePai) {
		this.fileMain = filePai;
	}

	/**
	 * construtor
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * O método construtor vazio, e utilizado de maneira informal.
	 * </ul>
	 */
	public DAORequisito() {

	}

	/**
	 *
	 *
	 * inicializarXMLVazio
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * O método é responsável por inicializar um elemento indexador, como
	 * primeira instancia da persistência, esse método é contatado para
	 * inicializar o processo de indexação de endereços.
	 * </ul>
	 * 
	 * @param file
	 *            de tipo {@link File}
	 */
	public void inicializarXMLVazio(File file) {
		GerenciadorDeLog.getMyInstance().registrarLog("Inicializando arquivo XML vazio",
				TipoDeLog.INFO, getClass());
		if (file.exists()) {
			this.documento = new Document();
			this.documento.setRootElement(new Element("Main"));
			try {
				this.fileWriter = new FileWriter(file);
				this.xout = new XMLOutputter();
				this.xout.output(this.documento, fileWriter);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				GerenciadorDeLog.getMyInstance().registrarLog("Erro: " + e.getMessage(),
						TipoDeLog.ERROR, getClass());
			}

		}
	}

	/**
	 * montarXML
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por montar o XML, ou seja, ele recebe uma lista de endereços, cada endereço ou arquivo representa um 
	 * requisito, o qual é indexado em um arquivo principal, para que a aplicação possa levantar todos os requisitos contidos nesse arquivo 
	 * principal que representa o seu domínio de requisitos.
	 * </ul>
	 * 
	 * @param enderecos de tipo {@link ArrayList}
	 */
	public void montarXML(ArrayList<String> enderecos) {
		GerenciadorDeLog.getMyInstance().registrarLog(
				"Montando arquivo XML com endereços dos requisitos",
				TipoDeLog.INFO, getClass());
		try {

			Element elementoLinkagem = new Element("linkagem");
			for (String enderecoTemp : enderecos) {
				Element elementoTemp = new Element("arquivo");
				elementoTemp.addContent(enderecoTemp);
				elementoLinkagem.addContent(elementoTemp);
			}
			this.documento = new Document();
			this.documento.setRootElement(elementoLinkagem);
			if (this.fileMain.exists()) {
				// this.fileWriter = new FileWriter(this.fileMain);
				FileOutputStream fileWriterMemoriaSaida = new FileOutputStream(
						this.fileMain);
				OutputStreamWriter outStreamWriter = new OutputStreamWriter(
						fileWriterMemoriaSaida, StandardCharsets.UTF_8);
				this.xout = new XMLOutputter();
				this.xout.output(this.documento, outStreamWriter);
			}
		} catch (IOException erroIO) {
			GerenciadorDeLog.getMyInstance().registrarLog("Erro: " + erroIO.getMessage(),
					TipoDeLog.ERROR, getClass());
		}
	}
	/**
	 * montarXML
	 * <ul>
	 * <li><b>Proprósito</b></li>
	 * Método responsável por montar o XML de um arquivo tipo requisito, afim de que esse arquivo contenha todos os atributos de um requisito 
	 * e que possa representar um requisito, o qual é indexado por um arquivo principal.
	 * </ul>
	 * 
	 * @param file de tipo {@link File}
	 * @param requisito de tipo {@link Requisito}
	 */
	public void montarXML(File file, Requisito requisito) {
		GerenciadorDeLog.getMyInstance()
				.registrarLog(
						"Montando arquivo XML a partir de um arquivo de requisito em extens�o XML.",
						TipoDeLog.INFO, getClass());
		if (requisito != null && file != null) {
			try {
				Element geral = new Element("requisito");
				Element id = new Element("id");
				id.addContent(String.valueOf(requisito.getId()));
				Element nome = new Element("nome");
				nome.addContent(requisito.getNome());
				Element descricao = new Element("descricao");
				Element expressao = new Element("expressao");
				expressao.addContent(requisito.getDescricao());
				descricao.addContent(expressao);
				Element dependencia = new Element("dependencias");
				for (String temp : requisito.getDependecias()) {
					Element elementoTemp = new Element("item");
					elementoTemp.addContent(String.valueOf(temp));
					dependencia.addContent(elementoTemp);
				}
				Element ambiente = new Element("ambiente");
				for (String caracteristicaTemp : requisito.getFatorAmbiental()
						.getCaracteristicas()) {
					Element elementoTemp = new Element("item");
					elementoTemp.addContent(caracteristicaTemp);
					ambiente.addContent(elementoTemp);
				}
				Element monitoracao = new Element("monitoracao");
				for (String caracteristicaTemp1 : requisito
						.getFatorDeMonitoracao().getCaracteristicas()) {
					Element elementoTemp1 = new Element("item");
					elementoTemp1.addContent(caracteristicaTemp1);
					monitoracao.addContent(elementoTemp1);
				}
				Element relacao = new Element("relacao");

				for (int count = 0; count < requisito.getRelacoes().size(); count++) {
					if (requisito.getRelacoes().get(count) != null) {

						Element elementoTemp1 = new Element("item");
						Element elementoChave = new Element("chave");
						elementoChave.addContent(String.valueOf(count));
						Element elementoValor1 = new Element("valorAmbiente");
						elementoValor1.addContent(requisito.getRelacoes()
								.get(count).getCaracteristicaAmbiental());
						Element elementoValor2 = new Element("valorMonitoracao");
						elementoValor2.addContent(requisito.getRelacoes()
								.get(count).getCaracteristicaMonitoracao());
						elementoTemp1.addContent(elementoChave);
						elementoTemp1.addContent(elementoValor1);
						elementoTemp1.addContent(elementoValor2);
						relacao.addContent(elementoTemp1);
					}
				}

				geral.addContent(id);
				geral.addContent(nome);
				geral.addContent(descricao);
				geral.addContent(dependencia);
				geral.addContent(ambiente);
				geral.addContent(monitoracao);
				geral.addContent(relacao);

				this.documento = new Document();
				this.documento.setRootElement(geral);
				if (file.exists()) {
					this.fileWriter = new FileWriter(file);
					this.xout = new XMLOutputter();
					this.xout.output(this.documento, fileWriter);
				}

			} catch (Exception erro) {
				GerenciadorDeLog.getMyInstance().registrarLog("Erro: " + erro.getMessage(),
						TipoDeLog.ERROR, getClass());
			}
		}
	}
	/**
	 * demonstarXML
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Responsável por interpretar as tags do arquivo main, esse m�todo demonsta-o inteiro, passo fundamental para a interpreta��o desses arquivos de 
	 * requisitos e obter suas informações.
	 * </ul>
	 * 
	 */
	public ArrayList<String> demonstarXML() {
		GerenciadorDeLog.getMyInstance().registrarLog("Desmontando arquivo XML.",
				TipoDeLog.INFO, getClass());
		ArrayList<String> enderecoDeArquivosRequisitos = new ArrayList<>();
		if (fileMain.exists()) {
			try {
				this.xinput = new SAXBuilder();
				this.documento = this.xinput.build(fileMain);
				if (this.documento != null) {
					Element elementLinkagem = this.documento.getRootElement();
					if (elementLinkagem != null) {
						List<Element> subElementosArquivo = elementLinkagem
								.getChildren("arquivo");
						if (subElementosArquivo != null) {
							for (Element elementoArquivoTemp : subElementosArquivo) {
								enderecoDeArquivosRequisitos
										.add(elementoArquivoTemp.getValue());
							}
						}
					}
				}
			} catch (FileNotFoundException e) {
				GerenciadorDeLog.getMyInstance().registrarLog("Error: " + e.getMessage(),
						TipoDeLog.ERROR, getClass());
			} catch (IOException e2) {
				GerenciadorDeLog.getMyInstance().registrarLog("Error: " + e2.getMessage(),
						TipoDeLog.ERROR, getClass());
			} catch (JDOMException e3) {
				GerenciadorDeLog.getMyInstance().registrarLog("Error: " + e3.getMessage(),
						TipoDeLog.ERROR, getClass());
			}
		}
		return enderecoDeArquivosRequisitos;
	}
	/**
	 * demonstarXML
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Responsável por interpretar as tags do arquivo requisito, esse método demonsta-o inteiro, processando as informações desse determinado requisito, 
	 * a apartir disso é construido um mapa das informações contidas nessas tags.
	 * </ul>
	 * 
	 * @param fileRequisito de tipo {@link File}
	 */
	public HashMap<TipoMapaChave, Object> demonstarXML(File fileRequisito) {
		GerenciadorDeLog.getMyInstance()
				.registrarLog(
						"Desmontando XML por mapa orientado pelas chaves (tags) do arquivo XML.",
						TipoDeLog.INFO, getClass());
		HashMap<TipoMapaChave, Object> dadosGeraisDoRequisito = new HashMap();
		if (fileRequisito.exists()) {
			try {
				this.xinput = new SAXBuilder();
				this.documento = this.xinput.build(fileRequisito);
				if (this.documento != null) {
					Element elementRequisito = this.documento.getRootElement();
					if (elementRequisito != null) {
						Element id = elementRequisito.getChild("id");
						String idRequisito = id.getValue();

						dadosGeraisDoRequisito.put(TipoMapaChave.ID,
								idRequisito);
						Element nome = elementRequisito.getChild("nome");
						String nomeReq = nome.getValue();
						dadosGeraisDoRequisito.put(TipoMapaChave.NOME, nomeReq);
						Element descricao = elementRequisito
								.getChild("descricao");
						Element expressao = descricao.getChild("expressao");
						String expressaoDescricao = expressao.getValue();
						dadosGeraisDoRequisito.put(TipoMapaChave.DESCRICAO,
								expressaoDescricao);

						Element dependencia = elementRequisito
								.getChild("dependencias");
						dadosGeraisDoRequisito.put(
								TipoMapaChave.DEPENDENCIA,
								recuperaDadosDeElementosFilhos("item",
										dependencia));

						Element ambiente = elementRequisito
								.getChild("ambiente");
						dadosGeraisDoRequisito
								.put(TipoMapaChave.AMBIENTE,
										recuperaDadosDeElementosFilhos("item",
												ambiente));

						Element monitoracao = elementRequisito
								.getChild("monitoracao");
						dadosGeraisDoRequisito.put(
								TipoMapaChave.MONITORACAO,
								recuperaDadosDeElementosFilhos("item",
										monitoracao));

						Element relacao = elementRequisito.getChild("relacao");
						List<Element> elementosFilhosRelacao = relacao
								.getChildren("item");
						ArrayList<String> dadosDeRelacao = new ArrayList<>();
						for (Element elementoTemp : elementosFilhosRelacao) {
							Element chave = elementoTemp.getChild("chave");
							Element valorAmbiente = elementoTemp
									.getChild("valorAmbiente");
							Element valorMonitoracao = elementoTemp
									.getChild("valorMonitoracao");
							dadosDeRelacao.add(chave.getValue());
							dadosDeRelacao.add(valorAmbiente.getValue());
							dadosDeRelacao.add(valorMonitoracao.getValue());
						}
						dadosGeraisDoRequisito.put(TipoMapaChave.RELACAO,
								dadosDeRelacao);
					}
					return dadosGeraisDoRequisito;
				}
			} catch (FileNotFoundException e) {
				GerenciadorDeLog.getMyInstance().registrarLog("Error: " + e.getMessage(),
						TipoDeLog.ERROR, getClass());
			} catch (IOException e2) {
				GerenciadorDeLog.getMyInstance().registrarLog("Error: " + e2.getMessage(),
						TipoDeLog.ERROR, getClass());
			} catch (JDOMException e3) {
				GerenciadorDeLog.getMyInstance().registrarLog("Error: " + e3.getMessage(),
						TipoDeLog.ERROR, getClass());
			}
		}
		return dadosGeraisDoRequisito;
	}
	/**
	 * recuperaDadosDeElementosFilhos
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por recuperar uma lista de dados a partir de um elemento pai.
	 * 
	 * 
	 * </ul>
	 * 
	 * @param nomeDaTag de tipo {@link String}
	 * @param elementoPai de tipo {@link Element}
	 */
	private ArrayList<String> recuperaDadosDeElementosFilhos(String nomeDaTag,
			Element elementoPai) {
		GerenciadorDeLog.getMyInstance().registrarLog(
				"Recuperando elementos filhos por determinada TAG",
				TipoDeLog.INFO, getClass());
		List<Element> listaAmbiente = elementoPai.getChildren(nomeDaTag);
		ArrayList<String> valores = new ArrayList<>();
		for (Element elementoTempAmbiente : listaAmbiente) {
			valores.add(elementoTempAmbiente.getValue());
		}
		return valores;
	}
	/**
	 * deletarRequisito
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por deletar fisicamente um arquivo de requisito.
	 * 
	 * </ul>
	 * 
	 * @param idDoRequisito de tipo {@link String}
	 * @param nome de tipo {@link nome}
	 * @param nomeDaEspecificacao de tipo {@link String}
	 */
	public void deletarRequisito(String idDoRequisito, String nome,
			String nomeDaEspecificacao) {
		GerenciadorDeLog.getMyInstance().registrarLog("Deletando arquivo requisito.",
				TipoDeLog.INFO, getClass());
		File fileTemp = new File(GerenciadorDeEsquema.PATH
				+ nomeDaEspecificacao + File.separator + idDoRequisito + "-" + nome
				+ ".xml");
		System.out.println(GerenciadorDeEsquema.PATH + nomeDaEspecificacao
				+ File.separator + idDoRequisito + "-" + nome + ".xml");
		System.out.println(fileTemp.exists());
		if (fileTemp.exists()) {
			fileTemp.delete();
		}
	}
}
