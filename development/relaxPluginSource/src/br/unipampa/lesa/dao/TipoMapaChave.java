package br.unipampa.lesa.dao;
/**
 * 
 * <b>Propósito:</b> <br>
 * Enum responsável por representar as possíveis tags utilizadas no arquivo de requisitos. <br>
 * <b>Instruções de uso:</b> <br>
 * Deve ser utilizada no mapeamento dos arquivos requisitos, para representar chaves das tags que possuem um determinado valor.
 * 
 * @author GabrielBMoro
 * @version 0.4
 * @since 26/04/2015
 */
public enum TipoMapaChave {
	ID, NOME, DESCRICAO, DEPENDENCIA, AMBIENTE, MONITORACAO, RELACAO,
}
