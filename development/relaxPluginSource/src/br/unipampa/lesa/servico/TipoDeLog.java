package br.unipampa.lesa.servico;

/**
 * 
 * <b>Propósito:</b> <br>
 * Enum responsável representar os poss�veis tipos de log. <br>
 * <b>Instruções de uso:</b> <br>
 * Essa classe deve ser utilizada em parceria com a classe {@link GerenciadorDeLog}
 * 
 * @author GabrielBMoro
 * @version 0.4
 * @since 26/04/2015
 */
public enum TipoDeLog {
	/**
	 * INFO - Log de Informação
	 * ERROR - Log de Erro ou Exception
	 * WARN - Log de Perigo
	 * FATAl - Log de Erros provenientes a plataforma eclipse
	 * DEBUG - Log de Depuração
	 */
	INFO, ERROR, WARN, FATAL, DEBUG;
}
