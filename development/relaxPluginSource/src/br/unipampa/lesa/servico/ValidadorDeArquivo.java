package br.unipampa.lesa.servico;

import java.io.File;

/**
 * 
 * <b>Propósito:</b> <br>
 * Classe responsável por efetuar validações em arquivos ou pastas através do objeto {@link File}. <br>
 * <b>Instruções de uso:</b> <br>
 * Essa classe deve ser utilizada para auxiliar as funcionalidades em termos de validações de arquivos.
 * 
 * @author GabrielBMoro
 * @version 0.4
 * @since 26/04/2015
 */
public class ValidadorDeArquivo {

	/**
	 * verificarExistenciaDoArquivoIndexador
	 *<ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por fornecer o serviçso de verificar se o arquivo indexador exista, para que seja possível abrir determinado projeto.
	 * </ul>
	 * @param path de tipo {@link String}
	 */
	public static boolean verificarExistenciaDoArquivoIndexador(String path){
		File file = new File(path);
		for(String nomeDoArquivo : file.list()){
			
			if(nomeDoArquivo.equalsIgnoreCase("Main.xml")){
				return true;
			}
		}
		return false;
	}
}
