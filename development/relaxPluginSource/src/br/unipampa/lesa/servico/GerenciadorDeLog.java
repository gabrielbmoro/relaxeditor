package br.unipampa.lesa.servico;

import java.util.Observable;

import org.apache.log4j.Logger;

/**
 * 
 * <b>Propósito:</b> <br>
 * Classe responsável por disparar logs. <br>
 * <b>Instruções de uso:</b> <br>
 * Essa classe deve ser utilizada em qualquer funcionalidade da aplicação, pois
 * é necessário mapear as chamadas realizadas pela aplicação, <br />
 * com o objetivo de restaurar ações futuramente e visualizar que ações estão
 * acontecendo a partir de uma ação de usuário.
 * 
 * @author GabrielBMoro
 * @version 0.4
 * @since 26/04/2015
 */
public class GerenciadorDeLog extends Observable{
	
	private static GerenciadorDeLog myInstance;
	
	private GerenciadorDeLog(){
		
	}
	
	public static GerenciadorDeLog getMyInstance(){
		if(myInstance==null){
			myInstance = new GerenciadorDeLog();
		}
		return myInstance;
	}
	/**
	 * registrarLog
	 * <ul>
	 * <li><b>Prop�sito</b></li>
	 * M�todo respons�vel por escrever os logs.
	 * </ul>
	 * 
	 * @param mensagem
	 * @param tipoDeLog
	 * @param classe
	 */
	public void registrarLog(String mensagem, TipoDeLog tipoDeLog,
			Class classe) {
		Logger logger = Logger.getLogger(classe);
		switch (tipoDeLog) {
		case INFO:
			logger.info(mensagem);
			return;
		case DEBUG:
			logger.debug(mensagem);
			return;
		case FATAL:
			logger.fatal(mensagem);
			return;
		case WARN:
			logger.warn(mensagem);
			return;
		case ERROR:
			logger.error(mensagem);
			return;
		}
	}
	
}