package br.unipampa.lesa.servico;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * <b>Propósito:</b> <br>
 * Classe responsável por efetuar validações em {@link String}. <br>
 * <b>Instruções de uso:</b> <br>
 * Essa classe deve ser utilizada para auxiliar as funcionalidades em termos de
 * validações de String.
 * 
 * @author GabrielBMoro
 * @version 0.4
 * @since 26/04/2015
 */
public class RefatoradorDeString {
	/**
	 * substituirCaracteresBrPorEng
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por converter na forma normal os caracteres especiais.
	 * </ul>
	 * 
	 * @param str
	 *            de tipo {@link String}
	 */
	public static String substituirCaracteresBrPorEng(String str) {
			str = str.replaceAll("[ÂŔÁÄĂ]", "A");
			str = str.replaceAll("[âăŕáä]", "a");
			str = str.replaceAll("[ĘČÉË]", "E");
			str = str.replaceAll("[ęčéë]", "e");
			str = str.replaceAll("[ÎÍĚĎ]", "I");
			str = str.replaceAll("[îíěď]", "i");
			str = str.replaceAll("[ÔŐŇÓÖ]", "O");
			str = str.replaceAll("[ôőňóö]", "o");
			str = str.replaceAll("[ŰŮÚÜ]", "U");
			str = str.replaceAll("[űúůü]", "u");
			str = str.replaceAll("Ç", "C");
			str = str.replaceAll("ç", "c");
			str = str.replaceAll("[ý˙]", "y");
			str = str.replaceAll("Ý", "Y");
			str = str.replaceAll("ń", "n");
			str = str.replaceAll("Ń", "N");
			str = str.replaceAll("[`´~><@#%¨&*()!?$]", "");
		return str;
	}

	/**
	 * prepararExpressaoParaYacc
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por preparar a expressão relax gerada para ser avaliada pelo yacc, ela deve ser abstraida com os seus parênteses juntos.
	 * </ul>
	 * 
	 * @param textoParaYacc de tipo {@link String}
	 * @return
	 */
	public static String prepararExpressaoParaYacc(String textoParaYacc) {
		textoParaYacc = textoParaYacc.replaceAll("\\(", "");
		textoParaYacc = textoParaYacc.replaceAll("\\)", "");
		textoParaYacc = textoParaYacc.replaceAll(" ", "");
		return textoParaYacc;
	}
	
	public static String preparaExpressaoParaLexica(String texto){
		texto = texto.replaceAll("\\(", "");
		texto = texto.replaceAll("\\)", "");
		texto = texto.replaceAll("f or q", "forq");
		texto = texto.replaceAll("f or q p", "forqp");
		texto = texto.replaceAll("t p", "tp");
		texto = texto.replaceAll("e p", "ep");
		return texto;
	}

	/**
	 * fecharParenteses
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por fechar os parêntes da expressão.
	 * Lembrando, a geração da expressão não contém parênteses, mas eles foram adicionados para a melhor visualização do usuário.
	 * </ul>
	 * @param str de {@link String}
	 * @param numeroDeParenteses de {@link Integer}
	 * @return
	 */
	public static String fecharParenteses(String str, int numeroDeParenteses) {
		for (int count = 0; count < numeroDeParenteses; count++) {
			str += ")";
		}
		return str;
	}

	/**
	 * verificarExistenciaDeItemRequisito
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por verificar a existência do item requisito, o qual é composto por um inteiro com h�fen e descrição do nome do requisito.
	 * </ul>
	 * @param str de tipo {@link String}
	 * @return
	 */
	public static boolean verificarExistenciaDeItemRequisito(String str) {
		Pattern pattern = Pattern.compile("\\d+-\\w+");
		return pattern.matcher(str).find();
	}
	
	public static String prepararExpressaoParaGeracao(String str){
		Pattern pattern = Pattern.compile("\\s+");
		Matcher matcher = pattern.matcher(str);
		str = matcher.replaceAll(" ");
		return str;
	}

}
