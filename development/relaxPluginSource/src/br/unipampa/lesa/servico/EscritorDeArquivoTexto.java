package br.unipampa.lesa.servico;

/**
 * ProcessarTexto
 * 
 * @author Gabriel Bronzatti Moro
 * @since 08/01/2013
 * @version 1.0
 */


import java.io.*;


/**
 * 
 * <b>Propósito:</b> <br>
 * Classe responsável por escrever arquivos .txt. <br>
 * <b>Instruções de uso:</b> <br>
 * Essa classe é utilizada pela análise de expressão, pois uma entrada do analizador sintático é a expressão de requisito salva em arquivo físico.
 * 
 * @author GabrielBMoro
 * @version 0.4
 * @since 26/04/2015
 */
public class EscritorDeArquivoTexto {

    private File file;
    private FileOutputStream fileOutput;
    private OutputStream output;
    private BufferedWriter buffer;
    public static String NOME_DO_ARQUIVO=System.getProperty("user.home")+"/entradaRequisito.txt";

    /**
	 * registrarLog
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por escrever em arquivo determinada String recebida como parímetro.
	 * </ul>
	 * 
	 * @param texto de tipo {@link String}
	 */
    public void escreverArquivo(String texto) {
        try {
            file = new File(NOME_DO_ARQUIVO);
            if(!file.exists()){
            	file.createNewFile();
            }
            fileOutput = new FileOutputStream(file);
            buffer = new BufferedWriter(new OutputStreamWriter(fileOutput));

            buffer.write(texto);

            buffer.close();
            fileOutput.close();
        } catch (FileNotFoundException erro1) {
            System.err.println("Exception: " + erro1 + "\n" + "Diretório não encontrado...");
        }catch (IOException erro3) {
            System.err.println("Exception: " + erro3 + "\n" + "Exception da classe IOException...");
        }
    }
}
