package br.unipampa.lesa.apresentacao;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyleRange;

import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Button;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.swt.widgets.Label;

public class RelaxParte {

	private StyledText txtExpressao;
	private ScrolledComposite scrolledComposite_3;
	public static Font fontCabecalhoArvore,fontItemArvore,fontSubItemArvore;
	private Color corDaPalavraChave,corDaPalavraNormal,corDaExpressao,corDeErrors;
	private Button btnConverter;
		
	public RelaxParte() {
		
	}

	/**
	 * Create contents of the view part.
	 */
	@PostConstruct
	public void createControls(Composite parent) {
		parent.setSize(300, 271);
		Composite compositePrincipal = new Composite(parent, SWT.NONE);
		compositePrincipal.setBounds(0, 0, 298, 233);

		fontCabecalhoArvore = new Font(parent.getDisplay(), new FontData(
				"Arial", 12, SWT.BOLD));
		fontItemArvore = new Font(parent.getDisplay(), new FontData("Arial",
				12, SWT.NONE));
		fontSubItemArvore = new Font(parent.getDisplay(), new FontData("Arial",
				10, SWT.NONE));
		
				scrolledComposite_3 = new ScrolledComposite(
						compositePrincipal, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				scrolledComposite_3.setBounds(12, 57, 240, 163);
				scrolledComposite_3.setExpandHorizontal(true);
				scrolledComposite_3.setExpandVertical(true);
				scrolledComposite_3.setVisible(true);
				
				txtExpressao = new StyledText(scrolledComposite_3, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI | SWT.WRAP);
				txtExpressao.setDoubleClickEnabled(false);
				txtExpressao.setBounds(271, 445, 573, 47);
				txtExpressao.setEditable(false);
				scrolledComposite_3.setContent(txtExpressao);
				scrolledComposite_3.setMinSize(txtExpressao.computeSize(SWT.DEFAULT,
						SWT.DEFAULT));
				
				btnConverter = new Button(compositePrincipal, SWT.NONE);
				btnConverter.setToolTipText("Convert for Expression");
				btnConverter.setImage(ResourceManager.getPluginImage("relaxPluginSource", "icons/icon_converter.png"));
				btnConverter.setBounds(12, 12, 37, 30);
				
				Label label = new Label(compositePrincipal, SWT.SEPARATOR | SWT.HORIZONTAL);
				label.setBounds(12, 48, 240, 2);

		

		corDaPalavraChave = new Color(parent.getDisplay(), 0, 185,
				185);
		corDaPalavraNormal = new Color(parent.getDisplay(), 0, 0, 0);
		corDaExpressao = new Color(parent.getDisplay(),34,177,76);
		corDeErrors = new Color(parent.getDisplay(),234,43,21);

		InicializadorDeControladores.getInstance().filtro(this);
	}

	@PreDestroy
	public void dispose() {
	}

	@Focus
	public void setFocus() {
		// TODO Set the focus to control
	}

	

	
	
	public void informarErros(String descricaoDeErros){
		txtExpressao.setText(descricaoDeErros
				);
		StyleRange styleRangeTemp1 = new StyleRange();
		styleRangeTemp1.start = 0;
		styleRangeTemp1.length = descricaoDeErros.length();
		styleRangeTemp1.fontStyle = SWT.BOLD;
		styleRangeTemp1.foreground= corDeErrors;
		txtExpressao.setStyleRange(styleRangeTemp1);
	}
	
	public void informarExpressaoGerada(String expressaoInicial){
		StyleRange styleRangeTemp = new StyleRange();
		styleRangeTemp.start = 0;
		styleRangeTemp.length = expressaoInicial
				.length();
		styleRangeTemp.fontStyle = SWT.BOLD;
		styleRangeTemp.foreground = corDaExpressao;
		txtExpressao.setStyleRange(styleRangeTemp);
	}
	

	public static Font getFontCabecalhoArvore() {
		return fontCabecalhoArvore;
	}

	public static void setFontCabecalhoArvore(Font fontCabecalhoArvore) {
		RelaxParte.fontCabecalhoArvore = fontCabecalhoArvore;
	}

	public static Font getFontItemArvore() {
		return fontItemArvore;
	}

	public static void setFontItemArvore(Font fontItemArvore) {
		RelaxParte.fontItemArvore = fontItemArvore;
	}

	public static Font getFontSubItemArvore() {
		return fontSubItemArvore;
	}

	public static void setFontSubItemArvore(Font fontSubItemArvore) {
		RelaxParte.fontSubItemArvore = fontSubItemArvore;
	}

	public Color getCorDaPalavraChave() {
		return corDaPalavraChave;
	}

	public void setCorDaPalavraChave(Color corDaPalavraChave) {
		this.corDaPalavraChave = corDaPalavraChave;
	}

	public Color getCorDaPalavraNormal() {
		return corDaPalavraNormal;
	}

	public void setCorDaPalavraNormal(Color corDaPalavraNormal) {
		this.corDaPalavraNormal = corDaPalavraNormal;
	}

	public StyledText getTxtExpressao() {
		return txtExpressao;
	}

	public void setTxtExpressao(StyledText txtExpressao) {
		this.txtExpressao = txtExpressao;
	}
	public Color getCorDaExpressao(){
		return this.corDaExpressao;
	}
	public Color getCorDeErros(){
		return this.corDeErrors;
	}

	public Button getBtnConverter() {
		return btnConverter;
	}

	public void setBtnConverter(Button btnConverter) {
		this.btnConverter = btnConverter;
	}
}
