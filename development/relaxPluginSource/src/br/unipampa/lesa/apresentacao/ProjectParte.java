package br.unipampa.lesa.apresentacao;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import br.unipampa.lesa.controle.ControleProjectParte;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;

public class ProjectParte {
	
	private ScrolledComposite scrolledProjeto;
	private Button btnNewRequirement, btnReopen, btnRemove;
	private ControleProjectParte controleProjetoParte;
	private Tree treeProjetos;
	private Menu menuTree;
	private MenuItem menuItemNovo, menuItemEditar, menuItemDeletar;
	public static Font fontCabecalhoArvore,fontItemArvore,fontSubItemArvore;
	private Color corDaExpressao,corDeErrors;
	private Label label;
	
	public ProjectParte() {
	}

	/**
	 * Create contents of the view part.
	 */
	@PostConstruct
	public void createControls(Composite parent) {
		parent.setSize(300, 500);
		Composite compositePrincipal = new Composite(parent, SWT.NONE);
		compositePrincipal.setBounds(0, 0, 298, 498);
		fontCabecalhoArvore = new Font(parent.getDisplay(), new FontData(
				"Arial", 12, SWT.BOLD));
		fontItemArvore = new Font(parent.getDisplay(), new FontData("Arial",
				12, SWT.NONE));
		fontSubItemArvore = new Font(parent.getDisplay(), new FontData("Arial",
				10, SWT.NONE));
		
				scrolledProjeto = new ScrolledComposite(compositePrincipal,
						SWT.H_SCROLL | SWT.V_SCROLL);
				scrolledProjeto.setExpandVertical(true);
				scrolledProjeto.setExpandHorizontal(true);
				scrolledProjeto.setBounds(10, 57, 278, 359);
				scrolledProjeto.setVisible(true);
				treeProjetos = new Tree(scrolledProjeto, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI | SWT.WRAP);
				scrolledProjeto.setContent(treeProjetos);
				scrolledProjeto.setMinSize(treeProjetos.computeSize(SWT.DEFAULT,
						SWT.DEFAULT));
				scrolledProjeto.setMinSize(new Point(85, 85));
				
						menuTree = new Menu(treeProjetos);
						menuItemNovo = new MenuItem(menuTree, SWT.CASCADE);
						menuItemNovo.setImage(ResourceManager.getPluginImage("relaxPluginSource", "icons/icon_adicionararquivo.png"));
						menuItemNovo.setText("New Requirement");
						menuItemEditar = new MenuItem(menuTree, SWT.CASCADE);
						menuItemEditar.setImage(ResourceManager.getPluginImage("relaxPluginSource", "icons/icon_atualizararquivo.png"));
						menuItemEditar.setText("Reopen Requirement");
						menuItemDeletar = new MenuItem(menuTree, SWT.CASCADE);
						menuItemDeletar.setImage(ResourceManager.getPluginImage("relaxPluginSource", "icons/icon_removerarquivo.png"));
						menuItemDeletar.setText("Remove Requirement");
						treeProjetos.setMenu(menuTree);
						 
						btnNewRequirement = new Button(compositePrincipal, SWT.NONE);
						btnNewRequirement.setImage(ResourceManager.getPluginImage("relaxPluginSource", "icons/icon_adicionararquivo.png"));
						btnNewRequirement.setToolTipText("New Requirement");
						btnNewRequirement.setBounds(10, 10, 37, 30);
						
						btnReopen = new Button(compositePrincipal, SWT.NONE);
						btnReopen.setImage(ResourceManager.getPluginImage("relaxPluginSource", "icons/icon_atualizararquivo.png"));
						btnReopen.setToolTipText("Reopen Requirement");
						btnReopen.setBounds(53, 10, 37, 30);
						
						btnRemove = new Button(compositePrincipal, SWT.NONE);
						btnRemove.setImage(ResourceManager.getPluginImage("relaxPluginSource", "icons/icon_removerarquivo.png"));
						btnRemove.setToolTipText("Remove Requirement");
						btnRemove.setBounds(96, 10, 37, 30);
						
						label = new Label(compositePrincipal, SWT.SEPARATOR | SWT.HORIZONTAL);
						label.setBounds(10, 46, 278, 2);

		
		corDaExpressao = new Color(compositePrincipal.getDisplay(),34,177,76);
		corDeErrors = new Color(compositePrincipal.getDisplay(),234,43,21);

		desabilitarComponentes();
		
		InicializadorDeControladores.getInstance().filtro(this);
		
	}

	public void habilitarComponentes(){
		btnNewRequirement.setEnabled(true);
		btnReopen.setEnabled(true);
		btnRemove.setEnabled(true);
		menuTree.setEnabled(true);
	}
	
	public void desabilitarComponentes(){
		btnNewRequirement.setEnabled(false);
		btnReopen.setEnabled(false);
		btnRemove.setEnabled(false);
		menuTree.setEnabled(false);
	}
	@PreDestroy
	public void dispose() {
	}

	@Focus
	public void setFocus() {
		// TODO Set the focus to control
	}

	
	
	
	public ScrolledComposite getScrolledComposite_4() {
		return scrolledProjeto;
	}

	public void setScrolledComposite_4(ScrolledComposite scrolledComposite_4) {
		this.scrolledProjeto = scrolledComposite_4;
	}
	public ControleProjectParte getControlePluginParte() {
		return controleProjetoParte;
	}

	public void setControlePluginParte(ControleProjectParte controlePluginParte) {
		this.controleProjetoParte = controlePluginParte;
	}
	
	public Tree getTreeProjetos() {
		return treeProjetos;
	}

	public void setTreeProjetos(Tree treeProjetos) {
		this.treeProjetos = treeProjetos;
	}

	public Menu getMenuTree() {
		return menuTree;
	}

	public void setMenuTree(Menu menuTree) {
		this.menuTree = menuTree;
	}

	public MenuItem getMenuItemNovo() {
		return menuItemNovo;
	}

	public void setMenuItemNovo(MenuItem menuItemNovo) {
		this.menuItemNovo = menuItemNovo;
	}

	public MenuItem getMenuItemEditar() {
		return menuItemEditar;
	}

	public void setMenuItemEditar(MenuItem menuItemEditar) {
		this.menuItemEditar = menuItemEditar;
	}

	public MenuItem getMenuItemDeletar() {
		return menuItemDeletar;
	}

	public void setMenuItemDeletar(MenuItem menuItemDeletar) {
		this.menuItemDeletar = menuItemDeletar;
	}

	public static Font getFontCabecalhoArvore() {
		return fontCabecalhoArvore;
	}

	public static void setFontCabecalhoArvore(Font fontCabecalhoArvore) {
		RelaxParte.fontCabecalhoArvore = fontCabecalhoArvore;
	}

	public static Font getFontItemArvore() {
		return fontItemArvore;
	}

	public static void setFontItemArvore(Font fontItemArvore) {
		RelaxParte.fontItemArvore = fontItemArvore;
	}

	public static Font getFontSubItemArvore() {
		return fontSubItemArvore;
	}

	public static void setFontSubItemArvore(Font fontSubItemArvore) {
		RelaxParte.fontSubItemArvore = fontSubItemArvore;
	}

	public Color getCorDaExpressao(){
		return this.corDaExpressao;
	}
	public Color getCorDeErros(){
		return this.corDeErrors;
	}

	public Button getBtnNewRequirement() {
		return btnNewRequirement;
	}

	public void setBtnNewRequirement(Button btnNewRequirement) {
		this.btnNewRequirement = btnNewRequirement;
	}

	public Button getBtnReopen() {
		return btnReopen;
	}

	public void setBtnReopen(Button btnReopen) {
		this.btnReopen = btnReopen;
	}

	public Button getBtnRemove() {
		return btnRemove;
	}

	public void setBtnRemove(Button btnRemove) {
		this.btnRemove = btnRemove;
	}
	
}
