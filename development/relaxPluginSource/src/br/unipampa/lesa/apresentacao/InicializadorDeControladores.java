package br.unipampa.lesa.apresentacao;

import br.unipampa.lesa.controle.ControleEditorParte;
import br.unipampa.lesa.controle.ControleObservadorGeral;
import br.unipampa.lesa.controle.ControleProjectParte;
import br.unipampa.lesa.controle.ControleRelaxParte;
import br.unipampa.lesa.modelo.repositorio.RepositorioDeEspecificacao;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.TipoDeLog;

public class InicializadorDeControladores {

	private ProjectParte projectParte;
	private EditorParte editorParte;
	private RelaxParte relaxParte;
	private AbrirHandler abrirHandler;
	private DeleteHandler deleteHandler;
	private NovoHandler novoHandler;
	private ControleEditorParte controleEditorParte;
	private ControleProjectParte controleProjectParte;
	private ControleRelaxParte controleRelaxParte;
	private static InicializadorDeControladores myInstance;
	private static int FLAG_DE_INICIALIZACAO_EVENTOS=0;

	private InicializadorDeControladores() {

	}

	public static InicializadorDeControladores getInstance() {
		GerenciadorDeLog.getMyInstance().registrarLog(
				"Inicializando Fábrica de Controladores", TipoDeLog.INFO,
				InicializadorDeControladores.class);
		if (myInstance == null) {
			myInstance = new InicializadorDeControladores();
		}
		return myInstance;
	}

	public void filtro(Object objetoDeInterface) {
		if (objetoDeInterface instanceof ProjectParte) {
			projectParte = (ProjectParte) objetoDeInterface;
			GerenciadorDeLog.getMyInstance().registrarLog(
					"ProjectParte capturado!", TipoDeLog.INFO,
					InicializadorDeControladores.class);
		} else if (objetoDeInterface instanceof EditorParte) {
			editorParte = (EditorParte) objetoDeInterface;
			GerenciadorDeLog.getMyInstance().registrarLog(
					"EditorParte capturado!", TipoDeLog.INFO,
					InicializadorDeControladores.class);
		} else if (objetoDeInterface instanceof RelaxParte) {
			relaxParte = (RelaxParte) objetoDeInterface;
			GerenciadorDeLog.getMyInstance().registrarLog(
					"RelaxParte capturado!", TipoDeLog.INFO,
					InicializadorDeControladores.class);
		} else if (objetoDeInterface instanceof NovoHandler) {
			novoHandler = (NovoHandler) objetoDeInterface;
			GerenciadorDeLog.getMyInstance().registrarLog(
					"NovoHandler capturado!", TipoDeLog.INFO,
					InicializadorDeControladores.class);
		} else if (objetoDeInterface instanceof AbrirHandler) {
			abrirHandler = (AbrirHandler) objetoDeInterface;
			GerenciadorDeLog.getMyInstance().registrarLog(
					"AbrirHandler capturado!", TipoDeLog.INFO,
					InicializadorDeControladores.class);
		} else if (objetoDeInterface instanceof DeleteHandler) {
			deleteHandler = (DeleteHandler) objetoDeInterface;
			GerenciadorDeLog.getMyInstance().registrarLog(
					"DeleteHandler capturado!", TipoDeLog.INFO,
					InicializadorDeControladores.class);
		}
		if (projectParte != null && editorParte != null && relaxParte != null && FLAG_DE_INICIALIZACAO_EVENTOS==0) {
			definirControladores();
			FLAG_DE_INICIALIZACAO_EVENTOS=+ 1;
		}
		if (projectParte != null && editorParte != null && relaxParte != null
				&& novoHandler != null && abrirHandler != null
				&& deleteHandler != null) {
			GerenciadorDeLog.getMyInstance().registrarLog(
					"Definindo controladores e levantando seus listeners!",
					TipoDeLog.INFO, getClass());

			novoHandler.getNovoControle().inicializarComponentes(novoHandler);
			abrirHandler.getControleAbrirHandler().inicializarComponentes(
					abrirHandler);
			deleteHandler.getControleDeleteHandler().registrarListeners();
		}
	}

	private void definirControladores() {
		controleEditorParte = new ControleEditorParte();
		controleEditorParte.setProjectParte(projectParte);
		controleEditorParte.setEditorParte(editorParte);

		controleProjectParte = new ControleProjectParte();
		controleProjectParte.setProjectParte(projectParte);
		controleProjectParte.setEditorParte(editorParte);

		controleRelaxParte = new ControleRelaxParte();
		controleRelaxParte.setProjectParte(projectParte);
		controleRelaxParte.setRelaxParte(relaxParte);
		controleRelaxParte.setEditorParte(editorParte);

		ControleObservadorGeral controleObservadorGeral = new ControleObservadorGeral(
				RepositorioDeEspecificacao.getMyInstance(), projectParte,
				editorParte);
		controleEditorParte.setRelaxParte(relaxParte);
		controleEditorParte.registrarListeners();
		controleProjectParte.registrarListeners();
		controleRelaxParte.registrarListeners();

	}

	public ProjectParte getProjectParte() {
		return projectParte;
	}

	public void setProjectParte(ProjectParte projectParte) {
		this.projectParte = projectParte;
	}

	public EditorParte getEditorParte() {
		return editorParte;
	}

	public void setEditorParte(EditorParte editorParte) {
		this.editorParte = editorParte;
	}

	public RelaxParte getRelaxParte() {
		return relaxParte;
	}

	public void setRelaxParte(RelaxParte relaxParte) {
		this.relaxParte = relaxParte;
	}

	public ControleEditorParte getControleEditorParte() {
		return controleEditorParte;
	}

	public void setControleEditorParte(ControleEditorParte controleEditorParte) {
		this.controleEditorParte = controleEditorParte;
	}

	public ControleProjectParte getControleProjectParte() {
		return controleProjectParte;
	}

	public void setControleProjectParte(ControleProjectParte controleProjectParte) {
		this.controleProjectParte = controleProjectParte;
	}

	public ControleRelaxParte getControleRelaxParte() {
		return controleRelaxParte;
	}

	public void setControleRelaxParte(ControleRelaxParte controleRelaxParte) {
		this.controleRelaxParte = controleRelaxParte;
	}
	
}
