package br.unipampa.lesa.apresentacao;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.workbench.IPresentationEngine;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Table;

import br.unipampa.lesa.modelo.DicionarioDeDados;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.TipoDeLog;

import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class EditorParte {

	private StyledText txtRequisito;
	private Table tableRelacionamento;
	private Button btnAddAmbiente, btnRemoveAmbiente, btnAddMonit,
			btnSalvarRequisito, btnAddRelacionamento, btnRemoverRelacionamento,
			btnRemoveMonit, btnAddDependence, btnRemoveDependence;
	private ScrolledComposite scrolledComposite, scrolledComposite_1,
			scrolledComposite_2, scrolledTabelaDeRelacoes, scrolledComposite_5;
	private Label lblDescrio, lblAmbiente, lblAmbiente_1, lblRelacionamento,
			lblMonitorao, lblDependncias, lblMonitorao_1, txtId;
	private List listAmbiente, listMonitoracao, listDependencia;
	private Combo comboAmb, comboMonit, comboRequisitos;
	private Text txtMonitoracao, txtAmbiente, txtNome;
	public static Font fontCabecalhoArvore, fontItemArvore, fontSubItemArvore;
	private Color corDaPalavraChave, corDaPalavraNormal;
	private Label label;

	public EditorParte() {
	}

	/**
	 * Create contents of the view part.
	 */
	@PostConstruct
	public void createControls(Composite parent) {
		parent.setSize(1055, 600);
		Composite compositePrincipal = new Composite(parent, SWT.BORDER
				| SWT.NO_FOCUS);
		compositePrincipal.setBounds(0, 0, 1086, 598);

		fontCabecalhoArvore = new Font(parent.getDisplay(), new FontData(
				"Arial", 12, SWT.BOLD));
		fontItemArvore = new Font(parent.getDisplay(), new FontData("Arial",
				12, SWT.NONE));
		fontSubItemArvore = new Font(parent.getDisplay(), new FontData("Arial",
				10, SWT.NONE));

		corDaPalavraChave = new Color(compositePrincipal.getDisplay(), 0, 185,
				185);
		corDaPalavraNormal = new Color(compositePrincipal.getDisplay(), 60, 60, 60);

		lblDescrio = new Label(compositePrincipal, SWT.NONE);
		lblDescrio
				.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblDescrio.setBounds(10, 141, 121, 15);
		lblDescrio.setText("Description:");

		lblAmbiente = new Label(compositePrincipal, SWT.NONE);
		lblAmbiente.setFont(SWTResourceManager
				.getFont("Segoe UI", 10, SWT.BOLD));
		lblAmbiente.setText("Environment:");
		lblAmbiente.setBounds(507, 72, 97, 15);

		btnAddAmbiente = new Button(compositePrincipal, SWT.NONE);
		btnAddAmbiente.setToolTipText("Add evironment adjective");
		btnAddAmbiente.setImage(ResourceManager.getPluginImage(
				"relaxPluginSource", "icons/icon_adicionaritem.png"));
		btnAddAmbiente.setBounds(995, 84, 40, 40);

		btnRemoveAmbiente = new Button(compositePrincipal, SWT.NONE);
		btnRemoveAmbiente.setImage(ResourceManager.getPluginImage(
				"relaxPluginSource", "icons/icon_removeritem.png"));
		btnRemoveAmbiente.setToolTipText("Remove evironment adjective");
		btnRemoveAmbiente.setBounds(995, 141, 40, 40);

		lblMonitorao = new Label(compositePrincipal, SWT.NONE);
		lblMonitorao.setFont(SWTResourceManager.getFont("Segoe UI", 10,
				SWT.BOLD));
		lblMonitorao.setText("Monitor:");
		lblMonitorao.setBounds(507, 226, 97, 15);

		btnAddMonit = new Button(compositePrincipal, SWT.NONE);
		btnAddMonit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnAddMonit.setToolTipText("Add Monitor adjective");
		btnAddMonit.setImage(ResourceManager.getPluginImage(
				"relaxPluginSource", "icons/icon_adicionaritem.png"));
		btnAddMonit.setBounds(995, 236, 40, 40);

		btnRemoveMonit = new Button(compositePrincipal, SWT.NONE);
		btnRemoveMonit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnRemoveMonit.setToolTipText("Remove Monitor adjective");
		btnRemoveMonit.setImage(ResourceManager.getPluginImage(
				"relaxPluginSource", "icons/icon_removeritem.png"));
		btnRemoveMonit.setBounds(995, 293, 40, 40);

		lblDependncias = new Label(compositePrincipal, SWT.NONE);
		lblDependncias.setFont(SWTResourceManager.getFont("Segoe UI", 10,
				SWT.BOLD));
		lblDependncias.setText("Dependence:");
		lblDependncias.setBounds(10, 293, 121, 15);

		scrolledComposite = new ScrolledComposite(compositePrincipal,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setBounds(507, 127, 482, 77);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		listAmbiente = new List(scrolledComposite, SWT.BORDER);
		scrolledComposite.setContent(listAmbiente);
		scrolledComposite.setMinSize(listAmbiente.computeSize(SWT.DEFAULT,
				SWT.DEFAULT));

		scrolledComposite_1 = new ScrolledComposite(compositePrincipal,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite_1.setExpandVertical(true);
		scrolledComposite_1.setExpandHorizontal(true);
		scrolledComposite_1.setBounds(507, 280, 482, 77);

		listMonitoracao = new List(scrolledComposite_1, SWT.BORDER);
		scrolledComposite_1.setContent(listMonitoracao);
		scrolledComposite_1.setMinSize(listMonitoracao.computeSize(SWT.DEFAULT,
				SWT.DEFAULT));

		scrolledComposite_2 = new ScrolledComposite(compositePrincipal,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite_2.setExpandVertical(true);
		scrolledComposite_2.setExpandHorizontal(true);
		scrolledComposite_2.setBounds(10, 349, 406, 101);

		listDependencia = new List(scrolledComposite_2, SWT.BORDER);
		listDependencia.setFont(SWTResourceManager.getFont("Segoe UI", 11,
				SWT.NORMAL));
		scrolledComposite_2.setContent(listDependencia);
		scrolledComposite_2.setMinSize(listDependencia.computeSize(SWT.DEFAULT,
				SWT.DEFAULT));

		lblAmbiente_1 = new Label(compositePrincipal, SWT.NONE);
		lblAmbiente_1.setFont(SWTResourceManager.getFont("Segoe UI", 10,
				SWT.BOLD));
		lblAmbiente_1.setBounds(507, 396, 97, 15);
		lblAmbiente_1.setText("Environment:");

		comboAmb = new Combo(compositePrincipal, SWT.NONE);
		comboAmb.setBounds(507, 417, 223, 29);

		lblMonitorao_1 = new Label(compositePrincipal, SWT.NONE);
		lblMonitorao_1.setFont(SWTResourceManager.getFont("Segoe UI", 10,
				SWT.BOLD));
		lblMonitorao_1.setText("Monitor:");
		lblMonitorao_1.setBounds(759, 396, 98, 15);

		comboMonit = new Combo(compositePrincipal, SWT.NONE);
		comboMonit.setBounds(755, 417, 223, 29);

		scrolledTabelaDeRelacoes = new ScrolledComposite(compositePrincipal,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledTabelaDeRelacoes.setBounds(507, 452, 482, 108);
		scrolledTabelaDeRelacoes.setExpandHorizontal(true);
		scrolledTabelaDeRelacoes.setExpandVertical(true);

		tableRelacionamento = new Table(scrolledTabelaDeRelacoes, SWT.BORDER
				| SWT.FULL_SELECTION);
		tableRelacionamento.setHeaderVisible(true);
		tableRelacionamento.setLinesVisible(true);
		scrolledTabelaDeRelacoes.setContent(tableRelacionamento);
		scrolledTabelaDeRelacoes.setMinSize(tableRelacionamento.computeSize(
				SWT.DEFAULT, SWT.DEFAULT));
		TableColumn coluna1 = new TableColumn(tableRelacionamento, SWT.NONE);
		coluna1.setText("Environment");
		coluna1.setWidth(235);

		TableColumn coluna2 = new TableColumn(tableRelacionamento, SWT.NONE);
		coluna2.setText("Monitor");
		coluna2.setWidth(140);

		lblRelacionamento = new Label(compositePrincipal, SWT.NONE);
		lblRelacionamento.setFont(SWTResourceManager.getFont("Segoe UI", 10,
				SWT.BOLD));
		lblRelacionamento.setText("Statement");
		lblRelacionamento.setBounds(507, 375, 116, 15);
		btnSalvarRequisito = new Button(compositePrincipal, SWT.NONE);
		btnSalvarRequisito.setToolTipText("Save Requirement");
		btnSalvarRequisito.setImage(ResourceManager.getPluginImage(
				"relaxPluginSource", "icons/icon_salvar.png"));

		btnSalvarRequisito.setBounds(12, 12, 37, 30);
		btnAddRelacionamento = new Button(compositePrincipal, SWT.NONE);
		btnAddRelacionamento.setToolTipText("Add statement");
		btnAddRelacionamento.setImage(ResourceManager.getPluginImage(
				"relaxPluginSource", "icons/icon_adicionaritem.png"));
		btnAddRelacionamento.setBounds(995, 410, 40, 40);
		btnRemoverRelacionamento = new Button(compositePrincipal, SWT.NONE);
		btnRemoverRelacionamento.setToolTipText("Remove statement");
		btnRemoverRelacionamento.setImage(ResourceManager.getPluginImage(
				"relaxPluginSource", "icons/icon_removeritem.png"));
		btnRemoverRelacionamento.setBounds(995, 465, 40, 40);

		txtMonitoracao = new Text(compositePrincipal, SWT.BORDER);
		txtMonitoracao.setBounds(507, 247, 482, 21);

		txtAmbiente = new Text(compositePrincipal, SWT.BORDER);
		txtAmbiente.setBounds(507, 93, 482, 21);

		Label lblId = new Label(compositePrincipal, SWT.NONE);
		lblId.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblId.setBounds(13, 72, 27, 15);
		lblId.setText("Id:");

		txtId = new Label(compositePrincipal, SWT.NONE);
		txtId.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		txtId.setBounds(46, 72, 55, 15);
		txtId.setText("<idReq>");

		scrolledComposite_5 = new ScrolledComposite(compositePrincipal,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite_5.setShowFocusedControl(true);
		scrolledComposite_5.setBounds(10, 162, 472, 122);
		scrolledComposite_5.setExpandHorizontal(true);
		scrolledComposite_5.setExpandVertical(true);

		txtRequisito = new StyledText(scrolledComposite_5, SWT.BORDER
				| SWT.V_SCROLL | SWT.MULTI | SWT.WRAP);
		txtRequisito.setFont(SWTResourceManager.getFont("Segoe UI", 11,
				SWT.NORMAL));
		scrolledComposite_5.setContent(txtRequisito);
		scrolledComposite_5.setMinSize(txtRequisito.computeSize(SWT.DEFAULT,
				SWT.DEFAULT));

		this.txtRequisito.setLineSpacing(2);
		Label lblNome = new Label(compositePrincipal, SWT.NONE);
		lblNome.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
		lblNome.setBounds(10, 94, 91, 15);
		lblNome.setText("Name:");

		txtNome = new Text(compositePrincipal, SWT.BORDER);
		txtNome.setBounds(10, 114, 472, 21);

		btnAddDependence = new Button(compositePrincipal, SWT.NONE);
		btnAddDependence.setToolTipText("Add dependence");
		btnAddDependence.setImage(ResourceManager.getPluginImage(
				"relaxPluginSource", "icons/icon_adicionaritem.png"));
		btnAddDependence.setBounds(432, 314, 40, 40);

		btnRemoveDependence = new Button(compositePrincipal, SWT.NONE);
		btnRemoveDependence.setToolTipText("Remove dependence");
		btnRemoveDependence.setImage(ResourceManager.getPluginImage(
				"relaxPluginSource", "icons/icon_removeritem.png"));
		btnRemoveDependence.setBounds(432, 360, 40, 40);

		comboRequisitos = new Combo(compositePrincipal, SWT.NONE);
		comboRequisitos.setBounds(10, 314, 406, 29);

		label = new Label(compositePrincipal, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setBounds(10, 48, 1025, 2);

		InicializadorDeControladores.getInstance().filtro(this);
		desabilitarTodosOsComponentes();
	}

	@PreDestroy
	public void dispose() {
	}

	@Focus
	public void setFocus() {
		// TODO Set the focus to control
	}

	public void validarCombosDeRelacao(String caracteristicaAmbiental,
			String caracteristicaMonitoracao) {
		String[] itensDeAmb = this.comboAmb.getItems();
		String[] itensDeMonit = this.comboMonit.getItems();

		for (int count1 = 0; count1 < itensDeAmb.length; count1++) {
			if (itensDeAmb[count1].equalsIgnoreCase(caracteristicaAmbiental)) {
				this.comboAmb.remove(count1);
			}
		}
		for (int count2 = 0; count2 < itensDeMonit.length; count2++) {
			if (itensDeMonit[count2]
					.equalsIgnoreCase(caracteristicaMonitoracao)) {
				this.comboMonit.remove(count2);
				return;
			}
		}

	}

	public void habilitarTodosOsComponentes() {
		this.txtNome.setEnabled(true);
		this.txtAmbiente.setEnabled(true);
		this.txtMonitoracao.setEnabled(true);
		this.txtRequisito.setEnabled(true);
		this.btnAddDependence.setEnabled(true);
		this.btnAddAmbiente.setEnabled(true);
		this.btnAddMonit.setEnabled(true);
		this.btnAddRelacionamento.setEnabled(true);
		this.btnSalvarRequisito.setEnabled(true);
		this.btnRemoveDependence.setEnabled(true);
		this.btnRemoveAmbiente.setEnabled(true);
		this.btnRemoveMonit.setEnabled(true);
		this.btnRemoverRelacionamento.setEnabled(true);
		this.tableRelacionamento.setEnabled(true);
		this.listAmbiente.setEnabled(true);
		this.listMonitoracao.setEnabled(true);
		this.listDependencia.setEnabled(true);
		this.comboAmb.setEnabled(true);
		this.comboMonit.setEnabled(true);
	}

	public boolean existeCamposVazios() {
		if (txtRequisito.getText().isEmpty() || txtNome.getText().isEmpty()) {
			if (tableRelacionamento.getItemCount() > 0) {
				if (listAmbiente.getItemCount() > 0
						&& listMonitoracao.getItemCount() > 0) {
					return false;
				} else {
					return true;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	public void desabilitarTodosOsComponentes() {
		this.txtNome.setEnabled(false);
		this.txtAmbiente.setEnabled(false);
		this.txtMonitoracao.setEnabled(false);
		this.txtRequisito.setEnabled(false);
		this.btnAddDependence.setEnabled(false);
		this.btnAddAmbiente.setEnabled(false);
		this.btnAddMonit.setEnabled(false);
		this.btnAddRelacionamento.setEnabled(false);
		this.btnSalvarRequisito.setEnabled(false);
		this.btnRemoveDependence.setEnabled(false);
		this.btnRemoveAmbiente.setEnabled(false);
		this.btnRemoveMonit.setEnabled(false);
		this.btnRemoverRelacionamento.setEnabled(false);
		this.tableRelacionamento.setEnabled(false);
		this.listAmbiente.setEnabled(false);
		this.listMonitoracao.setEnabled(false);
		this.listDependencia.setEnabled(false);
		this.comboAmb.setEnabled(false);
		this.comboMonit.setEnabled(false);
	}

	public void limparDadosDoEditor() {
		this.txtNome.setText("");
		this.txtRequisito.setText("");
		this.txtAmbiente.setText("");
		this.txtMonitoracao.setText("");
		this.listAmbiente.removeAll();
		this.listMonitoracao.removeAll();
		this.listDependencia.removeAll();
		this.tableRelacionamento.removeAll();
		this.comboAmb.removeAll();
		this.comboMonit.removeAll();
	}

	public void verificarTodasPalavrasChave(String textoTotal,
			DicionarioDeDados dicionarioDeDados) {

		String[] arrayTextoTotal = textoTotal.split(" ");
		StyleRange styleRange = new StyleRange();

		int inicio = 0, termometroDePalavra = 0;
		for (int count = 0; count < arrayTextoTotal.length; count++) {
			String textoDeQuebra = arrayTextoTotal[count];
			for (String termo : dicionarioDeDados.getTermos()) {
				if (count == 0) {
					inicio = count;
				} else if (count > 0) {
					inicio = count + termometroDePalavra;
				}
				if (!termo.contains(" ")
						&& termo.equalsIgnoreCase(textoDeQuebra)) {
					styleRange.start = inicio;
					styleRange.length = textoDeQuebra.length();
					styleRange.fontStyle = SWT.BOLD;
					styleRange.foreground = corDaPalavraChave;
					txtRequisito.setStyleRange(styleRange);
				} else {
					String[] arrayDeMembrosDeTermo = termo.split(" ");
					if (arrayDeMembrosDeTermo.length > 0) {
						for (String termoMembroTemp : arrayDeMembrosDeTermo) {
							if (termoMembroTemp.equalsIgnoreCase(textoDeQuebra)) {
								styleRange.start = inicio;
								styleRange.length = textoDeQuebra.length();
								styleRange.fontStyle = SWT.BOLD;
								styleRange.foreground = corDaPalavraChave;
								txtRequisito.setStyleRange(styleRange);
								break;
							} else {
								continue;
							}
						}
					}
				}

			}
			termometroDePalavra += textoDeQuebra.length();
		}
	}
	
	public void verificarTodasPalavrasNaoChave(String textoTotal,
			DicionarioDeDados dicionarioDeDados){
		String[] arrayTextoTotal = textoTotal.split(" ");
		StyleRange styleRange = new StyleRange();
		int inicio = 0, termometroDePalavra = 0;
		for (int count = 0; count < arrayTextoTotal.length; count++) {
			String textoDeQuebra = arrayTextoTotal[count];
			for (String termo : dicionarioDeDados.getTermos()) {
				if (count == 0) {
					inicio = count;
				} else if (count > 0) {
					inicio = count + termometroDePalavra;
				}
				if (!termo.contains(" ")
						&& !termo.equalsIgnoreCase(textoDeQuebra)) {
					styleRange.start = inicio;
					styleRange.length = textoDeQuebra.length();
					styleRange.fontStyle = SWT.NONE;
					styleRange.foreground = corDaPalavraNormal;
					txtRequisito.setStyleRange(styleRange);
					continue;
				} else {
					String[] arrayDeMembrosDeTermo = termo.split(" ");
					if (arrayDeMembrosDeTermo.length > 0) {
						for (String termoMembroTemp : arrayDeMembrosDeTermo) {
							if (!termoMembroTemp
									.equalsIgnoreCase(textoDeQuebra)) {
								styleRange.start = inicio;
								styleRange.length = textoDeQuebra.length();
								styleRange.fontStyle = SWT.NONE;
								styleRange.foreground = corDaPalavraNormal;
								txtRequisito.setStyleRange(styleRange);
								break;
							} else {
								continue;
							}
						}
					}
				}
			}
			termometroDePalavra += textoDeQuebra.length();
		}
	}
	
	public void verificarUltimaPalavraChave(String textoTotal, 
			DicionarioDeDados dicionarioDeDados){
		String[] arrayTextoTotal = textoTotal.split(" ");
		StyleRange styleRange = new StyleRange();

		int inicio = 0;
		try {
			if (arrayTextoTotal.length > 0) {
				String textoDeQuebra = arrayTextoTotal[arrayTextoTotal.length - 1];

				inicio = textoTotal.length() - (textoDeQuebra.length() + 1);
				if (inicio < 0) {
					inicio = 0;
				}

				for (String termo : dicionarioDeDados.getTermos()) {
					if (!termo.contains(" ")
							&& termo.equalsIgnoreCase(textoDeQuebra)) {
						styleRange.start = inicio;
						styleRange.length = textoDeQuebra.length() + 1;
						styleRange.fontStyle = SWT.NONE;
						styleRange.foreground = corDaPalavraChave;
						txtRequisito.setStyleRange(styleRange);
						break;
					}
				}
			}
		} catch (Exception erro) {
			GerenciadorDeLog.getMyInstance().registrarLog(
					"Erro: " + erro.getMessage(), TipoDeLog.ERROR, getClass());
		}
	}
	
	public void verificarUltimaPalavraNaoChave(String textoTotal,
			DicionarioDeDados dicionarioDeDados) {
		String[] arrayTextoTotal = textoTotal.split(" ");
		StyleRange styleRange = new StyleRange();

		int inicio = 0;
		try {
			if (arrayTextoTotal.length > 0) {
				String textoDeQuebra = arrayTextoTotal[arrayTextoTotal.length - 1];

				inicio = textoTotal.length() - (textoDeQuebra.length() + 1);
				if (inicio < 0) {
					inicio = 0;
				}

				for (String termo : dicionarioDeDados.getTermos()) {
					if (!termo.contains(" ")
							&& !termo.equalsIgnoreCase(textoDeQuebra)) {
						styleRange.start = inicio;
						styleRange.length = textoDeQuebra.length() + 1;
						styleRange.fontStyle = SWT.NONE;
						styleRange.foreground = corDaPalavraNormal;
						txtRequisito.setStyleRange(styleRange);
						break;
					}
				}
			}
		} catch (Exception erro) {
			GerenciadorDeLog.getMyInstance().registrarLog(
					"Erro: " + erro.getMessage(), TipoDeLog.ERROR, getClass());
		}
	}

	public List getListDependencia() {
		return listDependencia;
	}

	public void setListDependencia(List listDependencia) {
		this.listDependencia = listDependencia;
	}

	public StyledText getTextRequisito() {
		return this.txtRequisito;
	}

	public Table getTableRelacionamento() {
		return this.tableRelacionamento;
	}

	public Button getBtnAddAmbiente() {
		return this.btnAddAmbiente;
	}

	public Button getRemoveAmbiente() {
		return this.btnRemoveAmbiente;
	}

	public Button getBtnAddMonit() {
		return this.btnAddMonit;
	}

	public Button getBtnSalvarRequisito() {
		return this.btnSalvarRequisito;
	}

	public Button getAddRelacionamento() {
		return this.btnAddRelacionamento;
	}

	public Button getRemoveRelacionamento() {
		return this.btnRemoverRelacionamento;
	}

	public Button getBtnRemoveMOnit() {
		return this.btnRemoveMonit;
	}

	public List getListAmbiente() {
		return this.listAmbiente;
	}

	public List getListMonitoracao() {
		return this.listMonitoracao;
	}

	public Combo getComboAmb() {
		return this.comboAmb;
	}

	public Combo getComboMonit() {
		return this.comboMonit;
	}

	public StyledText getTxtRequisito() {
		return txtRequisito;
	}

	public void setTxtRequisito(StyledText txtRequisito) {
		this.txtRequisito = txtRequisito;
	}

	public Button getBtnRemoveAmbiente() {
		return btnRemoveAmbiente;
	}

	public void setBtnRemoveAmbiente(Button btnRemoveAmbiente) {
		this.btnRemoveAmbiente = btnRemoveAmbiente;
	}

	public void setBtnSalvarRequisito(Button btnSalvarRequisito) {
		this.btnSalvarRequisito = btnSalvarRequisito;
	}

	public Button getBtnAddRelacionamento() {
		return btnAddRelacionamento;
	}

	public void setBtnAddRelacionamento(Button btnAddRelacionamento) {
		this.btnAddRelacionamento = btnAddRelacionamento;
	}

	public Button getBtnRemoverRelacionamento() {
		return btnRemoverRelacionamento;
	}

	public void setBtnRemoverRelacionamento(Button btnRemoverRelacionamento) {
		this.btnRemoverRelacionamento = btnRemoverRelacionamento;
	}

	public Button getBtnRemoveMonit() {
		return btnRemoveMonit;
	}

	public void setBtnRemoveMonit(Button btnRemoveMonit) {
		this.btnRemoveMonit = btnRemoveMonit;
	}

	public ScrolledComposite getScrolledComposite() {
		return scrolledComposite;
	}

	public void setScrolledComposite(ScrolledComposite scrolledComposite) {
		this.scrolledComposite = scrolledComposite;
	}

	public ScrolledComposite getScrolledComposite_1() {
		return scrolledComposite_1;
	}

	public void setScrolledComposite_1(ScrolledComposite scrolledComposite_1) {
		this.scrolledComposite_1 = scrolledComposite_1;
	}

	public ScrolledComposite getScrolledComposite_2() {
		return scrolledComposite_2;
	}

	public void setScrolledComposite_2(ScrolledComposite scrolledComposite_2) {
		this.scrolledComposite_2 = scrolledComposite_2;
	}

	public ScrolledComposite getScrolledTabelaDeRelacoes() {
		return scrolledTabelaDeRelacoes;
	}

	public void setScrolledTabelaDeRelacoes(
			ScrolledComposite scrolledComposite_3) {
		this.scrolledTabelaDeRelacoes = scrolledComposite_3;
	}

	public Label getLblDescrio() {
		return lblDescrio;
	}

	public void setLblDescrio(Label lblDescrio) {
		this.lblDescrio = lblDescrio;
	}

	public Label getLblAmbiente() {
		return lblAmbiente;
	}

	public void setLblAmbiente(Label lblAmbiente) {
		this.lblAmbiente = lblAmbiente;
	}

	public Label getLblAmbiente_1() {
		return lblAmbiente_1;
	}

	public void setLblAmbiente_1(Label lblAmbiente_1) {
		this.lblAmbiente_1 = lblAmbiente_1;
	}

	public Label getLblRelacionamento() {
		return lblRelacionamento;
	}

	public void setLblRelacionamento(Label lblRelacionamento) {
		this.lblRelacionamento = lblRelacionamento;
	}

	public Label getLblMonitorao() {
		return lblMonitorao;
	}

	public void setLblMonitorao(Label lblMonitorao) {
		this.lblMonitorao = lblMonitorao;
	}

	public Label getLblDependncias() {
		return lblDependncias;
	}

	public void setLblDependncias(Label lblDependncias) {
		this.lblDependncias = lblDependncias;
	}

	public Label getLblMonitorao_1() {
		return lblMonitorao_1;
	}

	public void setLblMonitorao_1(Label lblMonitorao_1) {
		this.lblMonitorao_1 = lblMonitorao_1;
	}

	public void setTableRelacionamento(Table tableRelacionamento) {
		this.tableRelacionamento = tableRelacionamento;
	}

	public void setBtnAddAmbiente(Button btnAddAmbiente) {
		this.btnAddAmbiente = btnAddAmbiente;
	}

	public void setBtnAddMonit(Button btnAddMonit) {
		this.btnAddMonit = btnAddMonit;
	}

	public void setListAmbiente(List listAmbiente) {
		this.listAmbiente = listAmbiente;
	}

	public void setListMonitoracao(List listMonitoracao) {
		this.listMonitoracao = listMonitoracao;
	}

	public void setComboAmb(Combo comboAmb) {
		this.comboAmb = comboAmb;
	}

	public void setComboMonit(Combo comboMonit) {
		this.comboMonit = comboMonit;
	}

	public Text getTxtMonitoracao() {
		return txtMonitoracao;
	}

	public void setTxtMonitoracao(Text txtMonitoracao) {
		this.txtMonitoracao = txtMonitoracao;
	}

	public Text getTxtAmbiente() {
		return txtAmbiente;
	}

	public void setTxtAmbiente(Text txtAmbiente) {
		this.txtAmbiente = txtAmbiente;
	}

	public Label getTxtId() {
		return this.txtId;
	}

	public void setTxtId(Label txtId) {
		this.txtId = txtId;
	}

	public static Font getFontCabecalhoArvore() {
		return fontCabecalhoArvore;
	}

	public static void setFontCabecalhoArvore(Font fontCabecalhoArvore) {
		RelaxParte.fontCabecalhoArvore = fontCabecalhoArvore;
	}

	public static Font getFontItemArvore() {
		return fontItemArvore;
	}

	public static void setFontItemArvore(Font fontItemArvore) {
		RelaxParte.fontItemArvore = fontItemArvore;
	}

	public static Font getFontSubItemArvore() {
		return fontSubItemArvore;
	}

	public static void setFontSubItemArvore(Font fontSubItemArvore) {
		RelaxParte.fontSubItemArvore = fontSubItemArvore;
	}

	public ScrolledComposite getScrolledComposite_5() {
		return scrolledComposite_5;
	}

	public void setScrolledComposite_5(ScrolledComposite scrolledComposite_5) {
		this.scrolledComposite_5 = scrolledComposite_5;
	}

	public Color getCorDaPalavraChave() {
		return corDaPalavraChave;
	}

	public void setCorDaPalavraChave(Color corDaPalavraChave) {
		this.corDaPalavraChave = corDaPalavraChave;
	}

	public Color getCorDaPalavraNormal() {
		return corDaPalavraNormal;
	}

	public void setCorDaPalavraNormal(Color corDaPalavraNormal) {
		this.corDaPalavraNormal = corDaPalavraNormal;
	}

	public Text getTxtNome() {
		return txtNome;
	}

	public void setTxtNome(Text txtNome) {
		this.txtNome = txtNome;
	}

	public Combo getComboRequisitos() {
		return comboRequisitos;
	}

	public void setComboRequisitos(Combo comboRequisitos) {
		this.comboRequisitos = comboRequisitos;
	}

	public Button getBtnAddDependence() {
		return btnAddDependence;
	}

	public void setBtnAddDependence(Button btnAddDependence) {
		this.btnAddDependence = btnAddDependence;
	}

	public Button getBtnRemoveDependence() {
		return btnRemoveDependence;
	}

	public void setBtnRemoveDependence(Button btnRemoveDependence) {
		this.btnRemoveDependence = btnRemoveDependence;
	}
}
