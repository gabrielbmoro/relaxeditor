/*******************************************************************************
 * Copyright (c) 2010 - 2013 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Lars Vogel <lars.Vogel@gmail.com> - Bug 419770
 *******************************************************************************/
package br.unipampa.lesa.apresentacao;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.TipoDeLog;

public class SobreHandler {
	@Execute
	public void execute(Shell shell) {
		GerenciadorDeLog.getMyInstance().registrarLog("Inicializando o componente Handler", TipoDeLog.INFO,
				getClass());
		MessageDialog.openInformation(shell, "About", 
				"The present tool is an editor and relax interpreter, "
				+ "from it you can generate expressions to Relax language \n"
				+ "based on requirements written in informal "
				+ "language (using indicative operators of language).\n"
				+ "\nVersion 1.0"
				+ "\n\nTeam: Gabriel Bronzatti Moro (development), João Pablo Silva da Silva (Supervisor), Douglas Freitas Almeida (support) e Fernando Oliveira (support).");
	}
}
