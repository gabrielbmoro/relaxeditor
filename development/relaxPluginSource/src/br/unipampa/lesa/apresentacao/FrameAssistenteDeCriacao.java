package br.unipampa.lesa.apresentacao;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.wb.swt.ResourceManager;

public class FrameAssistenteDeCriacao {

	protected Shell shlCriarProjetoDe;
	private Text txtProjeto;
	private Text txtDiretorio;
	private Button btnBuscar, btnCriar;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			FrameAssistenteDeCriacao window = new FrameAssistenteDeCriacao();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public FrameAssistenteDeCriacao(){
		createContents();
	}
	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		shlCriarProjetoDe.setLocation(400, 180);
		shlCriarProjetoDe.open();
		shlCriarProjetoDe.layout();
		while (!shlCriarProjetoDe.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlCriarProjetoDe = new Shell();
		shlCriarProjetoDe.setImage(ResourceManager.getPluginImage("relaxPluginSource", "icons/logoTipo.png"));
		shlCriarProjetoDe.setSize(446, 236);
		shlCriarProjetoDe.setText("Create Project of Specification");
		
		Label lblNomeDoProjeto = new Label(shlCriarProjetoDe, SWT.NONE);
		lblNomeDoProjeto.setBounds(20, 12, 107, 15);
		lblNomeDoProjeto.setText("Name of Project:");
		
		txtProjeto = new Text(shlCriarProjetoDe, SWT.BORDER);
		txtProjeto.setBounds(20, 37, 346, 21);
		
		Label lblDiretrio = new Label(shlCriarProjetoDe, SWT.NONE);
		lblDiretrio.setText("Path:");
		lblDiretrio.setBounds(20, 73, 68, 15);
		
		txtDiretorio = new Text(shlCriarProjetoDe, SWT.BORDER);
		txtDiretorio.setToolTipText("Caso n\u00E3o selecionar um diret\u00F3rio, o projeto ser\u00E1 criado em seu Workspace");
		txtDiretorio.setBounds(20, 94, 215, 21);
		
		btnBuscar = new Button(shlCriarProjetoDe, SWT.NONE);
		btnBuscar.setImage(ResourceManager.getPluginImage("relaxPluginSource", "icons/icon_pesquisarpasta.png"));
		btnBuscar.setBounds(241, 92, 91, 34);
		btnBuscar.setText("Find");
		
		btnCriar = new Button(shlCriarProjetoDe, SWT.NONE);
		btnCriar.setImage(ResourceManager.getPluginImage("relaxPluginSource", "icons/icon_adicionarpasta.png"));
		btnCriar.setBounds(20, 158, 107, 34);
		btnCriar.setText("Create");

	}
	
	public Shell getShlCriarProjetoDe() {
		return shlCriarProjetoDe;
	}

	public void setShlCriarProjetoDe(Shell shlCriarProjetoDe) {
		this.shlCriarProjetoDe = shlCriarProjetoDe;
	}

	public Text getTxtProjeto() {
		return txtProjeto;
	}

	public void setTxtProjeto(Text txtProjeto) {
		this.txtProjeto = txtProjeto;
	}

	public Text getTxtDiretorio() {
		return txtDiretorio;
	}

	public void setTxtDiretorio(Text txtDiretorio) {
		this.txtDiretorio = txtDiretorio;
	}

	public Button getBtnBuscar() {
		return btnBuscar;
	}

	public void setBtnBuscar(Button btnBuscar) {
		this.btnBuscar = btnBuscar;
	}

	public Button getBtnCriar() {
		return btnCriar;
	}

	public void setBtnCriar(Button btnCriar) {
		this.btnCriar = btnCriar;
	}
	
	
}
