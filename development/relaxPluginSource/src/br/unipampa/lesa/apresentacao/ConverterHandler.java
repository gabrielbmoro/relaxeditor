/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Lars Vogel <lars.Vogel@gmail.com> - Bug 419770
 *******************************************************************************/
package br.unipampa.lesa.apresentacao;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.swt.widgets.Shell;
import br.unipampa.lesa.controle.ControleAbrirHandler;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.TipoDeLog;

public class ConverterHandler {
	
	private Shell shell;	
	 
	@Execute
	public void execute(Shell shell){
		GerenciadorDeLog.getMyInstance().registrarLog("Inicializando o componente Handler", TipoDeLog.INFO,
				getClass());
		this.shell = shell;
//		controleAbrirHandler = new ControleAbrirHandler(this);
		InicializadorDeControladores.getInstance().filtro(this);
		InicializadorDeControladores.getInstance().getControleRelaxParte().reproduzirEventoDeConversao();
	}

	public Shell getShell() {
		return shell;
	}

	public void setShell(Shell shell) {
		this.shell = shell;
	}	
	
	
}
