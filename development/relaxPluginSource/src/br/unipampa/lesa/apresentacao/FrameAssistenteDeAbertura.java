package br.unipampa.lesa.apresentacao;

import java.io.File;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.wb.swt.ResourceManager;
import br.unipampa.lesa.modelo.repositorio.GerenciadorDeEsquema;
import br.unipampa.lesa.modelo.repositorio.RepositorioDeEspecificacao;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.TipoDeLog;
import br.unipampa.lesa.servico.ValidadorDeArquivo;

public class FrameAssistenteDeAbertura {

	protected Shell shlOpenProjectOf;
	private Button buttonFind;
	private FileDialog dialog;
	private File file,filePai;
	
	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlOpenProjectOf.setLocation(400, 180);
		shlOpenProjectOf.open();
		shlOpenProjectOf.layout();
		while (!shlOpenProjectOf.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlOpenProjectOf = new Shell();
		shlOpenProjectOf.setImage(ResourceManager.getPluginImage("relaxPluginSource", "icons/logoTipo.png"));
		shlOpenProjectOf.setSize(446, 182);
		shlOpenProjectOf.setText("Open Project of Specification");
		
		buttonFind = new Button(shlOpenProjectOf, SWT.NONE);
		buttonFind.setText("Find your Path");
		buttonFind.setImage(ResourceManager.getPluginImage("relaxPluginSource", "icons/icon_pesquisarpasta.png"));
		buttonFind.setBounds(121, 36, 176, 49);

		buttonFind.addListener(SWT.MouseUp, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				GerenciadorDeLog.getMyInstance().registrarLog("Inicializando elementos controladores do ControleAbrirHandler",
						TipoDeLog.INFO, getClass());
				try{
				dialog = new FileDialog(shlOpenProjectOf);
				dialog.open();
			    file= new File(dialog.getFilterPath());
			    filePai = file.getParentFile();
				String diretorioDeProjeto = file.getName();
				
				String diretorioEscolhido = filePai.getAbsolutePath()+File.separator;
					
				GerenciadorDeEsquema.PATH = diretorioEscolhido;
				if(!GerenciadorDeEsquema.PATH.isEmpty()&&ValidadorDeArquivo.verificarExistenciaDoArquivoIndexador(GerenciadorDeEsquema.PATH+file.getName()+File.separator)){
					RepositorioDeEspecificacao.getMyInstance().recuperar(diretorioDeProjeto);
					shlOpenProjectOf.close();
				}else{
					GeradorDeMensagem.exibirMensagemDeErro("Please, select a valid directory!");
				}
				
			}catch(Exception erro){
				GerenciadorDeLog.getMyInstance().registrarLog("Erro: "+ erro.getMessage(), TipoDeLog.ERROR, getClass());
			}
			}
		});
	}

	public Button getButtonFind() {
		return buttonFind;
	}

	public void setButtonFind(Button buttonFind) {
		this.buttonFind = buttonFind;
	}

	public Shell getShlOpenProjectOf() {
		return shlOpenProjectOf;
	}

	public void setShlOpenProjectOf(Shell shlOpenProjectOf) {
		this.shlOpenProjectOf = shlOpenProjectOf;
	}
	
}
