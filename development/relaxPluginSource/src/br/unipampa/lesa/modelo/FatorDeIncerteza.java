package br.unipampa.lesa.modelo;

import java.util.ArrayList;

/**
 * Ambiente.java
 * 
 * <ul>
 * <li><b>Propósito:</b></li>
 * Representa uma abstração dos métodos que os fatores de {@link Monitoracao} e
 * {@link Ambiente} realizaram como objetos contratores (contrato firmado).
 * 
 * <li><b>Instruções de uso:</b></li>
 * Deve ser utilizada como interface de classe.
 * </ul>
 * 
 * @author gabrielbmoro
 * @version 1.0
 * @since 06/01/2015
 */
public class FatorDeIncerteza {

	private ArrayList<String> caracteristicas;
	private TipoDeIncerteza tipoDeIncerteza;

	public FatorDeIncerteza(TipoDeIncerteza tipoDeIncerteza) {
		this.tipoDeIncerteza = tipoDeIncerteza;
		this.caracteristicas = new ArrayList<String>();
	}

	/**
	 * adicionarItem
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por adicionar item a lista de itens do estado da
	 * classe.
	 * </ul>
	 * 
	 * @param item
	 *            de tipo {@link String}
	 */
	public void adicionarItem(String item) {
		caracteristicas.add(item);
	}

	/**
	 * removerItem
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por remover item a lista de itens do estado da classe.
	 * </ul>
	 * 
	 * @param indice
	 *            de tipo {@link Integer}
	 * @param item
	 *            de tipo {@link String}
	 */
	public boolean removerItem(int indice) {
		if (indice >= 0 && caracteristicas.size() != 0) {
			caracteristicas.remove(indice);
			return true;
		} else {
			return true;
		}
	}

	/**
	 * recuperarItem
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por recuperar determinado item a lista de itens do
	 * estado da classe.
	 * </ul>
	 * 
	 * @param indice
	 *            de tipo {@link Integer}
	 */
	public String recuperarItem(int indice) {
		if (indice >= 0 && indice < caracteristicas.size()) {
			return caracteristicas.get(indice);
		} else {
			return null;
		}
	}

	/**
	 * editarItem
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por editar item da lista de itens do estado da classe.
	 * </ul>
	 * 
	 * @param indice
	 *            de tipo {@link Integer}
	 * @param novoValor
	 *            de tipo {@link String}
	 */
	public boolean editarItem(int indice, String novoValor) {
		if (indice >= 0 && indice < caracteristicas.size() && novoValor != null) {
			caracteristicas.set(indice, novoValor);
			return true;
		} else {
			return false;
		}
	}

	public ArrayList<String> getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(ArrayList<String> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public TipoDeIncerteza getTipoDeIncerteza() {
		return tipoDeIncerteza;
	}

	public void setTipoDeIncerteza(TipoDeIncerteza tipoDeIncerteza) {
		this.tipoDeIncerteza = tipoDeIncerteza;
	}
	
	
}
