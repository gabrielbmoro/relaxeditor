package br.unipampa.lesa.modelo;

import java.util.LinkedList;
import java.util.regex.Pattern;

public class AnalisadorLexicoDeRequisito {

	private LinkedList<ItemRegraDeProducao> itensDeProducao;

	public AnalisadorLexicoDeRequisito() {
		carregadorDePatterns();
	}

	private void carregadorDePatterns() {
		this.itensDeProducao = new LinkedList<ItemRegraDeProducao>();
		this.itensDeProducao.add(new ItemRegraDeProducao(" shall ", Pattern
				.compile("\\w+ shall \\w+|shall \\w+| shall \\w+")));
		this.itensDeProducao
				.add(new ItemRegraDeProducao(
						" may ",
						" or may ",
						Pattern.compile("\\w+ may \\w* or \\w+|may \\w* or  may \\w+| \\w+ may \\w* or may \\w+")));
		this.itensDeProducao
				.add(new ItemRegraDeProducao(
						" eventually ",
						Pattern.compile("\\w+ eventually \\w+|eventually \\w+| eventually \\w+")));
		this.itensDeProducao.add(new ItemRegraDeProducao(" until ", Pattern
				.compile("\\b until \\w+")));
		this.itensDeProducao.add(new ItemRegraDeProducao(" before ", Pattern
				.compile("\\w+ before \\w+|before \\w+| before \\w+")));
		this.itensDeProducao.add(new ItemRegraDeProducao(" after ", Pattern
				.compile("\\w+ after \\w+|after \\w+| after \\w+")));
		this.itensDeProducao
				.add(new ItemRegraDeProducao(
						" in ",
						Pattern.compile("\\w+ in \\d+:\\d+ \\w+|\\w+ in \\d+ \\w+| in \\d+:\\d+ \\w+| in \\d+ \\w+")));
		this.itensDeProducao
				.add(new ItemRegraDeProducao(
						" as close as possible to ",
						Pattern.compile("\\w+ as close as possible to \\d+ \\w*|as close as possible to \\d+ \\w*| as close as possible to \\d+ \\w*| \\w+\\s as close as possible to \\d+ \\w*|"
								+ "\\w+ as close as possible to \\d+(,|.)\\d+ \\w*|as close as possible to \\d+(,|.)\\d+ \\w*| as close as possible to \\d+(,|.)\\d+ \\w*")));
		this.itensDeProducao
				.add(new ItemRegraDeProducao(
						" as early as possible ",
						Pattern.compile("\\w+ as early as possible \\w+|as early as possible \\w+| as early as possible \\w+")));
		this.itensDeProducao
				.add(new ItemRegraDeProducao(
						" as late as possible ",
						Pattern.compile("\\w+ as late as possible \\w+|as late as possible \\w+| as late as possible \\w+")));
		this.itensDeProducao
				.add(new ItemRegraDeProducao(
						" as many as possible ",
						Pattern.compile("\\w+ as many as possible \\w+|as many as possible \\w+| as many as possible \\w+")));
		this.itensDeProducao
				.add(new ItemRegraDeProducao(
						" as few as possible ",
						Pattern.compile("\\w+ as few as possible \\w+|as few as possible \\w+| as few as possible \\w+")));
		this.itensDeProducao
				.add(new ItemRegraDeProducao(
						" eventually ",
						Pattern.compile("\\w+ eventually \\w+|eventually \\w+| eventually \\w+")));
		// this.itensDeProducao.add(new ItemRegraDeProducao(" ",
		// Pattern.compile("\\w+ \\w+ \\w+ \\w+")));
	}

	private int auxiliaContador(boolean resp) {
		if (resp == false) {
			return 1;
		} else {
			return 0;
		}
	}

	public int verificarValidadeDaExpressao(String expressao) {
		expressao= expressao + " ";
		expressao = expressao.toLowerCase();
		int resultadoDaVerificacao = 0;
		for (ItemRegraDeProducao itemDeProducao : itensDeProducao) {
			if (itemDeProducao.getOperador1() != null) {
				if ((expressao.contains(itemDeProducao.getOperador()) || expressao
						.contains(itemDeProducao.getOperador().replaceAll(" ",
								"")))
						&& (expressao.contains(itemDeProducao.getOperador1()) || expressao
								.contains(itemDeProducao.getOperador1()
										.replaceAll(" ", "")))) {
					resultadoDaVerificacao += auxiliaContador(itemDeProducao
							.getPadraoObedecido().matcher(expressao).find());
				}
			} else {
				if (expressao.contains(itemDeProducao.getOperador())
						|| expressao.contains(itemDeProducao.getOperador()
								.replaceAll(" ", "")+" ")) {
					resultadoDaVerificacao += auxiliaContador(itemDeProducao
							.getPadraoObedecido().matcher(expressao).find());
				}
			}
		}
		return resultadoDaVerificacao;
	}
}
