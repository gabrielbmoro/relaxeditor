package br.unipampa.lesa.modelo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Requisito.java
 * 
 * <ul>
 * <li><b>Propósito:</b></li>
 * Abstração dos requisitos que podem ser interpretados pelo
 * {@link Interpretador}, o qual deve estar descrito em linguagem relax para que
 * seja possível o mapeamento de seu estado.
 * <li><b>Instruções de uso:</b></li>
 * Deve ser utilizado de acordo para cadastro das informações contidas em um
 * requisito, o objeto requisito encapsula todo o estado (id, descrição,
 * fatores) e comportamento definidos pelo mesmo.
 * </ul>
 * 
 * @author gabrielbmoro
 * @version 1.0
 * @since 19/11/2014
 */

public class Requisito {
	
	private int id;
	private String nome;
	private String descricao;
	private ArrayList<String> dependecias;
	private FatorDeIncerteza fatorAmbiental;
	private FatorDeIncerteza fatorDeMonitoracao;
	private HashMap<Integer, FatorRelacao> relacoes;

	/**
	 * construtor
	 * <ul>
	 * <li><b>Propósito</b>
	 * </ul>
	 * Retornar um objeto requisito, configurado de acordo com as propriedades
	 * passadas por parâmetro.
	 */
	public Requisito() {
		this.dependecias = new ArrayList<String>();
		this.relacoes = new HashMap<>();
		this.fatorAmbiental = new FatorDeIncerteza(TipoDeIncerteza.AMBIENTE);
		this.fatorDeMonitoracao = new FatorDeIncerteza(
				TipoDeIncerteza.MONITORACAO);
		this.relacoes = new HashMap<>();
	}

	/**
	 * adicionarDependecia
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Adicionar um requisito nas suas próprias dependências.
	 * </ul>
	 * 
	 * @param requisito
	 *            de tipo {@link Requisito}
	 */
	public void adicionarDependencia(String nomeRequisito) {
		this.dependecias.add(nomeRequisito);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ArrayList<String> getDependecias() {
		return dependecias;
	}

	public void setDependecias(ArrayList<String> dependecias) {
		this.dependecias = dependecias;
	}

	public FatorDeIncerteza getFatorAmbiental() {
		return fatorAmbiental;
	}

	public void setFatorAmbiental(FatorDeIncerteza fatorAmbiental) {
		if (fatorAmbiental.getTipoDeIncerteza() == TipoDeIncerteza.AMBIENTE)
			this.fatorAmbiental = fatorAmbiental;
	}

	public FatorDeIncerteza getFatorDeMonitoracao() {
		return fatorDeMonitoracao;
	}

	public void setFatorDeMonitoracao(FatorDeIncerteza fatorDeMonitoracao) {
		if (fatorDeMonitoracao.getTipoDeIncerteza() == TipoDeIncerteza.MONITORACAO)
			this.fatorDeMonitoracao = fatorDeMonitoracao;
	}

	public HashMap<Integer, FatorRelacao> getRelacoes() {
		return relacoes;
	}

	public void setRelacoes(HashMap<Integer, FatorRelacao> relacoes) {
		this.relacoes = relacoes;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}