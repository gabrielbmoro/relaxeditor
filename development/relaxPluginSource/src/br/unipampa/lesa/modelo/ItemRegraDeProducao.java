package br.unipampa.lesa.modelo;

import java.util.regex.*;

public class ItemRegraDeProducao {

	private String operador;
	private String operador1;
	private Pattern padraoObedecido;
	
	public ItemRegraDeProducao(String operador, Pattern padrao){
		this.operador = operador;
		this.padraoObedecido = padrao;
	}
	public ItemRegraDeProducao(String operador, String operador1, Pattern padrao){
		this.operador = operador;
		this.operador1 = operador1;
		this.padraoObedecido = padrao;
	}
	public String getOperador() {
		return operador;
	}
	public void setOperador(String operador) {
		this.operador = operador;
	}
	public String getOperador1() {
		return operador1;
	}
	public void setOperador1(String operador1) {
		this.operador1 = operador1;
	}
	public Pattern getPadraoObedecido() {
		return padraoObedecido;
	}
	public void setPadraoObedecido(Pattern padraoObedecido) {
		this.padraoObedecido = padraoObedecido;
	}
}
