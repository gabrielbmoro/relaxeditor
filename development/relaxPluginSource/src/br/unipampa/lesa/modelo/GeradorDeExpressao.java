package br.unipampa.lesa.modelo;

import br.unipampa.lesa.servico.RefatoradorDeString;

public class GeradorDeExpressao {
	
	public String gerarExpressaoRelax(Requisito requisitoTeste) {
		requisitoTeste.setDescricao(
		RefatoradorDeString.prepararExpressaoParaGeracao(requisitoTeste.getDescricao())
		);
		if((requisitoTeste.getDescricao().contains(" OR ")
				||requisitoTeste.getDescricao().contains(" or "))
				&&(!requisitoTeste.getDescricao().toLowerCase().contains(" may "))){
				requisitoTeste.setDescricao(requisitoTeste.getDescricao().replaceAll(" or ", " "));
				requisitoTeste.setDescricao(requisitoTeste.getDescricao().replaceAll(" OR ", " "));
		}
		
		Interpretador interpretador = new Interpretador();
		int numeroDeOcorrencias = interpretador.recuperaOcorrenciaDeOperadores(requisitoTeste);
		int copiaTempNumeroDeOcorrencias = numeroDeOcorrencias;
		int numeroDeParenteses= 0;
		String expressaoNaturalDoRequisito = requisitoTeste.getDescricao().toLowerCase();
		String expressaoCopiaNaturalDoRequisito = expressaoNaturalDoRequisito;
		String expressaoRelax = "", operadorTemp = "", prefixoOrSufixoTemp = "";
		while (numeroDeOcorrencias > 0) {
			requisitoTeste.setDescricao(
					expressaoNaturalDoRequisito);
			operadorTemp = interpretador.recuperaDeterminadoOperador(1, requisitoTeste);
			prefixoOrSufixoTemp = interpretador.gerarPrefixoOrSufixo(requisitoTeste,
					operadorTemp);
			if (expressaoRelax.equalsIgnoreCase("")) {
				expressaoRelax = operadorTemp + prefixoOrSufixoTemp;
			} else {
				expressaoRelax = expressaoRelax + " (" + operadorTemp
						+ prefixoOrSufixoTemp;
			}
			expressaoNaturalDoRequisito = expressaoNaturalDoRequisito
					.replaceAll(" " + operadorTemp.toLowerCase() + " ", "");
			expressaoNaturalDoRequisito = expressaoNaturalDoRequisito
					.replaceAll(operadorTemp.toLowerCase() + " ", "");
			numeroDeOcorrencias--;numeroDeParenteses++;
		}
		expressaoRelax=	RefatoradorDeString.fecharParenteses(expressaoRelax, numeroDeParenteses-1);
	
		
		if((expressaoRelax.contains(" UNTIL ")||expressaoRelax.contains(" until ")||expressaoRelax.contains("UNTIL ")|| expressaoRelax.contains("until ")) 
				&& (copiaTempNumeroDeOcorrencias==1)){
			String expressaoRelax2= "";
			String[] expressaoQuebrada = expressaoCopiaNaturalDoRequisito.toLowerCase().split("until");
				if(expressaoQuebrada.length==2){
					Requisito reqTemp = new Requisito();
					reqTemp.setDescricao(expressaoQuebrada[0] + " until ");
					expressaoRelax2 += interpretador.gerarPrefixoOrSufixo(reqTemp, "UNTIL");
					expressaoRelax2=expressaoRelax2.replaceAll(" ", "");
					expressaoRelax2 += " UNTIL";
					reqTemp.setDescricao(expressaoQuebrada[1]+ " until ");
					expressaoRelax2 += interpretador.gerarPrefixoOrSufixo(reqTemp, "UNTIL");
				}
				return expressaoRelax2;
		}
		return expressaoRelax;
	}
}
