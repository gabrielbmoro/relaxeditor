package br.unipampa.lesa.modelo.repositorio;
/**
 * 
 * <b>Propósito:</b> <br>
 *Interface responsável por obrigar os repositórios que a implementão realizar as operações básicas de persistência. <br>
 * <b>Instruções de uso:</b> <br>
 * Essa classe deve ser utilizada apenas por uma classe de tipo Repositorio
 * 
 * @author GabrielBMoro
 * @version 0.4
 * @since 26/04/2015
 */
public interface Repository {
	/**
	 * salvar
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por salvar.
	 * </ul>
	 * 
	 * @param objeto
	 */
	public void salvar(Object objeto);

	/**
	 * recuperar
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por recuperar.
	 * </ul>
	 * 
	 * @param nome
	 */
	public Object recuperar(String nome);
	/**
	 * alterar
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por alterar.
	 * </ul>
	 * 
	 * @param objeto
	 */
	public void alterar(Object objeto);
	/**
	 * deletar
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por deletar.
	 * </ul>
	 * 
	 * @param objeto
	 */
	public void deletar(Object objeto);
}
