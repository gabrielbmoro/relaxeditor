package br.unipampa.lesa.modelo.repositorio;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import br.unipampa.lesa.dao.DAORequisito;
import br.unipampa.lesa.dao.TipoMapaChave;
import br.unipampa.lesa.modelo.Especificacao;
import br.unipampa.lesa.modelo.Requisito;

/**
 * 
 * <b>Propósito:</b> <br>
 * Classe reponsável por gerenciar o esquema de persistência, seu objeto
 * principal é garantir que o modelo físico de arquivos é o mesmo do modelo de
 * arquivos da aplicação <br>
 * <b>Instruções de uso:</b> <br>
 * Essa classe deve ser utilizada em parceria com a classe de tipo
 * {@link Especificacao}.
 * 
 * @author GabrielBMoro
 * @version 0.4
 * @since 26/04/2015
 */
public class GerenciadorDeEsquema {

	public static String PATH = System.getProperty("user.home")+ File.separator;
	public static final String nomeArquivoPai = "Main.xml";
	private DAORequisito daoRequisito;
	private Especificacao especificacao;

	/**
	 * Método construtor vazio
	 */
	public GerenciadorDeEsquema() {
		
	}
	/**
	 * construtor
	 * 
	 * @param especificacao
	 *            de tipo {@link Especificacao}
	 */
	public GerenciadorDeEsquema(Especificacao especificacao) {
		this.especificacao = especificacao;
	}

	/**
 * 
 */
	public void criarProjetoDeEspecificacao() {
		String nomeDoProjeto = especificacao.getNomeDoProjeto();
		if (!new File(PATH + nomeDoProjeto).exists()) {
			new File(PATH + nomeDoProjeto).mkdir();
		}
		File file = new File(PATH + nomeDoProjeto + File.separator + nomeArquivoPai);
		try {
			file.createNewFile();
			this.daoRequisito = new DAORequisito(file);
			this.daoRequisito.inicializarXMLVazio(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * criarArquivosIndexados
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por manter o arquivo principal (Main.xml), garantido
	 * que o mesmo possua a indexação dos demais arquivos <br >
	 * utilizados pela aplicação.
	 * </ul>
	 * 
	 * @throws IOException
	 */
	public void criarArquivosIndexados() throws IOException {
		String nomeDoProjeto = especificacao.getNomeDoProjeto();
		File fileMain = new File(PATH + nomeDoProjeto + File.separator + nomeArquivoPai);
		this.daoRequisito = new DAORequisito(fileMain);
		ArrayList<String> enderecos = new ArrayList<String>();
		if (fileMain.exists()) {
			for (Requisito reqTemp : especificacao.getRequisitos()) {
				String enderecoTemp = PATH + nomeDoProjeto + File.separator
						+ reqTemp.getId() + "-" + reqTemp.getNome() + ".xml";
				File file2 = new File(enderecoTemp);
				file2.createNewFile();
				enderecos.add(reqTemp.getId() + "-" + reqTemp.getNome()
						+ ".xml");
				this.daoRequisito.montarXML(file2, reqTemp);
			}
			this.daoRequisito.montarXML(enderecos);
		} else {
			criarProjetoDeEspecificacao();
		}
	}

	/**
	 * 
	 * recuperarProjeto
	 * 
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por criar restaurar em forma de mapa os dados contidos
	 * em um arquivo requisito, esse mapa é o mapeamento de todos os dados
	 * contidos nas tags de requisitos.
	 * </ul>
	 * 
	 * @param nomeDeProjeto
	 *            de tipo {@link String}
	 * @return
	 */
	public HashMap<Integer, Object> recuperarProjeto(String nomeDeProjeto) {
		HashMap<Integer, Object> projetoDeRequisitos = new HashMap<>();
		int key = 0;
		projetoDeRequisitos.put(key, nomeDeProjeto);
		key++;
		File file = new File(PATH + File.separator + nomeDeProjeto + File.separator
				+ nomeArquivoPai);
		if (file.exists()) {
			this.daoRequisito = new DAORequisito(file);
			ArrayList<String> enderecosArquivosRequisitos = this.daoRequisito
					.demonstarXML();
			if (enderecosArquivosRequisitos != null) {
				for (String enderecoTemp : enderecosArquivosRequisitos) {
					HashMap<TipoMapaChave, Object> reqTemp = this.daoRequisito
							.demonstarXML(new File(PATH + File.separator + nomeDeProjeto
									+ File.separator + enderecoTemp));
					projetoDeRequisitos.put(key, reqTemp);
					key++;
				}
			}
			return projetoDeRequisitos;
		} else {
			return projetoDeRequisitos;
		}
	}

	/**
	 * deletarProjeto
	 * 
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por deletar fisicamente o projeto, através da remoção do arquivo main (indexador dos demais).
	 * </ul>
	 * @param nomeDeProjeto de tipo {@link String}
	 */
	public void deletarProjeto(String nomeDeProjeto) {
		File fileTemp = new File(PATH + nomeDeProjeto + File.separator + nomeArquivoPai);
		if (fileTemp.exists()) {
			fileTemp.delete();
		}

	}

}
