package br.unipampa.lesa.modelo.repositorio;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import br.unipampa.lesa.controle.ControleObservadorGeral;
import br.unipampa.lesa.dao.TipoMapaChave;
import br.unipampa.lesa.modelo.Especificacao;
import br.unipampa.lesa.modelo.FatorDeIncerteza;
import br.unipampa.lesa.modelo.FatorRelacao;
import br.unipampa.lesa.modelo.Requisito;
import br.unipampa.lesa.modelo.TipoDeIncerteza;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.TipoDeLog;

/**
 * 
 * <b>Propósito:</b> <br>
 * Repositório responsável por gerenciar as especificações mantidas pela
 * aplicação. <br>
 * <b>Instruções de uso:</b> <br>
 * Essa classe deve ser utilizada em parceria com a classe de tipo
 * {@link Especificacao} e {@link ControleObservadorGeral}.
 * 
 * @author GabrielBMoro
 * @version 0.4
 * @since 26/04/2015
 */
public final class RepositorioDeEspecificacao extends Observable implements
		Repository {

	private static RepositorioDeEspecificacao myInstance;
	private ArrayList<Especificacao> especificacaoList;

	/**
	 * construtor em singleton
	 */
	private RepositorioDeEspecificacao() {
		this.especificacaoList = new ArrayList<>();
	}

	/**
	 * 
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por retornar uma instancia de tipo {@link RepositorioDeEspecificacao}.
	 * </ul>
	 * 
	 * @return
	 */
	public static RepositorioDeEspecificacao getMyInstance() {
		if (myInstance == null) {
			myInstance = new RepositorioDeEspecificacao();
		}
		return myInstance;
	}

	/**
	 * salvar
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por salvar.
	 * </ul>
	 * 
	 * @param objeto
	 */
	@Override
	public void salvar(Object objeto) {
		if (objeto instanceof Especificacao) {
			Especificacao especificacao = (Especificacao) objeto;
			try {
				GerenciadorDeEsquema geradorDeEsquema = new GerenciadorDeEsquema(
						especificacao);
				geradorDeEsquema.criarProjetoDeEspecificacao();
				geradorDeEsquema.criarArquivosIndexados();
				for (int count = 0; count < especificacaoList.size(); count++) {
					if (especificacao == especificacaoList.get(count)) {
						especificacaoList.set(count, especificacao);
						setChanged();
						notifyObservers(especificacao);
						return;
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * recuperar
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por recuperar.
	 * </ul>
	 * 
	 * @param nome
	 */
	@Override
	public Object recuperar(String nomeDoProjeto) {
		for (Especificacao especificacaoTemp : especificacaoList) {
			if (especificacaoTemp.getNomeDoProjeto().equalsIgnoreCase(
					nomeDoProjeto)) {
				return especificacaoTemp;
			}
		}

		// TODO Auto-generated method stub
		GerenciadorDeEsquema geradorDeEsquema = new GerenciadorDeEsquema();
		HashMap<Integer, Object> mapaDeDados = geradorDeEsquema
				.recuperarProjeto(nomeDoProjeto);
		if (mapaDeDados.isEmpty()) {
			return null;
		} else {
			String nomeDaEspecificacao = mapaDeDados.get(0).toString();
			Especificacao especificacao = new Especificacao(nomeDaEspecificacao);
			for (int count = 1; count < mapaDeDados.size(); count++) {
				if (mapaDeDados.get(count) instanceof HashMap) {
					HashMap<TipoMapaChave, Object> mapaReqTemp = (HashMap<TipoMapaChave, Object>) mapaDeDados
							.get(count);
					Requisito requisitoTemp = new Requisito();
					int idReqTemp = Integer.parseInt(mapaReqTemp.get(
							TipoMapaChave.ID).toString());
					requisitoTemp.setId(idReqTemp);
					String nome = mapaReqTemp.get(TipoMapaChave.NOME)
							.toString();
					requisitoTemp.setNome(nome);
					String descricao = mapaReqTemp.get(TipoMapaChave.DESCRICAO)
							.toString();

					requisitoTemp.setDescricao(descricao);
					ArrayList<String> listaDeDependencias = validarObjeto(mapaReqTemp
							.get(TipoMapaChave.DEPENDENCIA));
					ArrayList<String> nomesParaDependencia = new ArrayList<>();
					if (listaDeDependencias != null
							&& !listaDeDependencias.isEmpty()) {
						for (String depend : listaDeDependencias) {
							String[] dadosQuebrados = depend.split("-");
							if ((dadosQuebrados.length - 1) > 0) {
								String nameDoRequisito = dadosQuebrados[0];
								nomesParaDependencia.add(nameDoRequisito);
							}
						}
						requisitoTemp.setDependecias(nomesParaDependencia);
					}
					ArrayList<String> listaDeAmbiente = validarObjeto(mapaReqTemp
							.get(TipoMapaChave.AMBIENTE));
					if (listaDeAmbiente != null) {
						if (!listaDeAmbiente.isEmpty()) {
							FatorDeIncerteza ambiente = new FatorDeIncerteza(TipoDeIncerteza.AMBIENTE);
							ambiente.setCaracteristicas(listaDeAmbiente);
							requisitoTemp.setFatorAmbiental(ambiente);
						}
					}

					ArrayList<String> listaDeMonitoracao = validarObjeto(mapaReqTemp
							.get(TipoMapaChave.MONITORACAO));
					if (listaDeMonitoracao != null) {
						if (!listaDeMonitoracao.isEmpty()) {
							FatorDeIncerteza monitoracao = new FatorDeIncerteza(TipoDeIncerteza.MONITORACAO);
							monitoracao.setCaracteristicas(listaDeMonitoracao);
							requisitoTemp.setFatorDeMonitoracao(monitoracao);
						}
					}

					ArrayList<String> listaDeRelacao = validarObjeto(mapaReqTemp
							.get(TipoMapaChave.RELACAO));
					if (listaDeRelacao != null) {
						if (!listaDeRelacao.isEmpty()) {
							FatorRelacao relacao = null;
							HashMap<Integer, FatorRelacao> relacaoDeReq = new HashMap<>();
							int chaveRelacao = 0;
							for (int count1 = 0; count1 < listaDeRelacao.size(); count1++) {
								relacao = new FatorRelacao();
								String valorAmbiente = listaDeRelacao
										.get(count1 + 1);
								String valorMonitoracao = listaDeRelacao
										.get(count1 + 2);
								relacao.estabelecerRelacao(valorMonitoracao,
										valorAmbiente);
								relacaoDeReq.put(chaveRelacao, relacao);
								chaveRelacao++;
								relacao = null;
								count1 += 2;
							}
							requisitoTemp.setRelacoes(relacaoDeReq);
						}
					}
					especificacao.adicionarRequisito(requisitoTemp);
				}
			}
			this.especificacaoList.add(especificacao);
			setChanged();
			notifyObservers(especificacao);
			return especificacao;
		}
	}
	/**
	 * alterar
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por alterar.
	 * </ul>
	 * 
	 * @param objeto
	 */
	@Override
	public void alterar(Object objeto) {
		try {
			if (objeto instanceof Especificacao && objeto != null) {
				Especificacao especificacao = (Especificacao) objeto;
				String nomeDaEspecificacao = especificacao.getNomeDoProjeto();
				for (int count = 0; count < especificacaoList.size(); count++) {
					if (especificacaoList.get(count).getNomeDoProjeto()
							.equalsIgnoreCase(nomeDaEspecificacao)) {
						especificacaoList.set(count, especificacao);
						GerenciadorDeEsquema geradorDeEsquema = new GerenciadorDeEsquema(
								especificacao);
						geradorDeEsquema.criarProjetoDeEspecificacao();
						geradorDeEsquema.criarArquivosIndexados();
						setChanged();
						notifyObservers(especificacao);
						return;
					}
				}
			}
		} catch (IOException erro) {
			GerenciadorDeLog.getMyInstance().registrarLog(erro.getMessage(), TipoDeLog.ERROR,
					getClass());
		}
	}

	/**
	 * deletar
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por deletar.
	 * </ul>
	 * 
	 * @param objeto
	 */
	@Override
	public void deletar(Object objeto) {
		if (objeto instanceof Especificacao) {
			Especificacao especificacaoTemp = (Especificacao) objeto;
			for (int count = 0; count < especificacaoList.size(); count++) {
				if (especificacaoTemp.equals(especificacaoList.get(count))) {
					especificacaoList.remove(count);
					setChanged();
					notifyObservers(null);
					return;
				}
			}
		}
	}

	/**
	 * 
	 * validarObjeto
	 * 
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Método responsável por validar objeto identificando se o mesmo é uma lista ou não.
	 * </ul>
	 * 
	 * @param objeto
	 * @return
	 */
	private ArrayList<String> validarObjeto(Object objeto) {
		if (objeto != null && objeto instanceof ArrayList) {
			ArrayList<String> lista = (ArrayList<String>) objeto;
			return lista;
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Especificacao> getEspecificacaoList() {
		return especificacaoList;
	}

	/**
	 * 
	 * @param especificacao
	 */
	public void setEspecificacaoList(ArrayList<Especificacao> especificacao) {
		this.especificacaoList = especificacao;
	}

}
