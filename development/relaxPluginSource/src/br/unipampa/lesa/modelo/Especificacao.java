package br.unipampa.lesa.modelo;

import java.io.File;
import java.util.LinkedList;

import br.unipampa.lesa.dao.DAORequisito;
import br.unipampa.lesa.modelo.repositorio.GerenciadorDeEsquema;
import br.unipampa.lesa.modelo.repositorio.RepositorioDeEspecificacao;

public class Especificacao {
	
	private String nomeDoProjeto;
	private LinkedList<Requisito> requisitos;
	private GerenciadorDeEsquema geradorDeEsquema;
	private DAORequisito daoRequisito;
	
	public Especificacao(String nomeDoProjeto){
		this.nomeDoProjeto= nomeDoProjeto;
		this.requisitos = new LinkedList<>();
	}
	
	public void criarEspecificacao(){
		RepositorioDeEspecificacao.getMyInstance().salvar(this);
		this.geradorDeEsquema = new GerenciadorDeEsquema(this);
		this.geradorDeEsquema.criarProjetoDeEspecificacao();
	}
	
	public void criarEspecificacao(String path){
		RepositorioDeEspecificacao.getMyInstance().salvar(this);
		this.geradorDeEsquema = new GerenciadorDeEsquema(this);
		this.geradorDeEsquema.PATH = path + File.separator;
		this.geradorDeEsquema.criarProjetoDeEspecificacao();
	}
	
	
	public void adicionarRequisito(Requisito requisito){
		
		for(int count=0;count<requisitos.size();count++){
			if(requisitos.get(count).getId()==requisito.getId()){
		this.requisitos.set(count, requisito);
		RepositorioDeEspecificacao.getMyInstance().alterar(this);
		return;
			}
		}
			this.requisitos.add(requisito);
			RepositorioDeEspecificacao.getMyInstance().alterar(this);
		}
	
	public void removerRequisito(int idRequisito){
		for(int count=0;count<this.requisitos.size();count++){
			if(idRequisito == this.requisitos.get(count).getId()){
				this.daoRequisito = new DAORequisito();
				this.daoRequisito.deletarRequisito(String.valueOf(idRequisito), this.requisitos.get(count).getNome(), this.nomeDoProjeto);
				this.requisitos.remove(count);
				break;
			}else{
				continue;
			}
		}
		RepositorioDeEspecificacao.getMyInstance().alterar(this);
	}
	
	public Requisito recuperarRequisito(int idRequisito){
		for(int count=0;count<this.requisitos.size();count++){
			if(idRequisito == this.requisitos.get(count).getId()){
				return this.requisitos.get(count);
			}else{
				continue;
			}
		}
		return null;
	}
	
	public void deletarDados(){
		RepositorioDeEspecificacao.getMyInstance().deletar(this);
		geradorDeEsquema = new GerenciadorDeEsquema();
		geradorDeEsquema.deletarProjeto(this.nomeDoProjeto);
		
	}
	
	public LinkedList<Requisito> getRequisitos() {
		return requisitos;
	}

	public void setRequisitos(LinkedList<Requisito> requisitos) {
		this.requisitos = requisitos;
	}

	public String getNomeDoProjeto() {
		return nomeDoProjeto;
	}

	public void setNomeDoProjeto(String nomeDoProjeto) {
		this.nomeDoProjeto = nomeDoProjeto;
	}
	
}
