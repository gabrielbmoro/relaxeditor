package br.unipampa.lesa.modelo;

import java.io.File;
import java.io.FileReader;
import br.unipampa.lesa.servico.EscritorDeArquivoTexto;
import br.unipampa.lesa.servico.lexer.parser;
import br.unipampa.lesa.servico.lexer.ScannerDeAnalizadorSintatico;

public class AnalisadorSintaticoDeExpressao {
	
	public boolean parse(String expressao){
		try {
			EscritorDeArquivoTexto escreverTextoDeExpressao = new EscritorDeArquivoTexto();
			escreverTextoDeExpressao.escreverArquivo(expressao);
			
            parser parse = new parser(new ScannerDeAnalizadorSintatico(
            		new FileReader(new File(EscritorDeArquivoTexto.NOME_DO_ARQUIVO))));
             parse.parse();
             return true;
        } catch (Error ex) {
            return false;
        } catch(Exception erroException){
        	return false;
        }
	}
}
