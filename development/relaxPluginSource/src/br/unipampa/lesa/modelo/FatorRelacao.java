package br.unipampa.lesa.modelo;


/**
 * Relacao.java
 * 
 * <ul>
 * <li><b>Propósito:</b></li>
 * Representa o fator relação, seu propósito é realizar todas atividades que
 * envolvem relacionamento de características de monitoração em requisitos.
 * <li><b>Instruções de uso:</b></li>
 * Deve ser utilizado como objeto agregado de um requisito.
 * </ul>
 * 
 * @author gabrielbmoro
 * @version 1.0
 * @since 19/11/2014
 */
public class FatorRelacao {

	private String caracteristaAmbiental;
	private String caracteristicaMonitoracao;

	/**
	 * estabelecerRelacao
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Registrar duas características distintas do requisito.
	 * </ul>
	 * 
	 * @param caracteristicaMonitoracao
	 *            é uma característica provida por {@link String}
	 * @param caracteristicaAmbiental
	 *            é uma característica provida por {@link String}
	 */
	public void estabelecerRelacao(String caracteristicaMonitoracao,
			String caracteristicaAmbiental) {
		this.caracteristicaMonitoracao = caracteristicaMonitoracao;
		this.caracteristaAmbiental = caracteristicaAmbiental;
	}

	public String getCaracteristicaMonitoracao() {
		return caracteristicaMonitoracao;
	}

	public void setCaracteristicaMonitoracao(String caracteristicaMonitoracao) {
		this.caracteristicaMonitoracao = caracteristicaMonitoracao;
	}

	public String getCaracteristicaAmbiental() {
		return caracteristaAmbiental;
	}

	public void setCaracteristicaAmbiental(String caracteristicaAmbiental) {
		this.caracteristaAmbiental = caracteristicaAmbiental;
	}
	
	
}