package br.unipampa.lesa.modelo;

import java.util.ArrayList;

/**
 * DicionarioDeDados.java
 * 
 * <ul>
 * <li><b>Propósito:</b></li>
 * Representa o DicionarioDeDados, o objetivo dessa classe é abstrair o
 * dicionário da linguagem relax, o qual possui seus termos definidos.
 * 
 * <li><b>Instruções de uso:</b></li>
 * Deve ser utilizado apenas pelo {@link Interpretador}, como base de busca em
 * termos especificos da linguagem.
 * </ul>
 * 
 * @author gabrielbmoro
 * @version 1.0
 * @since 19/11/2014
 */
public class DicionarioDeDados {

	private ArrayList<String> termos;
	/**
	 * construtor
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * O método construtor deve inicializar a lista de características da classe.
	 * </ul>
	 */
	public DicionarioDeDados() {
		carregarTermos();
	}
	/**
	 * carregarTermos
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * Carregar todos os termos, palavras-chave da linguagem Relax.
	 * </ul>
	 */
	private void carregarTermos() {
		this.termos = new ArrayList<>();
		this.termos.add("SHALL");
		this.termos.add("MAY");
		this.termos.add("OR");
		this.termos.add("EVENTUALLY");
		this.termos.add("UNTIL");
		this.termos.add("BEFORE");
		this.termos.add("AFTER");
		this.termos.add("IN");
		this.termos.add("AS CLOSE AS POSSIBLE TO");
		this.termos.add("AS EARLY AS POSSIBLE");
		this.termos.add("AS LATE AS POSSIBLE");
		this.termos.add("AS MANY AS POSSIBLE");
		this.termos.add("AS FEW AS POSSIBLE");
	}
	
	public ArrayList<String> getTermos() {
		return termos;
	}
	public void setTermos(ArrayList<String> termos) {
		this.termos = termos;
	}

	
	
}