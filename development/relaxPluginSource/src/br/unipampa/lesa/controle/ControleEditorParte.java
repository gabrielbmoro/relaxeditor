package br.unipampa.lesa.controle;

import java.util.ArrayList;
import java.util.HashMap;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableItem;
import br.unipampa.lesa.apresentacao.EditorParte;
import br.unipampa.lesa.apresentacao.GeradorDeMensagem;
import br.unipampa.lesa.apresentacao.RelaxParte;
import br.unipampa.lesa.apresentacao.ProjectParte;
import br.unipampa.lesa.modelo.AnalisadorLexicoDeRequisito;
import br.unipampa.lesa.modelo.DicionarioDeDados;
import br.unipampa.lesa.modelo.Especificacao;
import br.unipampa.lesa.modelo.FatorDeIncerteza;
import br.unipampa.lesa.modelo.GeradorDeExpressao;
import br.unipampa.lesa.modelo.Interpretador;
import br.unipampa.lesa.modelo.FatorRelacao;
import br.unipampa.lesa.modelo.Requisito;
import br.unipampa.lesa.modelo.TipoDeIncerteza;
import br.unipampa.lesa.modelo.repositorio.RepositorioDeEspecificacao;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.RefatoradorDeString;
import br.unipampa.lesa.servico.TipoDeLog;

public class ControleEditorParte implements Controller {
	
	private Requisito requisito;
	private DicionarioDeDados dicionarioDeDados;
	private Interpretador interpretador;
	private GeradorDeExpressao geradorDeExpressao;
	private AnalisadorLexicoDeRequisito analizadorLexico;
	private EditorParte editorParte;
	private RelaxParte relaxParte;
	private ProjectParte projectParte;

	public ControleEditorParte(){
//		this.editorParte = editorParte;
		inicializarComponentes();
	}
	@Override
	public void registrarListeners() {
		// TODO Auto-generated method stub
this.editorParte.getTxtRequisito().addListener(SWT.MouseUp, new Listener() {
	@Override
	public void handleEvent(Event arg0) {
		// TODO Auto-generated method stub
		editorParte.verificarTodasPalavrasNaoChave(editorParte.getTxtRequisito().getText(), dicionarioDeDados);
		editorParte.verificarTodasPalavrasChave(editorParte.getTxtRequisito().getText(), dicionarioDeDados);
	}
});

this.editorParte.getTxtRequisito().addKeyListener(new KeyListener() {
	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		if (arg0.keyCode == java.awt.event.KeyEvent.VK_SPACE) {
			editorParte.verificarTodasPalavrasChave(editorParte.getTxtRequisito()
					.getText(),dicionarioDeDados);
		} else if (arg0.keyCode == java.awt.event.KeyEvent.VK_BACK_SPACE) {
			editorParte.verificarUltimaPalavraNaoChave(editorParte.getTxtRequisito()
					.getText(),dicionarioDeDados);
			editorParte.verificarTodasPalavrasChave(editorParte.getTxtRequisito()
					.getText(),dicionarioDeDados);
		}
	}
});

this.editorParte.getAddRelacionamento().addListener(SWT.MouseUp,
		new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				try {
					int indice1 = 0, indice2 = 0;

					indice1 = editorParte.getComboAmb()
							.getSelectionIndex();
					indice2 = editorParte.getComboMonit()
							.getSelectionIndex();

					String caracteristicaAmbiental = editorParte
							.getComboAmb().getItem(indice1);
//					pluginParte.getComboAmb().remove(indice1);
					String caracteristicaMonitoracao = editorParte
							.getComboMonit().getItem(indice2);
//					pluginParte.getComboMonit().remove(indice2);

					TableItem tableItem = new TableItem(editorParte
							.getTableRelacionamento(), SWT.NONE);
					tableItem.setText(new String[] {
							caracteristicaAmbiental,
							caracteristicaMonitoracao });
				} catch (Exception erro) {
					GeradorDeMensagem.exibirMensagemDeInformacao(
							"Please, select a valid value!",
							"Alert of User");
				}
			}
		});
this.editorParte.getRemoveRelacionamento().addListener(SWT.MouseUp,
		new Listener() {
			@Override
			public void handleEvent(Event arg0) {

				int indiceARemover = editorParte
						.getTableRelacionamento().getSelectionIndex();

				try {
					TableItem tableItem = editorParte
							.getTableRelacionamento().getItem(
									indiceARemover);
					if (tableItem != null
							&& !tableItem.getText(0).isEmpty()
							&& !tableItem.getText(1).isEmpty()) {
						editorParte.getComboAmb().add(
								tableItem.getText(0));
						editorParte.getComboMonit().add(
								tableItem.getText(1));

						editorParte.getTableRelacionamento().remove(
								indiceARemover);
					}
				} catch (IllegalArgumentException erro) {
					GeradorDeMensagem
							.exibirMensagemDeInformacao(
									"Please, Table removing items can only be performed when the line is selected.",
									"Alert of User");
				}
			}
		});
this.editorParte.getBtnAddMonit().addListener(SWT.MouseUp,
		new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				String monitoracaoText = editorParte
						.getTxtMonitoracao().getText().toString();
				editorParte.getTxtMonitoracao().setText("");
				monitoracaoText = RefatoradorDeString
						.substituirCaracteresBrPorEng(monitoracaoText);
				GerenciadorDeLog.getMyInstance().registrarLog(
						"Removendo caracteres especiais e acento",
						TipoDeLog.INFO, getClass());
				editorParte.getTxtMonitoracao()
						.setText(monitoracaoText);
				if (!monitoracaoText.isEmpty()) {
					editorParte.getListMonitoracao().add(
							monitoracaoText);
					editorParte.getComboMonit().add(monitoracaoText);
					editorParte.getTxtMonitoracao().setText("");
				}
			}
		});
this.editorParte.getBtnRemoveMOnit().addListener(SWT.MouseUp,
		new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				int itemSelecionado = editorParte.getListMonitoracao()
						.getSelectionIndex();
				String item = editorParte.getListMonitoracao().getItem(
						itemSelecionado);
				requisito.getFatorDeMonitoracao().removerItem(
						itemSelecionado);
				editorParte.getListMonitoracao()
						.remove(itemSelecionado);
				editorParte.getComboMonit().remove(itemSelecionado);
			}
		});
this.editorParte.getBtnAddAmbiente().addListener(SWT.MouseUp,
		new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				String ambienteTxt = editorParte.getTxtAmbiente()
						.getText().toString();
				editorParte.getTxtAmbiente().setText("");
				ambienteTxt = RefatoradorDeString
						.substituirCaracteresBrPorEng(ambienteTxt);
				GerenciadorDeLog.getMyInstance().registrarLog(
						"Removendo caracteres especiais e acento",
						TipoDeLog.INFO, getClass());
				editorParte.getTxtAmbiente().setText(ambienteTxt);

				if (!ambienteTxt.isEmpty()) {
					editorParte.getListAmbiente().add(ambienteTxt);
					editorParte.getComboAmb().add(ambienteTxt);
					editorParte.getTxtAmbiente().setText("");
				}
			}
		});
this.editorParte.getBtnRemoveAmbiente().addListener(SWT.MouseUp,
		new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				int itemSelecionado = editorParte.getListAmbiente()
						.getSelectionIndex();
				String item = editorParte.getListAmbiente().getItem(
						itemSelecionado);
				requisito.getFatorAmbiental().removerItem(
						itemSelecionado);
				editorParte.getListAmbiente().remove(itemSelecionado);
				editorParte.getComboAmb().remove(itemSelecionado);
			}
		});

this.editorParte.getBtnSalvarRequisito().addListener(SWT.MouseUp,
		new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				reproduzirEventoSalvar();
				editorParte.desabilitarTodosOsComponentes();
			}
		});

		this.editorParte.getBtnAddDependence()
				.addListener(SWT.MouseUp, new Listener() {
					@Override
					public void handleEvent(Event arg0) {
							int indexSelecionado = editorParte.getComboRequisitos().getSelectionIndex();
							if(indexSelecionado>=0){
							String nomeRequisito = editorParte.getComboRequisitos().getItem(indexSelecionado);
							try {
								String nomeDoRequisitoCorrente = editorParte
										.getTxtNome().getText();
								if (nomeRequisito != null
										&& !nomeRequisito
												.equals(nomeDoRequisitoCorrente)) {
									for (String idSTemp : editorParte
											.getListDependencia().getItems()) {
										if (idSTemp.equals(nomeRequisito)) {
											GeradorDeMensagem
													.exibirMensagemDeInformacao(
															"There is already a dependency of this requirement!",
															"Alert to the User");
											return;
										}
									}
									editorParte.getListDependencia().add(
											String.valueOf(nomeRequisito));
									GeradorDeMensagem
											.exibirMensagemDeInformacao(
													"Added dependency successfully!",
													"Alert to the User");
								} else {
									GeradorDeMensagem
											.exibirMensagemDeInformacao(
													"You can not add this depedency!",
													"Alert to the User");
								}
							} catch (Exception erro) {
								GeradorDeMensagem
										.exibirMensagemDeInformacao(
												"You can not add this depedency!",
												"Alert to the User");
							}
							}
					}
	
				});
		this.editorParte.getBtnRemoveDependence().addListener(SWT.MouseUp,
				new Listener() {
			@Override
			public void handleEvent(Event arg0) {
						int indexSelecionado = editorParte.getListDependencia().getSelectionIndex();
						
							String nomeRequisito = editorParte.getListDependencia().getItem(indexSelecionado);
							try {
								String nomeRequisitoCorrente = editorParte
										.getTxtNome().getText();
								if (nomeRequisito != null
										&& !nomeRequisito
												.equals(nomeRequisitoCorrente)) {
	
									for (int count = 0; count < editorParte
											.getListDependencia()
											.getItemCount(); count++) {
										if (editorParte.getListDependencia()
												.getItem(count)
												.equals(nomeRequisito)) {
											editorParte.getListDependencia()
													.remove(count);
											GeradorDeMensagem
													.exibirMensagemDeInformacao(
															"Successfully removed dependency!",
															"Alert to the User");
											return;
										}
									}
								} else {
									GeradorDeMensagem
											.exibirMensagemDeInformacao(
													"You can not remove this dependency!",
													"Alert to the User");
								}
							} catch (Exception erro) {
								GeradorDeMensagem
										.exibirMensagemDeInformacao(
												"You can not remove this dependency!",
												"Alert to the User");
							}
			}
						
					
				});
	}
	
	private String recuperarNomeDoRequisitoSelecionado(
			TreeItem[] treeSelecionado) {
		GerenciadorDeLog.getMyInstance()
				.registrarLog(
						"Recuperar ID da execução do requisito contatado pelo menu árvore",
						TipoDeLog.INFO, getClass());
		
		if (treeSelecionado != null 
				&& treeSelecionado.length == 1 
				&& RefatoradorDeString.verificarExistenciaDeItemRequisito(treeSelecionado[0].getText())) {
			String nomeItemDeArvore = treeSelecionado[0].getText();
			if (nomeItemDeArvore != null) {
				return nomeItemDeArvore;
			}
		}
		return null;
	}
	@Override
	public void inicializarComponentes() {
		this.dicionarioDeDados = new DicionarioDeDados();		
//		registrarListeners();
	}
	@Override
	public void inicializarComponentes(Object objeto) {
		// TODO Auto-generated method stub
		
	}
	
	public void reproduzirEventoSalvar(){
		if (!editorParte.existeCamposVazios()) {
			if (projectParte.getTreeProjetos().getItems()[0] != null) {
				String titulo = projectParte.getTreeProjetos()
						.getItems()[0].getText();
				Especificacao especificacao = (Especificacao) RepositorioDeEspecificacao
						.getMyInstance().recuperar(titulo);

				if (especificacao != null) {

					Requisito requisitoTemporario = new Requisito();

					int idRequisito = Integer
							.parseInt(editorParte.getTxtId()
									.getText().toString());
					requisitoTemporario.setId(idRequisito);
					String nome = editorParte.getTxtNome()
							.getText();
					nome = RefatoradorDeString
							.substituirCaracteresBrPorEng(nome);
					requisitoTemporario.setNome(nome);
					String expressaoText = editorParte
							.getTxtRequisito().getText();
					expressaoText = RefatoradorDeString
							.substituirCaracteresBrPorEng(expressaoText);
					requisitoTemporario
							.setDescricao(
									expressaoText);

					String[] listaDeCaracteristicasAmiente = editorParte
							.getListAmbiente().getItems();
					ArrayList<String> arrayListaDeCaracteristicasAmbiente = new ArrayList<>();
					for (String caracteristicaAmbiente : listaDeCaracteristicasAmiente) {
						arrayListaDeCaracteristicasAmbiente
								.add(caracteristicaAmbiente);
					}
					FatorDeIncerteza ambiente = new FatorDeIncerteza(TipoDeIncerteza.AMBIENTE);
					ambiente.setCaracteristicas(arrayListaDeCaracteristicasAmbiente);
					requisitoTemporario
							.setFatorAmbiental(ambiente);

					String[] listaDeCaracteristicasMonitoracao = editorParte
							.getListMonitoracao().getItems();
					ArrayList<String> arrayListaDeCaracteristicasMonitoracao = new ArrayList<>();
					for (String caracteristicaMonitoracao : listaDeCaracteristicasMonitoracao) {
						arrayListaDeCaracteristicasMonitoracao
								.add(caracteristicaMonitoracao);
					}
					FatorDeIncerteza monitoracao = new FatorDeIncerteza(TipoDeIncerteza.MONITORACAO);
					monitoracao
							.setCaracteristicas(arrayListaDeCaracteristicasMonitoracao);
					requisitoTemporario
							.setFatorDeMonitoracao(monitoracao);

					ArrayList<String> dadosDeDependencia = new ArrayList<>();
					String[] listaDeDependencia = editorParte
							.getListDependencia().getItems();
					for (String dadoDependencia : listaDeDependencia) {
						// int idRequisitoDependente = Integer
						// .parseInt(dadoDependencia);
						dadosDeDependencia.add(dadoDependencia);
					}
					requisitoTemporario
							.setDependecias(dadosDeDependencia);

					TableItem[] itensDeTabela = editorParte
							.getTableRelacionamento()
							.getItems();
					HashMap<Integer, FatorRelacao> mapaDeRelacao = new HashMap<>();
					int key = 0;
					for (TableItem tableItemTemp : itensDeTabela) {
						FatorRelacao relacao = new FatorRelacao();
						relacao.estabelecerRelacao(
								tableItemTemp.getText(1),
								tableItemTemp.getText(0));
						mapaDeRelacao.put(key, relacao);
						key++;
					}
					requisitoTemporario
							.setRelacoes(mapaDeRelacao);

					especificacao
							.adicionarRequisito(requisitoTemporario);

					GeradorDeMensagem
							.exibirMensagemDeInformacao(
									"Requirement registered successfully!",
									"Alert of User");
					// RepositorioDeEspecificacao.getMyInstance()
					// .alterar(especificacao);
					editorParte.limparDadosDoEditor();
				} else {
					GeradorDeMensagem
							.exibirMensagemDeInformacao(
									"Could not perform this operation , do it!",
									"Alert of User");
				}
			} else {
				GeradorDeMensagem
						.exibirMensagemDeInformacao(
								"Unable to save the requirement , as there are blank fields!",
								"Alert of User");
			}
		} else {
			GeradorDeMensagem
					.exibirMensagemDeInformacao(
							"Unable to save the requirement, as there are blank fields!",
							"Alert of User");
		}
		editorParte.getTxtId().setText("<idReq>");
	}
	public Requisito getRequisito() {
		return requisito;
	}
	public void setRequisito(Requisito requisito) {
		this.requisito = requisito;
	}
	public DicionarioDeDados getDicionarioDeDados() {
		return dicionarioDeDados;
	}
	public void setDicionarioDeDados(DicionarioDeDados dicionarioDeDados) {
		this.dicionarioDeDados = dicionarioDeDados;
	}
	public Interpretador getInterpretador() {
		return interpretador;
	}
	public void setInterpretador(Interpretador interpretador) {
		this.interpretador = interpretador;
	}
	public GeradorDeExpressao getGeradorDeExpressao() {
		return geradorDeExpressao;
	}
	public void setGeradorDeExpressao(GeradorDeExpressao geradorDeExpressao) {
		this.geradorDeExpressao = geradorDeExpressao;
	}
	public AnalisadorLexicoDeRequisito getAnalizadorLexico() {
		return analizadorLexico;
	}
	public void setAnalizadorLexico(AnalisadorLexicoDeRequisito analizadorLexico) {
		this.analizadorLexico = analizadorLexico;
	}
	public EditorParte getEditorParte() {
		return editorParte;
	}
	public void setEditorParte(EditorParte editorParte) {
		this.editorParte = editorParte;
	}
	public RelaxParte getRelaxParte() {
		return relaxParte;
	}
	public void setRelaxParte(RelaxParte relaxParte) {
		this.relaxParte = relaxParte;
	}
	public ProjectParte getProjectParte() {
		return projectParte;
	}
	public void setProjectParte(ProjectParte projectParte) {
		this.projectParte = projectParte;
	}
	
}
