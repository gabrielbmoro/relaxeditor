package br.unipampa.lesa.controle;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.TreeItem;

import br.unipampa.lesa.apresentacao.EditorParte;
import br.unipampa.lesa.apresentacao.GeradorDeMensagem;
import br.unipampa.lesa.apresentacao.ProjectParte;
import br.unipampa.lesa.modelo.DicionarioDeDados;
import br.unipampa.lesa.modelo.Especificacao;
import br.unipampa.lesa.modelo.FatorRelacao;
import br.unipampa.lesa.modelo.Requisito;
import br.unipampa.lesa.modelo.repositorio.RepositorioDeEspecificacao;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.TipoDeLog;

public class ControleProjectParte implements Controller {

	private ProjectParte projectParte;
	private EditorParte editorParte;
	private ControleEditorParte controleEditor;
	private ControleObservadorGeral controleObservador;
	private DicionarioDeDados dicionarioDeDados;
	
	public ControleProjectParte() {
		GerenciadorDeLog.getMyInstance().registrarLog(
				"Iniciando a execução do plugin", TipoDeLog.INFO, getClass());
//		inicializarComponentes();
		dicionarioDeDados = new DicionarioDeDados();
	}

	@Override
	public void registrarListeners() {
		GerenciadorDeLog.getMyInstance().registrarLog(
				"Registrando listeners do PluginPart", TipoDeLog.INFO,
				getClass());

		this.projectParte.getMenuItemNovo().addSelectionListener(
				new SelectionListener() {
					@Override
					public void widgetSelected(SelectionEvent arg0) {
						// TODO Auto-generated method stub
						editorParte.limparDadosDoEditor();
						editorParte.habilitarTodosOsComponentes();
						try {
							String titulo = projectParte.getTreeProjetos()
									.getItems()[0].getText();

							Especificacao especificacao = (Especificacao) RepositorioDeEspecificacao
									.getMyInstance().recuperar(titulo);

							if (especificacao != null) {
								int id = especificacao.getRequisitos().size() + 1;
								if (id != 0) {
									editorParte.getTxtId().setText(
											String.valueOf(id));
									GeradorDeMensagem
											.exibirMensagemDeInformacao(
													"You can write Requirement That will use the id presented!",
													"Alert to the User");
								}
								editorParte.habilitarTodosOsComponentes();
							}
						} catch (Exception erro) {
							GeradorDeMensagem
									.exibirMensagemDeErro("Please, this is not a requirement.");
							editorParte.desabilitarTodosOsComponentes();
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent arg0) {
						// TODO Auto-generated method stub

					}
				});
		this.projectParte.getBtnNewRequirement().addListener(SWT.MouseUp,
				new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				// TODO Auto-generated method stub
				editorParte.limparDadosDoEditor();
				editorParte.habilitarTodosOsComponentes();
				try {
					String titulo = projectParte.getTreeProjetos()
							.getItems()[0].getText();

					Especificacao especificacao = (Especificacao) RepositorioDeEspecificacao
							.getMyInstance().recuperar(titulo);

					if (especificacao != null) {
						int id = especificacao.getRequisitos().size() + 1;
						if (id != 0) {
							editorParte.getTxtId().setText(
									String.valueOf(id));
							GeradorDeMensagem
									.exibirMensagemDeInformacao(
											"You can write Requirement That will use the id presented!",
											"Alert to the User");
						}
						editorParte.habilitarTodosOsComponentes();
					}
				} catch (Exception erro) {
					GeradorDeMensagem
							.exibirMensagemDeErro("Please, this is not a requirement.");
					editorParte.desabilitarTodosOsComponentes();
				}				
			}
		});
		this.projectParte.getMenuItemEditar().addSelectionListener(
				new SelectionListener() {
					@Override
					public void widgetSelected(SelectionEvent arg0) {
						editorParte.limparDadosDoEditor();
						editorParte.habilitarTodosOsComponentes();
						editorParte.getTxtRequisito().setFocus();
						GerenciadorDeLog
								.getMyInstance()
								.registrarLog(
										"Limpando dados da interface gráfica do editor de requisito",
										TipoDeLog.INFO, getClass());

						try {

							TreeItem[] treeSelecionado = projectParte
									.getTreeProjetos().getSelection();
							if (treeSelecionado != null
									&& treeSelecionado.length > 0) {
								int idRequisito = recuperarIdRequisitoSelecionado(treeSelecionado);
								if (idRequisito != 0) {
									editorParte.getTxtId().setText(
											String.valueOf(idRequisito));
									ArrayList<Especificacao> especificacoes = RepositorioDeEspecificacao
											.getMyInstance()
											.getEspecificacaoList();
									if (!especificacoes.isEmpty()) {
										for (Especificacao especificacaoTemp : especificacoes) {
											Requisito requisitoTemp = especificacaoTemp
													.recuperarRequisito(idRequisito);
											editorParte.getTxtNome().setText(
													requisitoTemp.getNome());
											editorParte
													.getTxtRequisito()
													.setText(
															requisitoTemp
																	.getDescricao());
											ArrayList<String> caracteristicasAmbientais = requisitoTemp
													.getFatorAmbiental()
													.getCaracteristicas();
											if (caracteristicasAmbientais != null
													&& !caracteristicasAmbientais
															.isEmpty()) {
												for (String caracteristaAmbiental : caracteristicasAmbientais) {
													editorParte
															.getListAmbiente()
															.add(caracteristaAmbiental);
													editorParte
															.getComboAmb()
															.add(caracteristaAmbiental);
												}
											}
											ArrayList<String> caracteristicasMonitoracao = requisitoTemp
													.getFatorDeMonitoracao()
													.getCaracteristicas();
											if (caracteristicasMonitoracao != null
													&& !caracteristicasMonitoracao
															.isEmpty()) {
												for (String caracteristicaMonitoracao : caracteristicasMonitoracao) {
													editorParte
															.getListMonitoracao()
															.add(caracteristicaMonitoracao);
													editorParte
															.getComboMonit()
															.add(caracteristicaMonitoracao);
												}
											}

											HashMap<Integer, FatorRelacao> dadosDeRelacao = requisitoTemp
													.getRelacoes();

											System.err
													.println("Size of Relacao: "
															+ dadosDeRelacao
																	.size());

											if (dadosDeRelacao != null
													&& !dadosDeRelacao
															.isEmpty()) {
												for (int count = 0; count < dadosDeRelacao
														.size(); count++) {
													if (dadosDeRelacao
															.get(count) == null) {
														continue;
													} else {
														FatorRelacao relacionamentoObjeto = dadosDeRelacao
																.get(count);
														TableItem tableItemX = new TableItem(
																editorParte
																		.getTableRelacionamento(),
																SWT.NONE);
														String caracteristicaAmbiental = relacionamentoObjeto
																.getCaracteristicaAmbiental();
														String caracteristicaMonitoracao = relacionamentoObjeto
																.getCaracteristicaMonitoracao();
														tableItemX
																.setText(new String[] {
																		caracteristicaAmbiental,
																		caracteristicaMonitoracao });
														editorParte.verificarTodasPalavrasChave(editorParte
																.getTxtRequisito()
																.getText(),dicionarioDeDados);

														editorParte
																.validarCombosDeRelacao(
																		caracteristicaAmbiental,
																		caracteristicaMonitoracao);
													}
												}
											}

											if (requisitoTemp.getDependecias() != null
													&& !requisitoTemp
															.getDependecias()
															.isEmpty()) {
												for (String nameReq : requisitoTemp
														.getDependecias()) {
													editorParte
															.getListDependencia()
															.add(nameReq);
												}
											}
											GerenciadorDeLog
													.getMyInstance()
													.registrarLog(
															"Requisito recuperado com sucesso da especificação",
															TipoDeLog.INFO,
															getClass());
										}
									}
								}
							}
						} catch (Exception erro) {
							GeradorDeMensagem
									.exibirMensagemDeErro("Please, this is not a requirement.");
							editorParte.desabilitarTodosOsComponentes();
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent arg0) {
						// TODO Auto-generated method stub

					}
				});
		this.projectParte.getBtnReopen().addListener(SWT.MouseUp, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				editorParte.limparDadosDoEditor();
				editorParte.habilitarTodosOsComponentes();
				editorParte.getTxtRequisito().setFocus();

				GerenciadorDeLog
						.getMyInstance()
						.registrarLog(
								"Limpando dados da interface gráfica do editor de requisito",
								TipoDeLog.INFO, getClass());

				try {

					TreeItem[] treeSelecionado = projectParte
							.getTreeProjetos().getSelection();
					if (treeSelecionado != null
							&& treeSelecionado.length > 0) {
						int idRequisito = recuperarIdRequisitoSelecionado(treeSelecionado);
						if (idRequisito != 0) {
							editorParte.getTxtId().setText(
									String.valueOf(idRequisito));
							ArrayList<Especificacao> especificacoes = RepositorioDeEspecificacao
									.getMyInstance()
									.getEspecificacaoList();
							if (!especificacoes.isEmpty()) {
								for (Especificacao especificacaoTemp : especificacoes) {
									Requisito requisitoTemp = especificacaoTemp
											.recuperarRequisito(idRequisito);
									editorParte.getTxtNome().setText(
											requisitoTemp.getNome());
									editorParte
											.getTxtRequisito()
											.setText(
													requisitoTemp
															.getDescricao());
									ArrayList<String> caracteristicasAmbientais = requisitoTemp
											.getFatorAmbiental()
											.getCaracteristicas();
									if (caracteristicasAmbientais != null
											&& !caracteristicasAmbientais
													.isEmpty()) {
										for (String caracteristaAmbiental : caracteristicasAmbientais) {
											editorParte
													.getListAmbiente()
													.add(caracteristaAmbiental);
											editorParte
													.getComboAmb()
													.add(caracteristaAmbiental);
										}
									}
									ArrayList<String> caracteristicasMonitoracao = requisitoTemp
											.getFatorDeMonitoracao()
											.getCaracteristicas();
									if (caracteristicasMonitoracao != null
											&& !caracteristicasMonitoracao
													.isEmpty()) {
										for (String caracteristicaMonitoracao : caracteristicasMonitoracao) {
											editorParte
													.getListMonitoracao()
													.add(caracteristicaMonitoracao);
											editorParte
													.getComboMonit()
													.add(caracteristicaMonitoracao);
										}
									}

									HashMap<Integer, FatorRelacao> dadosDeRelacao = requisitoTemp
											.getRelacoes();

									System.err
											.println("Size of Relacao: "
													+ dadosDeRelacao
															.size());

									if (dadosDeRelacao != null
											&& !dadosDeRelacao
													.isEmpty()) {
										for (int count = 0; count < dadosDeRelacao
												.size(); count++) {
											if (dadosDeRelacao
													.get(count) == null) {
												continue;
											} else {
												FatorRelacao relacionamentoObjeto = dadosDeRelacao
														.get(count);
												TableItem tableItemX = new TableItem(
														editorParte
																.getTableRelacionamento(),
														SWT.NONE);
												String caracteristicaAmbiental = relacionamentoObjeto
														.getCaracteristicaAmbiental();
												String caracteristicaMonitoracao = relacionamentoObjeto
														.getCaracteristicaMonitoracao();
												tableItemX
														.setText(new String[] {
																caracteristicaAmbiental,
																caracteristicaMonitoracao });
												editorParte.verificarTodasPalavrasChave(editorParte
														.getTxtRequisito()
														.getText(),dicionarioDeDados);

												editorParte
														.validarCombosDeRelacao(
																caracteristicaAmbiental,
																caracteristicaMonitoracao);
											}
										}
									}

									if (requisitoTemp.getDependecias() != null
											&& !requisitoTemp
													.getDependecias()
													.isEmpty()) {
										for (String nameReq : requisitoTemp
												.getDependecias()) {
											editorParte
													.getListDependencia()
													.add(nameReq);
										}
									}
									GerenciadorDeLog
											.getMyInstance()
											.registrarLog(
													"Requisito recuperado com sucesso da especificação",
													TipoDeLog.INFO,
													getClass());
								}
							}
						}
					}
				} catch (Exception erro) {
					GeradorDeMensagem
							.exibirMensagemDeErro("Please, this is not a requirement.");
					editorParte.desabilitarTodosOsComponentes();
				}
							
			}
		});
		this.projectParte.getMenuItemDeletar().addSelectionListener(
				new SelectionListener() {
					@Override
					public void widgetSelected(SelectionEvent arg0) {
						boolean resposta = GeradorDeMensagem
								.exibirMensagemDeConfirmacao(
										"Do you want delete the requirement?",
										"Alert of User");
						if (resposta) {
							TreeItem[] treeSelecionado = projectParte
									.getTreeProjetos().getSelection();
							try {
								if (treeSelecionado != null
										&& treeSelecionado.length > 0) {
									int idRequisito = recuperarIdRequisitoSelecionado(treeSelecionado);
									if (idRequisito != 0) {
										ArrayList<Especificacao> especificacoes = RepositorioDeEspecificacao
												.getMyInstance()
												.getEspecificacaoList();
										if (!especificacoes.isEmpty()) {
											for (Especificacao especificacaoTemp : especificacoes) {
												especificacaoTemp
														.removerRequisito(idRequisito);
												GerenciadorDeLog
														.getMyInstance()
														.registrarLog(
																"Requirement has been successfully removed",
																TipoDeLog.INFO,
																getClass());
												GeradorDeMensagem
														.exibirMensagemDeInformacao(
																"The requirement was deleted sucess!",
																"Alert of User");
											}
										}
									}
								}
							} catch (Exception erro) {
								GeradorDeMensagem
										.exibirMensagemDeErro("Sorry, an error occurred, please perform the operation later, do not forget, this operation is not requirement and specification.");
							}
						}
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent arg0) {
						// TODO Auto-generated method stub

					}
				});
		this.projectParte.getBtnRemove().addListener(SWT.MouseUp, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				boolean resposta = GeradorDeMensagem
						.exibirMensagemDeConfirmacao(
								"Do you want delete the requirement?",
								"Alert of User");
				if (resposta) {
					TreeItem[] treeSelecionado = projectParte
							.getTreeProjetos().getSelection();
					try {
						if (treeSelecionado != null
								&& treeSelecionado.length > 0) {
							int idRequisito = recuperarIdRequisitoSelecionado(treeSelecionado);
							if (idRequisito != 0) {
								ArrayList<Especificacao> especificacoes = RepositorioDeEspecificacao
										.getMyInstance()
										.getEspecificacaoList();
								if (!especificacoes.isEmpty()) {
									for (Especificacao especificacaoTemp : especificacoes) {
										especificacaoTemp
												.removerRequisito(idRequisito);
										GerenciadorDeLog
												.getMyInstance()
												.registrarLog(
														"Requirement has been successfully removed",
														TipoDeLog.INFO,
														getClass());
										GeradorDeMensagem
												.exibirMensagemDeInformacao(
														"The requirement was deleted sucess!",
														"Alert of User");
									}
								}
							}
						}
					} catch (Exception erro) {
						GeradorDeMensagem
								.exibirMensagemDeErro("Sorry, an error occurred, please perform the operation later, do not forget, this operation is not requirement and specification.");
					}
				}
			}
		});
	}

	private int recuperarIdRequisitoSelecionado(TreeItem[] treeSelecionado) {
		GerenciadorDeLog
				.getMyInstance()
				.registrarLog(
						"Recuperar ID da execução do requisito contatado pelo menu árvore",
						TipoDeLog.INFO, getClass());
		if (treeSelecionado != null && treeSelecionado.length == 1) {
			String nomeItemDeArvore = treeSelecionado[0].getText();
			String[] nomeItemQuebrado = nomeItemDeArvore.split("-");
			if (nomeItemQuebrado.length > 0) {
				String idText = nomeItemQuebrado[0].toString();
				int id = Integer.parseInt(idText);
				if (id != 0) {
					return id;
				}
			}
		}
		return 0;
	}

	@Override
	public void inicializarComponentes() {
		/*VERIFICAR*/
//		this.editorParte = editorParte;
//		this.controleEditor = controleEditorParte;
		/*VERIFICAR*/
		GerenciadorDeLog
		.getMyInstance()
		.registrarLog(
				"Inicializando componentes controladores responsáveis pela monitoração do PluginPart",
				TipoDeLog.INFO, getClass());
		registrarListeners();
		
		this.controleObservador = new ControleObservadorGeral(RepositorioDeEspecificacao.getMyInstance(), projectParte, editorParte);
		
		

		// this.controleObservador = new ControleObservadorGeral(
		// RepositorioDeEspecificacao.getMyInstance(), pluginParte);
		//
		// pluginParte.desabilitarTodosOsComponentes();
		registrarListeners();
	}

	@Override
	public void inicializarComponentes(Object objeto) {
		// TODO Auto-generated method stub
		
	}

	public ProjectParte getProjectParte() {
		return projectParte;
	}

	public void setProjectParte(ProjectParte projectParte) {
		this.projectParte = projectParte;
	}

	public EditorParte getEditorParte() {
		return editorParte;
	}

	public void setEditorParte(EditorParte editorParte) {
		this.editorParte = editorParte;
	}

	public ControleEditorParte getControleEditor() {
		return controleEditor;
	}

	public void setControleEditor(ControleEditorParte controleEditor) {
		this.controleEditor = controleEditor;
	}

	public ControleObservadorGeral getControleObservador() {
		return controleObservador;
	}

	public void setControleObservador(ControleObservadorGeral controleObservador) {
		this.controleObservador = controleObservador;
	}

}
