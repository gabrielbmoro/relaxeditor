package br.unipampa.lesa.controle;

import java.io.File;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import br.unipampa.lesa.apresentacao.FrameAssistenteDeCriacao;
import br.unipampa.lesa.apresentacao.GeradorDeMensagem;
import br.unipampa.lesa.apresentacao.NovoHandler;
import br.unipampa.lesa.modelo.Especificacao;
import br.unipampa.lesa.modelo.repositorio.RepositorioDeEspecificacao;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.RefatoradorDeString;
import br.unipampa.lesa.servico.TipoDeLog;

public class ControleNovoHandler implements Controller{

	
	private FrameAssistenteDeCriacao frameAssistente;
	private FileDialog dialog;
	private File file;
	
	public ControleNovoHandler(NovoHandler novoHandler){
		GerenciadorDeLog.getMyInstance().registrarLog("Inicializando controlador do NovoHandler",
				TipoDeLog.INFO, getClass());
		inicializarComponentes(novoHandler);
	}

	public void inicializarComponentes(Object objeto) {
		// TODO Auto-generated method stub
		if(objeto instanceof NovoHandler){
			GerenciadorDeLog.getMyInstance().registrarLog("Inicializando Componentes de NovoHandler",
					TipoDeLog.INFO, getClass());
			this.frameAssistente = new FrameAssistenteDeCriacao();
			registrarListeners();
			frameAssistente.open();
			
		}
	}

	@Override
	public void registrarListeners() {
		GerenciadorDeLog.getMyInstance().registrarLog("Registrando Listeners de NovoHandler",
				TipoDeLog.INFO, getClass());
		GerenciadorDeLog.getMyInstance().registrarLog("Registrando listeners do FrameAssistente",
				TipoDeLog.INFO, getClass());
		this.frameAssistente.getBtnBuscar().addListener(SWT.MouseUp,
				new Listener() {
					@Override
					public void handleEvent(Event arg0) {
					    dialog = new FileDialog(frameAssistente.getShlCriarProjetoDe());
						dialog.open();
					    file= new File(dialog.getFilterPath());
					    frameAssistente.getBtnCriar().setFocus();
						frameAssistente.getTxtDiretorio().setText(
								file.getPath().toString());
//					    puxaDiretorio = new JFileChooser();
//						puxaDiretorio
//								.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
//						int resposta = puxaDiretorio.showOpenDialog(null);
//						if (resposta == puxaDiretorio.APPROVE_OPTION) {
//							File fileObjeto = puxaDiretorio.getSelectedFile();
//							GeradorDeMensagem.exibirMensagemDeInformacao(
//									"Directory is selected!",
//									"Alert of the User");
//							frameAssistente.getBtnCriar().setFocus();
//							frameAssistente.getTxtDiretorio().setText(
//									fileObjeto.getPath().toString());
//							
//						}
					}
				});

		this.frameAssistente.getBtnCriar().addListener(SWT.MouseUp,
				new Listener() {
					@Override
					public void handleEvent(Event arg0) {
						try {
							if (!frameAssistente.getTxtProjeto().getText()
									.isEmpty()) {
								String nomeDeProjeto= RefatoradorDeString.substituirCaracteresBrPorEng(frameAssistente.getTxtProjeto()
										.getText());
								frameAssistente.getTxtProjeto().setText(nomeDeProjeto);
								Especificacao especificacao = new Especificacao(
										nomeDeProjeto);
								File fileAvaliacao = new File(frameAssistente.getTxtDiretorio().getText());
								if (frameAssistente.getTxtDiretorio().getText()
										.isEmpty()) {
									especificacao.criarEspecificacao();
								} else {
									File fileDeAvaliacao = new File(frameAssistente.getTxtDiretorio().getText());
									if(fileDeAvaliacao.exists()){
									especificacao
											.criarEspecificacao(frameAssistente
													.getTxtDiretorio()
													.getText());
									RepositorioDeEspecificacao.getMyInstance().recuperar(especificacao.getNomeDoProjeto());
									GeradorDeMensagem.exibirMensagemDeInformacao("Your specification project was created with Success!", "Alert of User");
									GerenciadorDeLog.getMyInstance().registrarLog("Criando novo projeto de especificação de requisitos", TipoDeLog.INFO, getClass());
									frameAssistente.getShlCriarProjetoDe().close();
									}else{
										GeradorDeMensagem.exibirMensagemDeInformacao("Invalid directory path adjustment The Way To An existing folder does not!", "Alert of User");
									}
								}
								
								}

							
						} catch (Exception erro) {
							GeradorDeMensagem.exibirMensagemDeErro("Could not create your specification, please perform this operation later!");
						}

					}
				});

	}

	@Override
	public void inicializarComponentes() {
		// TODO Auto-generated method stub
		
	}

	public FrameAssistenteDeCriacao getFrameAssistente() {
		return frameAssistente;
	}

	public void setFrameAssistente(FrameAssistenteDeCriacao frameAssistente) {
		this.frameAssistente = frameAssistente;
	}
	
	

}
