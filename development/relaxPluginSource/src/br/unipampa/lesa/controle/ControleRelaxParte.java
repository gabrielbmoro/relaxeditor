package br.unipampa.lesa.controle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import br.unipampa.lesa.apresentacao.EditorParte;
import br.unipampa.lesa.apresentacao.GeradorDeMensagem;
import br.unipampa.lesa.apresentacao.ProjectParte;
import br.unipampa.lesa.apresentacao.RelaxParte;
import br.unipampa.lesa.modelo.AnalisadorLexicoDeRequisito;
import br.unipampa.lesa.modelo.DicionarioDeDados;
import br.unipampa.lesa.modelo.GeradorDeExpressao;
import br.unipampa.lesa.modelo.Interpretador;
import br.unipampa.lesa.modelo.Requisito;
import br.unipampa.lesa.servico.EscritorDeArquivoTexto;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.RefatoradorDeString;
import br.unipampa.lesa.servico.TipoDeLog;

public class ControleRelaxParte implements Controller {

	private RelaxParte relaxParte;
	private ProjectParte projectParte;
	private EditorParte editorParte;
	private AnalisadorLexicoDeRequisito analizadorLexico;
	private Interpretador interpretador;
	private GeradorDeExpressao geradorDeExpressao;

	public ControleRelaxParte() {

	}

	@Override
	public void inicializarComponentes() {
		// TODO Auto-generated method stub

	}

	@Override
	public void inicializarComponentes(Object objeto) {
		// TODO Auto-generated method stub

	}

	@Override
	public void registrarListeners() {
		// TODO Auto-generated method stub
		this.relaxParte.getBtnConverter().addListener(SWT.MouseUp,
				new Listener() {
					@Override
					public void handleEvent(Event arg0) {
						reproduzirEventoDeConversao();
						}
					}
				);
	}

public void reproduzirEventoDeConversao()
{
	relaxParte.getTxtExpressao().setText("");
	String textRequisito = editorParte.getTxtRequisito()
			.getText();
	if (textRequisito == null || textRequisito.isEmpty()) {
		GeradorDeMensagem.exibirMensagemDeInformacao("Could not convert requirement, do not forget that you must specify a requirement to carry out this process .","Alert of User");
	} else {
		GerenciadorDeLog.getMyInstance().registrarLog(
				"Removendo caracteres especiais e acento",
				TipoDeLog.INFO, getClass());
		textRequisito = RefatoradorDeString
				.substituirCaracteresBrPorEng(textRequisito);
		String expressaoTempParaPropriedades = textRequisito;

		editorParte.getTxtRequisito()
				.setText(textRequisito);
		editorParte.verificarTodasPalavrasChave(textRequisito,
				new DicionarioDeDados());

		Requisito requisitoTeste = new Requisito();
		requisitoTeste.setDescricao(textRequisito);
		requisitoTeste.setDescricao(RefatoradorDeString
				.prepararExpressaoParaGeracao(requisitoTeste
						.getDescricao()));
		analizadorLexico = new AnalisadorLexicoDeRequisito();

		int validacaoNumero = analizadorLexico
				.verificarValidadeDaExpressao(requisitoTeste
						.getDescricao());

		if (validacaoNumero == 0) {
			interpretador = new Interpretador();
			geradorDeExpressao = new GeradorDeExpressao();
			String expressaoInicial = geradorDeExpressao
					.gerarExpressaoRelax(requisitoTeste);
			relaxParte.getTxtExpressao().setText(
					expressaoInicial + "\n");

			String textoParaYacc = RefatoradorDeString
					.prepararExpressaoParaYacc(expressaoInicial);
			String textoExpressaoSemParenteses = RefatoradorDeString
					.preparaExpressaoParaLexica(expressaoInicial);
			EscritorDeArquivoTexto escritor = new EscritorDeArquivoTexto();
			escritor.escreverArquivo(textoParaYacc);
			if (!interpretador
					.avaliaLexicamenteExpressao(textoExpressaoSemParenteses)) {
				relaxParte.informarErros("Lexical Error");
				GerenciadorDeLog
						.getMyInstance()
						.registrarLog(
								"Foi identificado erros de léxicos!",
								TipoDeLog.INFO, getClass());
			} else {
				if (!interpretador
						.avaliaSintaticamenteExpressao(textoParaYacc)) {
					relaxParte
							.informarErros("Syntactic Error");
					GerenciadorDeLog
							.getMyInstance()
							.registrarLog(
									"Foi identificado erros de construção!",
									TipoDeLog.INFO,
									getClass());
				} else {
					GerenciadorDeLog
							.getMyInstance()
							.registrarLog(
									"Não foi identificado erros de construção!",
									TipoDeLog.INFO,
									getClass());
					if (!relaxParte.getTxtExpressao()
							.getText().isEmpty()) {
						relaxParte
								.informarExpressaoGerada(expressaoInicial);
						GerenciadorDeLog
								.getMyInstance()
								.registrarLog(
										"Informando expressão gerada para interface gráfica!",
										TipoDeLog.INFO,
										getClass());
						String forq = interpretador
								.recuperarExpressaoFrequenciasOuQuantidade(expressaoTempParaPropriedades);
						String eventos = interpretador
								.recuperarEventos(expressaoTempParaPropriedades);
						String tempos = interpretador
								.recuperarTempos(expressaoTempParaPropriedades);
						String p = interpretador
								.recuperarExpressaoSemOperadores(
										expressaoTempParaPropriedades,
										tempos, eventos,
										forq);

						if (!forq.isEmpty() && forq != null) {
							relaxParte.getTxtExpressao()
									.append("\n f^q: "
											+ forq);
						}
						if (!eventos.isEmpty()
								&& eventos != null) {
							relaxParte.getTxtExpressao()
									.append("\n e: "
											+ eventos);
						}
						if (!tempos.isEmpty()
								&& tempos != null) {
							relaxParte.getTxtExpressao()
									.append("\n t: "
											+ tempos);
						}
						if (!p.isEmpty() && p != null) {
							relaxParte.getTxtExpressao()
									.append("\n p: " + p);
						}
						GerenciadorDeLog
								.getMyInstance()
								.registrarLog(
										"Terminando de informar as propriedades para interface gráfica!",
										TipoDeLog.INFO,
										getClass());

					}
				}

			}
		}

		else {
			String descricaoDeErros = "Number of Errors: "
					+ validacaoNumero;
			relaxParte.informarErros(descricaoDeErros);
		}	
	}
}

	public RelaxParte getRelaxParte() {
		return relaxParte;
	}

	public void setRelaxParte(RelaxParte relaxParte) {
		this.relaxParte = relaxParte;
	}

	public ProjectParte getProjectParte() {
		return projectParte;
	}

	public void setProjectParte(ProjectParte projectParte) {
		this.projectParte = projectParte;
	}

	public EditorParte getEditorParte() {
		return editorParte;
	}

	public void setEditorParte(EditorParte editorParte) {
		this.editorParte = editorParte;
	}

}
