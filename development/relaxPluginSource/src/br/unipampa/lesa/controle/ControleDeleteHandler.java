package br.unipampa.lesa.controle;

import br.unipampa.lesa.apresentacao.GeradorDeMensagem;
import br.unipampa.lesa.modelo.Especificacao;
import br.unipampa.lesa.modelo.repositorio.RepositorioDeEspecificacao;

public class ControleDeleteHandler implements Controller {

	public Especificacao especificacao;

	public ControleDeleteHandler() {
		inicializarComponentes(null);
		registrarListeners();
	}

	public void inicializarComponentes(Object objeto) {
		this.especificacao = RepositorioDeEspecificacao.getMyInstance()
				.getEspecificacaoList().get(0);
	}

	@Override
	public void registrarListeners() {
		// TODO Auto-generated method stub
		try {
			boolean resposta = GeradorDeMensagem.exibirMensagemDeConfirmacao(
					"Do you want delete this specification?", "Alert of User");
			if (resposta) {
				this.especificacao.deletarDados();
			}
		} catch (Exception erro) {
			GeradorDeMensagem
					.exibirMensagemDeErro("Problem occurred hum paragraph perform this operation, check for a Project");
		}
	}

	@Override
	public void inicializarComponentes() {
		// TODO Auto-generated method stub
		
	}
}
