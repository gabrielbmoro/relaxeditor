package br.unipampa.lesa.controle;

public interface Controller {
	
	public abstract void inicializarComponentes();
	public void inicializarComponentes(Object objeto);
	public abstract void registrarListeners();
	
}
