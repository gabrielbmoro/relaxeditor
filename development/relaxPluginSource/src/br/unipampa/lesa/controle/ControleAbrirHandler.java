package br.unipampa.lesa.controle;

import br.unipampa.lesa.apresentacao.AbrirHandler;
import br.unipampa.lesa.apresentacao.FrameAssistenteDeAbertura;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.TipoDeLog;

public class ControleAbrirHandler implements Controller{
	
	private FrameAssistenteDeAbertura frameAssistente;

	
	public ControleAbrirHandler(AbrirHandler abrirHandler){
		GerenciadorDeLog.getMyInstance().registrarLog("Iniciando o controle do AbrirHandler",
				TipoDeLog.INFO, getClass());
		inicializarComponentes(abrirHandler);
	}
	
	
	public void inicializarComponentes(Object objeto) {
		// TODO Auto-generated method stub
		if(objeto instanceof AbrirHandler){
			GerenciadorDeLog.getMyInstance().registrarLog("Inicializando elementos controladores do ControleAbrirHandler",
					TipoDeLog.INFO, getClass());
			frameAssistente = new FrameAssistenteDeAbertura();
			frameAssistente.open();
			
		}
	}

	@Override
	public void registrarListeners() {
	
	}


	@Override
	public void inicializarComponentes() {
		// TODO Auto-generated method stub
		
	}


	public FrameAssistenteDeAbertura getFrameAssistente() {
		return frameAssistente;
	}


	public void setFrameAssistente(FrameAssistenteDeAbertura frameAssistente) {
		this.frameAssistente = frameAssistente;
	}

	
	
}
