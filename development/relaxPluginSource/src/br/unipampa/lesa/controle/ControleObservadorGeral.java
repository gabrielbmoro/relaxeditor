package br.unipampa.lesa.controle;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import br.unipampa.lesa.apresentacao.EditorParte;
import br.unipampa.lesa.apresentacao.ProjectParte;
import br.unipampa.lesa.modelo.Especificacao;
import br.unipampa.lesa.modelo.Requisito;
import br.unipampa.lesa.modelo.repositorio.RepositorioDeEspecificacao;
import br.unipampa.lesa.servico.GerenciadorDeLog;
import br.unipampa.lesa.servico.TipoDeLog;

public class ControleObservadorGeral implements Observer {

	private ProjectParte projectParte;
	private EditorParte editorParte;

	public ControleObservadorGeral(Observable observado, ProjectParte projectParte, EditorParte editorParte) {
		GerenciadorDeLog.getMyInstance().registrarLog(
				"Inicializando ObservadorGeral de eventos", TipoDeLog.INFO,
				getClass());
		this.projectParte = projectParte;
		this.editorParte = editorParte;
		observado.addObserver(this);
	}

	/*
	 * ELEMENTO �RVORE for(int count=0;count<5;count++){ TreeItem treeItemTemp =
	 * new TreeItem(tree, SWT.NONE); treeItemTemp.setText("item"); for(int
	 * count1=0;count1<5;count1++){ TreeItem subTreeItem = new
	 * TreeItem(treeItemTemp, SWT.NONE); subTreeItem.setText("sub item"); } }
	 */

	@Override
	public void update(Observable objetoObservado, Object arg1) {
		projectParte.getTreeProjetos().setEnabled(true);
		GerenciadorDeLog.getMyInstance().registrarLog(
				"Notificação de alteração de algum elemento observado",
				TipoDeLog.INFO, getClass());
		Tree treeEspecificacao = this.projectParte.getTreeProjetos();
		treeEspecificacao.removeAll();

		if (objetoObservado instanceof RepositorioDeEspecificacao) {
			if (arg1 != null && arg1 instanceof Especificacao) {
				Especificacao especificacao = (Especificacao) arg1;
				preencherArvoreDeProjetos(especificacao);
				projectParte.habilitarComponentes();
			} else {
				RepositorioDeEspecificacao repositorioDeEspecificacao = (RepositorioDeEspecificacao) objetoObservado;
				ArrayList<Especificacao> especificacaoList = repositorioDeEspecificacao
						.getEspecificacaoList();
				preencherArvoreDeProjetos(especificacaoList);
				projectParte.habilitarComponentes();
			}
		}
	}
	

	private void preencherArvoreDeProjetos(
			ArrayList<Especificacao> especificacaoList) {
//		pluginParte.getComboRequisitos().removeAll();
		
		GerenciadorDeLog.getMyInstance().registrarLog(
				"Atualizando o elemento árvore de interface gráfica do plugin",
				TipoDeLog.INFO, getClass());
		for (Especificacao especificacao : especificacaoList) {
			TreeItem treeItemtemp = new TreeItem(
					this.projectParte.getTreeProjetos(), SWT.NONE);
			treeItemtemp.setFont(this.projectParte.fontCabecalhoArvore);
			treeItemtemp.setText(especificacao.getNomeDoProjeto());
			if (!especificacao.getRequisitos().isEmpty()) {
				for (Requisito requisitoTemp : especificacao.getRequisitos()) {
					TreeItem subItemTemp = new TreeItem(treeItemtemp, SWT.NONE,
							0);
					String denominacao = String.valueOf(requisitoTemp.getId())
					+ "-" + requisitoTemp.getNome();
					subItemTemp.setText(denominacao);
					editorParte.getComboRequisitos().add(denominacao);
					subItemTemp.setFont(this.projectParte.fontItemArvore);
				}
			}
		}

	}

	private void preencherArvoreDeProjetos(Especificacao especificacao) {
		GerenciadorDeLog.getMyInstance().registrarLog(
				"Atualizando o elemento árvore de interface gráfica do plugin",
				TipoDeLog.INFO, getClass());
		TreeItem treeItemtemp = new TreeItem(
				this.projectParte.getTreeProjetos(), SWT.NONE);
		treeItemtemp.setFont(this.projectParte.fontCabecalhoArvore);
		treeItemtemp.setText(especificacao.getNomeDoProjeto());
		if (!especificacao.getRequisitos().isEmpty()) {
			for (Requisito requisitoTemp : especificacao.getRequisitos()) {
				TreeItem subItemTemp = new TreeItem(treeItemtemp, SWT.NONE);
				String denominacao = String.valueOf(requisitoTemp.getId())
						+ "-" + requisitoTemp.getNome();
						subItemTemp.setText(denominacao);
						editorParte.getComboRequisitos().add(denominacao);
				subItemTemp.setFont(this.editorParte.fontItemArvore);
			}
		}
	}
}
