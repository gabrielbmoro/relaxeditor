
/* --------------------------Codigo de Usuario----------------------- */
package br.unipampa.lesa.servico.lexer;

import java_cup.runtime.*;
import java.io.Reader;
%implements java_cup.runtime.Scanner

%% //inicio de opciones

/* ------ Seccion de opciones y declaraciones de JFlex -------------- */  
   
/* 
    Cambiamos el nombre de la clase del analizador a Lexer
*/
%class ScannerDeAnalizadorSintatico
/*
    Activar el contador de lineas, variable yyline
    Activar el contador de columna, variable yycolumn
*/
%line
%column
    
/* 
   Activamos la compatibilidad con Java CUP para analizadores
   sintacticos(parser)
*/
%cup
   
/*
    Declaraciones

    El codigo entre %{  y %} sera copiado integramente en el 
    analizador generado.
*/
%{
    /*  Generamos un java_cup.Symbol para guardar el tipo de token 
        encontrado */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    /* Generamos un Symbol para el tipo de token encontrado 
       junto con su valor */
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}
   

/*
    Macro declaraciones
  
    Declaramos expresiones regulares que despues usaremos en las
    reglas lexicas.
*/
   
//Expressões Regulares
//Declarações
ESPACIO=" "
//fin declaraciones


%% //fin de opciones
/* -------------------- Seccion de reglas lexicas ------------------ */
   
/*
   Esta seccion contiene expresiones regulares y acciones. 
   Las acciones son código en Java que se ejecutara cuando se
   encuentre una entrada valida para la expresion regular correspondiente */
   
   /* YYINITIAL es el estado inicial del analizador lexico al escanear.
      Las expresiones regulares solo serán comparadas si se encuentra
      en ese estado inicial. Es decir, cada vez que se encuentra una 
      coincidencia el scanner vuelve al estado inicial. Por lo cual se ignoran
      estados intermedios.*/
   
<YYINITIAL> {
   
    /* Regresa que el token SEMI declarado en la clase sym fue encontrado. */
    "SHALL"                { return symbol(sym.SHALL); }
    "MAY"                { return symbol(sym.MAY); }
    "OR"                { return symbol(sym.OR); }
	"EVENTUALLY"                { return symbol(sym.EVENTUALLY); }
	"UNTIL"                { return symbol(sym.UNTIL); }
	"BEFORE"                { return symbol(sym.BEFORE); }
	"e"                { return symbol(sym.E); }
	"AFTER"                { return symbol(sym.AFTER); }
	"IN"                { return symbol(sym.IN); }
	"t"                { return symbol(sym.T); }
	"AS"  { return symbol(sym.AS); }
	"CLOSE"  { return symbol(sym.CLOSE); }
	"POSSIBLE"  { return symbol(sym.POSSIBLE); }
	"TO"  { return symbol(sym.TO); }
	"EARLY"  { return symbol(sym.EARLY); }
	"LATE"  { return symbol(sym.LATE); }
	"MANY"  { return symbol(sym.MANY); }
	"FEW"  { return symbol(sym.FEW); }
	"p"    { return symbol(sym.P); }
	"forq" { return symbol(sym.ForQ);}
	"true" { return symbol(sym.TRUE);}
	"false" { return symbol(sym.FALSE);}
    /* No hace nada si encuentra el espacio en blanco */
    {ESPACIO}       { /* ignora el espacio */ } 
}


/* Si el token contenido en la entrada no coincide con ninguna regla
    entonces se marca un token ilegal */
[^]                    { throw new Error("Caracter ilegal <"+yytext()+">"); }
