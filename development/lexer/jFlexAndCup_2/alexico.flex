/*Seccion de codigo de usuario*/
package br.unipampa.lesa.modelo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

//Classe dos tokens devolvidos - exemplo utilizado de @author Arturo de Casso
class Yytoken {
    Yytoken (int numToken,String token, String tipo, int linea, int columna){
        //Contador de Tokens reconhecidos
        this.numToken = numToken;
        //String do token reconhecido
        this.token = new String(token);
        //Tipo de componente léxico encontrado
        this.tipo = tipo;
        //Número da linha
        this.linea = linea;
        //Número da coluna
        this.columna = columna;
    }
    public int numToken;
    public String token;
    public String tipo;
    public int linea;
    public int columna;
    //Metodo que devolve os tokens em um arquivo de saída
    public String toString() {
        return "Token #"+numToken+": "+token+" C.Lexico: "+tipo+" ["+linea
        + "," +columna + "]";
    }
}

/* Sessão de Componentes e Declarações JFlex */
%% //Início das Opções
//Trocarmos o nome da função token para next-token
%function nextToken
//Classe pública
%public
//Trocarmos o nome da classe para AnalizardorLexico
%class AnalizadorLexicoDeExpressao
//Acrescentamos suporte a unicode
%unicode
//Código java
%{
	
    private int contador;
    private ArrayList<Yytoken> tokens;

	private void writeOutputFile() throws IOException{
			String filename = "file.out";
			BufferedWriter out = new BufferedWriter(
				new FileWriter(filename));
            System.out.println("\n*** Tokens gravados no arquivo***\n");
			for(Yytoken t: this.tokens){
				System.out.println(t);
				out.write(t + "\n");
			}
			out.close();
	}
%}
//Contador de tokens
%init{
    contador = 0;
	tokens = new ArrayList<Yytoken>();
%init}
//Quando alcança o fim do arquivo de entrada
%eof{
	try{
		this.writeOutputFile();
        System.exit(0);
	}catch(IOException ioe){
		ioe.printStackTrace();
	}
%eof}
//Ativar o contador de linhas, variável yyline
%line
//Ativar o contador de colunas, variável yycolumn
%column
//Fim das Opções

//Expressões Regulares
//Declarações
ESPACIO=" "
SALTO=\n|\r|\r\n
//fin declaraciones

/* Seccion de reglas lexicas */
%% 
//Regla     {Acciones}

"SHALL"    {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"SHALL",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"true"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"true",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"p"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"p",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"t"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"t",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"e"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"e",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"forq"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"forq",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"false"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"false",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"MAY"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"MAY",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"OR"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"OR",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"EVENTUALLY"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"EVENTUALLY",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"UNTIL"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"UNTIL",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"BEFORE"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"BEFORE",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"AFTER"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"AFTER",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"IN"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"IN",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"AS"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"AS",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"POSSIBLE"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"POSSIBLE",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"EARLY"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"EARLY",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"LATE"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"LATE",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"CLOSE"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"CLOSE",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"FEW"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"FEW",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"TO"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"TO",yyline,yycolumn);
    tokens.add(t);
    return t;
}
"MANY"   {
    contador++;
    Yytoken t = new Yytoken(contador,yytext(),"MANY",yyline,yycolumn);
    tokens.add(t);
    return t;
}
{ESPACIO}   {
	//Ignore
}
{SALTO} {
    contador++;
    Yytoken t = new Yytoken(contador,"","fin_linea",yyline,yycolumn);
    tokens.add(t);
    return t;
}
