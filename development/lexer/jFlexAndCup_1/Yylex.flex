/*UserCode*/
package br.unipampa.lesa.servico.lexer;

import java.io.*;
import java_cup.runtime.*;

%%
/*Options and declarations*/
%class Yylex
%public
%type Symbol

%{
	public Symbol token(int tokenType)
	{
		System.err.println("Obtendo o token: "+ tokenType);
		return new java_cup.runtime.Symbol(sym.SHALL, new String(yytext()));
	}
%}
%%
("SHALL") {return token(sym.SHALL);}
("true") {return token(sym.TRUE);}
("p") {return token(sym.P);}
("t") {return token(sym.T);}
("e") {return token(sym.E);}
("f|q") {return token(sym.ForQ);}
("false") {return token(sym.FALSE);}
("MAY") {return token(sym.MAY);}
("OR") {return token(sym.OR);}
("EVENTUALLY") {return token(sym.EVENTUALLY);}
("UNTIL") {return token(sym.UNTIL);}
("BEFORE") {return token(sym.BEFORE);}
("AFTER") {return token(sym.AFTER);}
("IN") {return token(sym.IN);}
("AS") {return token(sym.AS);}
("POSSIBLE") {return token(sym.POSSIBLE);}
("EARLY") {return token(sym.EARLY);}
("LATE") {return token(sym.LATE);}
("CLOSE") {return token(sym.CLOSE);}
("FEW") {return token(sym.FEW);}
("TO") {return token(sym.TO);}
("MANY") {return token(sym.MANY);}
	. {return token(sym.ERRO);}
	

