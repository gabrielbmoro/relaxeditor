/*UserCode*/
package br.unipampa.lesa.servico.lexer;

import static br.unipampa.lesa.servico.lexer.Token.*;
%implements java_cup.runtime.Scanner
%%
/*Options and declarations*/
%class Yylex
%public
%cup
%type Token

white = [ \n\t\r]
letras = [a-z]
numeros = [0-9]
%%
	
("SHALL") {return new java_cup.runtime.Symbol(sym.SHALL, new String(yytext()));}
("true") {return new java_cup.runtime.Symbol(sym.TRUE, new String(yytext()));}
("p") {return new java_cup.runtime.Symbol(sym.P, new String(yytext()));}
("t") {return new java_cup.runtime.Symbol(sym.T, new String(yytext()));}
("e") {return new java_cup.runtime.Symbol(sym.E, new String(yytext()));}
("f|q") {return new java_cup.runtime.Symbol(sym.ForQ, new String(yytext()));}
("false") {return new java_cup.runtime.Symbol(sym.FALSE, new String(yytext()));}
("MAY") {return new java_cup.runtime.Symbol(sym.MAY, new String(yytext()));}
("OR") {return new java_cup.runtime.Symbol(sym.OR, new String(yytext()));}
("EVENTUALLY") {return new java_cup.runtime.Symbol(sym.EVENTUALLY, new String(yytext()));}
("UNTIL") {return new java_cup.runtime.Symbol(sym.UNTIL, new String(yytext()));}
("BEFORE") {return new java_cup.runtime.Symbol(sym.BEFORE, new String(yytext()));}
("AFTER") {return new java_cup.runtime.Symbol(sym.AFTER, new String(yytext()));}
("IN") {return new java_cup.runtime.Symbol(sym.IN, new String(yytext()));}
("AS") {return new java_cup.runtime.Symbol(sym.AS, new String(yytext()));}
("POSSIBLE") {return new java_cup.runtime.Symbol(sym.POSSIBLE, new String(yytext()));}
("EARLY") {return new java_cup.runtime.Symbol(sym.EARLY, new String(yytext()));}
("LATE") {return new java_cup.runtime.Symbol(sym.LATE, new String(yytext()));}
("CLOSE") {return new java_cup.runtime.Symbol(sym.CLOSE, new String(yytext()));}
("FEW") {return new java_cup.runtime.Symbol(sym.FEW, new String(yytext()));}
("TO") {return new java_cup.runtime.Symbol(sym.TO, new String(yytext()));}
("MANY") {return new java_cup.runtime.Symbol(sym.MANY, new String(yytext()));}
({letras}+) {return new java_cup.runtime.Symbol(sym.P, new String(yytext()));}
	. {return new java_cup.runtime.Symbol(sym.ERRO, new String(yytext()));}
	
