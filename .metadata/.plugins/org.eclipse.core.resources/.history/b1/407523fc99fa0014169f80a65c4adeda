package br.unipampa.lesa.modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import br.unipampa.lesa.servico.EscritorDeArquivoTexto;
import br.unipampa.lesa.servico.RefatoradorDeString;

/**
 * Interpretador.java
 * 
 * <ul>
 * <li><b>Propósito:</b></li>
 * Representa o núcleo da aplicação, através dele é possível reconhecer o
 * requisito de tipo {@link Requisito} definido na linguagem relax (a qual
 * utiliza os termos definidos no {@link DicionarioDeDados}.
 * 
 * <li><b>Instruções de uso:</b></li>
 * Deve ser utilizado de acordo com as interfaces disponibilizadas.
 * </ul>
 * 
 * @author gabrielbmoro
 * @version 1.0
 * @since 19/11/2014
 */
public class Interpretador {

	private DicionarioDeDados dicionarioDeDados;

	/**
	 * construtor
	 * <ul>
	 * <li><b>Propósito</b></li>
	 * O método construtor deve inicializar a lista de características da
	 * classe.
	 * </ul>
	 */
	public Interpretador() {
		this.dicionarioDeDados = new DicionarioDeDados();
	}

	public boolean avaliaLexicamenteExpressao(String expressao) {

		EscritorDeArquivoTexto escritor = new EscritorDeArquivoTexto();
		escritor.escreverArquivo(expressao);
		try {
			FileReader fileReader = new FileReader(new File(
					EscritorDeArquivoTexto.NOME_DO_ARQUIVO));

			BufferedReader bf = null;

			bf = new BufferedReader(fileReader);
			AnalizadorLexicoDeExpressao a = new AnalizadorLexicoDeExpressao(bf);
			Yytoken token = null;
			int size = expressao.split(" ").length;
			
			int numeroDeTokens = 0;
			
			do {
				token = a.nextToken();
				numeroDeTokens++;
				// System.out.println(token);
			} while (token != null);

		} catch (Error ex) {
			// Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null,
			// ex);
			// System.out.println("ERRO" );
			return false;
		} catch (FileNotFoundException fileException) {
			return false;
		} catch (IOException erroDeIO) {
			return false;
		} catch (Exception exception) {
			return false;
		}
		return true;
	}

	public boolean avaliaSintaticamenteExpressao(String expressao) {
		return new AnalizadorSintaticoDeExpressao().parse(expressao);
	}

	public int recuperaOcorrenciaDeOperadores(Requisito requisito) {
		int ocorrencias = 0;
		if (requisito != null) {
			String expressaoDoRequisito = requisito.getDescricao()
					.getExpressao().toString().toLowerCase();

			for (String operador : dicionarioDeDados.getTermos()) {
				if ((expressaoDoRequisito.contains(" " + operador.toUpperCase()
						+ " "))
						|| (expressaoDoRequisito.contains(" "
								+ operador.toLowerCase() + " "))) {
					ocorrencias++;
				} else {
					if ((expressaoDoRequisito.startsWith(operador.toLowerCase()
							+ " "))
							|| (expressaoDoRequisito.startsWith(operador
									.toUpperCase() + " "))) {
						ocorrencias++;
					}
				}
			}
			return ocorrencias;
		} else {
			return 0;
		}
	}

	public String gerarPrefixoOrSufixo(Requisito requisito,
			String operadorCorrente) {
		int ocorrenciaDeOperadores = recuperaOcorrenciaDeOperadores(requisito);
		if (requisito != null
				&& requisito.getDescricao().getExpressao() != null
				&& !requisito.getDescricao().getExpressao().isEmpty()) {
			if (operadorCorrente.equalsIgnoreCase("SHALL")
					|| operadorCorrente.equalsIgnoreCase("EVENTUALLY")
					|| operadorCorrente
							.equalsIgnoreCase("AS EARLY AS POSSIBLE")
					|| operadorCorrente.equalsIgnoreCase("UNTIL")
					|| operadorCorrente.equalsIgnoreCase("AS LATE AS POSSIBLE")
					|| operadorCorrente.equalsIgnoreCase("AS MANY AS POSSIBLE")
					|| operadorCorrente.equalsIgnoreCase("AS FEW AS POSSIBLE")
					) {
				if (ocorrenciaDeOperadores == 1
						|| recuperaDeterminadoOperador(ocorrenciaDeOperadores,
								requisito).equalsIgnoreCase(operadorCorrente)) {
					if (requisito.getDescricao().getExpressao()
							.contains("true")) {
						return " true";
					} else if (requisito.getDescricao().getExpressao()
							.contains("false")) {
						return " false";
					} else {
						return " p";
					}
				}
			} else if (operadorCorrente.equalsIgnoreCase("BEFORE")
					|| operadorCorrente.equalsIgnoreCase("AFTER")) {
				if (ocorrenciaDeOperadores == 1
						|| ((ocorrenciaDeOperadores
								- recuperaDeterminadoOperadorPorOcorrencia(
										requisito, operadorCorrente) == 0))) {
					if (requisito.getDescricao().getExpressao()
							.contains("true")) {
						return " e true";
					} else if (requisito.getDescricao().getExpressao()
							.contains("false")) {
						return " e false";
					} else {
						return " e p";
					}
				} else {
					return " e";
				}
			} else if(operadorCorrente.equalsIgnoreCase("MAY")
					|| operadorCorrente.equalsIgnoreCase("OR")){
					return " p";
			} else if (operadorCorrente.equalsIgnoreCase("IN")) {
				if (ocorrenciaDeOperadores == 1) {
					return " t p";
				} else {
					return " t";
				}
			} else if (operadorCorrente
					.equalsIgnoreCase("AS CLOSE AS POSSIBLE TO")) {
				if (ocorrenciaDeOperadores == 1) {
					return " (f or q) p";
				} else {
					return " (f or q)";
				}
			}
		}
		return "";
	}

	public String recuperaDeterminadoOperador(int numeroDeOrdemCrescente,
			Requisito requisito) {
		int count = 0;
		if (requisito != null) {
			String expressaoDoRequisito = requisito.getDescricao()
					.getExpressao().toString().toLowerCase();
			String[] expressaoDoRequisitoQuebrada = expressaoDoRequisito
					.split(" ");
			String operadorTemp = "";

			for (int countA = 0; countA < expressaoDoRequisitoQuebrada.length; countA++) {
				for (String operador : dicionarioDeDados.getTermos()) {
					if (expressaoDoRequisitoQuebrada[countA]
							.equalsIgnoreCase(operador)) {
						operadorTemp = operador;
						count++;
						break;
					} else {
						String operadorModificado = operador
								.replaceAll(" ", "").toLowerCase();
						if (operador
								.equalsIgnoreCase("AS CLOSE AS POSSIBLE TO")) {
							if ((countA + 4) < expressaoDoRequisitoQuebrada.length) {
								if (operadorModificado
										.contains(expressaoDoRequisitoQuebrada[countA])
										&& operadorModificado
												.contains(expressaoDoRequisitoQuebrada[countA + 1])
										&& operadorModificado
												.contains(expressaoDoRequisitoQuebrada[countA + 2])
										&& operadorModificado
												.contains(expressaoDoRequisitoQuebrada[countA + 3])
									    && operadorModificado
									    		.contains(expressaoDoRequisitoQuebrada[countA + 4])) {
									operadorTemp = operador;
									count++;
								}
							}
						} else {
							if (operador.contains("AS")
									&& operador.contains("POSSIBLE")) {
								if ((countA + 3) < expressaoDoRequisitoQuebrada.length) {
									if (operadorModificado
											.contains(expressaoDoRequisitoQuebrada[countA + 1])
											&& operadorModificado
													.contains(expressaoDoRequisitoQuebrada[countA + 2])
											&& operadorModificado
													.contains(expressaoDoRequisitoQuebrada[countA + 3])) {
										operadorTemp = operador;
										count++;
									}
								}
							}

						}
					}
				}
				if (count == numeroDeOrdemCrescente) {
					return operadorTemp;
				}
			}
			return null;
		} else {
			return null;
		}
	}

	public int recuperaDeterminadoOperadorPorOcorrencia(Requisito requisito,
			String expr) {
		int count = 0;
		if (requisito != null) {
			String expressaoDoRequisito = requisito.getDescricao()
					.getExpressao().toString().toLowerCase();
			for (String operador : dicionarioDeDados.getTermos()) {
				if ((expressaoDoRequisito.contains(" " + operador.toUpperCase()
						+ " ") || expressaoDoRequisito.contains(" "
						+ operador.toLowerCase() + " "))) {
					count++;
					if (operador.equalsIgnoreCase(expr)) {
						return count;
					}
				}
			}
		} else {
			return 0;
		}
		return 0;
	}

	private String retirarExcessosDeExpressao(String expressaoTotal,
			String propriedade) {

		if (propriedade.contains("-")) {
			String[] dadosDeTempQuebrados = propriedade.split("-");
			for (int count1 = 0; count1 < dadosDeTempQuebrados.length; count1++) {
				if (expressaoTotal.startsWith(dadosDeTempQuebrados[count1]
						+ " ")) {
					expressaoTotal = expressaoTotal.replaceAll(" "
							+ dadosDeTempQuebrados[count1] + " ", " ");
				} else {
					expressaoTotal = expressaoTotal.replaceAll(" "
							+ dadosDeTempQuebrados[count1] + " ", " ");
				}
			}
		} else {
			expressaoTotal = expressaoTotal.replaceAll(propriedade, "");
		}
		return expressaoTotal;
	}

	public String recuperarExpressaoSemOperadores(String expressaoTotal,String tempo, String frequenciaOuQuantidade,String eventos) {
		String expressaoTotalTemporaria = expressaoTotal;
		if(!expressaoTotal.isEmpty() && expressaoTotal!=null){
		expressaoTotal = RefatoradorDeString
				.prepararExpressaoParaGeracao(expressaoTotal);
		
		expressaoTotal = retirarExcessosDeExpressao(expressaoTotal, tempo);
		expressaoTotal = retirarExcessosDeExpressao(expressaoTotal, eventos);
		expressaoTotal = retirarExcessosDeExpressao(expressaoTotal, frequenciaOuQuantidade);
		
			for (String termo : dicionarioDeDados.getTermos()) {
				String termoOficial = "";
				if (expressaoTotal.contains(" " + termo.toLowerCase() + " ")) {
					termoOficial = termo.toLowerCase();
				} else if (expressaoTotal.contains(" " + termo.toUpperCase()
						+ " ")) {
					termoOficial = termo.toUpperCase();
				}
				if (!termoOficial.isEmpty()) {
					if(termoOficial.equalsIgnoreCase("OR")){
						if(expressaoTotalTemporaria.contains(" may ")
								|| expressaoTotalTemporaria.contains(" MAY ")){
							expressaoTotal = expressaoTotal.replaceAll(" or ", " ");
							expressaoTotal = expressaoTotal.replaceAll(" OR ", " ");
							}
					}else{
					String[] array = expressaoTotal.split(" " + termoOficial);
					for (int count = 0; count < array.length; count++) {
						if (count == 0) {
							expressaoTotal = array[count];
						} else {
							expressaoTotal = expressaoTotal + array[count];
						}
					}
				}}

		}
		expressaoTotal = RefatoradorDeString
				.prepararExpressaoParaGeracao(expressaoTotal);
		return expressaoTotal;
		}else{
			return expressaoTotal;
		}
	}

	private String recuperaProximaPalavra(String expressao, String termo) {
		String resultado = "";
		if (expressao.contains(termo) || expressao.startsWith(termo)) {
			String[] expressaoQuebrada = expressao.split(termo);
			if (expressaoQuebrada.length == 2) {
				resultado = expressaoQuebrada[1].split(" ")[0];
			} else {
				for (int count = 0; count < expressaoQuebrada.length; count++) {
					if (count == 1) {
						resultado = expressaoQuebrada[count].split(" ")[0];
					} else {
						resultado = resultado + "-"
								+ expressaoQuebrada[count].split(" ")[0];
					}
				}
			}
		}

		return resultado;
	}

	public String recuperarExpressaoFrequenciasOuQuantidade(
			String expressaoTotal) {
		expressaoTotal = RefatoradorDeString
				.prepararExpressaoParaGeracao(expressaoTotal.toLowerCase());
		String termoDeReferencia = " as close as possible to ", resultado = "";

		resultado = recuperaProximaPalavra(expressaoTotal, termoDeReferencia);

		return resultado;
	}

	public String recuperarEventos(String expressaoTotal) {
		expressaoTotal = RefatoradorDeString
				.prepararExpressaoParaGeracao(expressaoTotal.toLowerCase());

		String termoDeReferencia = "before ", termoDeReferencia1 = "after ", resultado = "";
		resultado = resultado
				+ recuperaProximaPalavra(expressaoTotal, termoDeReferencia);
		String proximoTermo = recuperaProximaPalavra(expressaoTotal,
				termoDeReferencia1);
		if (!resultado.isEmpty()&&!proximoTermo.isEmpty()) {
			resultado = resultado + "-" + proximoTermo;
		}else if(resultado.isEmpty() && !proximoTermo.isEmpty()){
			resultado = proximoTermo;
		}else if(!resultado.isEmpty()&&proximoTermo.isEmpty()){
			resultado = resultado;
		}
		return resultado;
	}

	public String recuperarTempos(String expressaoTotal) {
		expressaoTotal = RefatoradorDeString
				.prepararExpressaoParaGeracao(expressaoTotal.toLowerCase());

		String termoDeReferencia = " in ", resultado = "";
		resultado = recuperaProximaPalavra(expressaoTotal, termoDeReferencia);
		return resultado;
	}

	public DicionarioDeDados getDicionarioDeDados() {
		return dicionarioDeDados;
	}

	public void setDicionarioDeDados(DicionarioDeDados dicionarioDeDados) {
		this.dicionarioDeDados = dicionarioDeDados;
	}

}